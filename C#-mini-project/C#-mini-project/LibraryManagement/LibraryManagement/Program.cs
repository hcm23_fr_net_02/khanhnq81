﻿using LibraryManagement.DTO.BookDTO;
using LibraryManagement.DTO.CustomerDTO;
using LibraryManagement.Helper;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Repository.RepositoryImplement;
using LibraryManagement.Service.IService;
using LibraryManagement.Service.ServiceImplement;

IBookRepository bookRepository = new BookRepository();
ICustomerRepository customerRepository = new CustomerRepository();

IBookService bookService = new BookService(bookRepository);
ICustomerService customerService = new CustomerService(customerRepository);
IBorrowBookService borrowBookService = new BorrowBookService(bookRepository, customerRepository);

int choice = 0;

do
{
    try
    {
        choice = Menu.Paging();

        switch (choice)
        {
            #region Book
            //Paging
            case 1:
                bookService.Paging();
                break;

            //Add new book
            case 2:
                Book book = new Book();
                book.Id = Library.Books.Count == 0 ? 1 : Library.Books.Last().Id + 1;
                Console.WriteLine("Enter book title: ");
                book.Title = Console.ReadLine();
                Console.WriteLine("Enter author name: ");
                book.Author = Console.ReadLine();
                Console.WriteLine("Enter genre: ");
                string genre = Console.ReadLine();
                book.Genre = (LibraryEnum.BookGenre)Enum.Parse(typeof(LibraryEnum.BookGenre), genre);
                Console.WriteLine("Enter publication date: ");
                book.PublicationYear = DateTime.Parse(Console.ReadLine());
                Console.WriteLine("Enter stock quantity: ");
                book.Quantity = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter description: ");
                book.Description = Console.ReadLine();
                bookService.AddNewBook(book);
                Menu.PressAnyKeyToContinue();
                break;

            //Update book
            case 3:
                BookUpdateDTO bookUpdateDTO = new BookUpdateDTO();
                Console.WriteLine("Enter book id to update: ");
                int bookUpdateId = int.Parse(Console.ReadLine());
                bookRepository.GetBookById(bookUpdateId);
                Console.WriteLine("Enter new title (Enter to keep old title): ");
                bookUpdateDTO.Title = Console.ReadLine();
                Console.WriteLine("Enter new author (Enter to keep old author): ");
                bookUpdateDTO.Author = Console.ReadLine();
                Console.WriteLine("Enter new quantity (Enter to keep old quantity): ");
                bookUpdateDTO.Quantity = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter new publishcation date: ");
                bookUpdateDTO.PublicationYear = DateTime.Parse(Console.ReadLine());
                bookService.UpdateBook(bookUpdateId, bookUpdateDTO);
                Menu.PressAnyKeyToContinue();
                break;

            //Remove book
            case 4:
                Console.WriteLine("Enter book id to remove: ");
                int bookRemoveId = int.Parse(Console.ReadLine());
                Console.WriteLine("Do you really want to remove ? (Y/N): ");
                var option = bookService.RemoveOption(Console.ReadLine());
                if (option.ToLower().Equals(Constance.REMOVE_ACCEPT.ToLower()))
                {
                    bookService.DeleteBook(bookRemoveId);
                }
                Menu.PressAnyKeyToContinue();
                break;

            //Find book and add review
            case 5:
                Console.WriteLine("Enter book id to find: ");
                int bookFindId = int.Parse(Console.ReadLine());
                var addBookReview = bookService.GetBookById(bookFindId);

                if (addBookReview is not null)
                {
                    Console.Clear();
                    Console.WriteLine($"ID: {addBookReview.Id} | Title: {addBookReview.Title} | Author: {addBookReview.Author} | Publication Year: {addBookReview.PublicationYear.ToString("dd/MM/yyyy")} | Quantity: {addBookReview.Quantity} | Genre: {addBookReview.Genre} | Description: {addBookReview.Description}");

                    foreach (var item in addBookReview.Reviews)
                    {
                        Console.WriteLine($"Review: \n\t Title: {item.ReviewTitle} \n\t Content: {item.Content}\n\t => Rate: {item.Rate}");
                    }
                }

                Console.WriteLine(Constance.REVIEW_OPTIONS);
                ConsoleKeyInfo key = Console.ReadKey();
                Console.Clear();
                if (key.Key == ConsoleKey.Y)
                {
                    Review review = new Review();

                    Console.WriteLine("Enter review title: ");
                    review.ReviewTitle = Console.ReadLine();
                    Console.WriteLine("Enter content: ");
                    review.Content = Console.ReadLine();
                    Console.WriteLine("Rate this book(1-5)");
                    review.Rate = int.Parse(Console.ReadLine());

                    bookService.AddReview(bookFindId, review);
                }
                Menu.PressAnyKeyToContinue();
                break;

            #endregion
            #region Borrow-Book
            //Borrow book
            case 6:
                Console.WriteLine("Enter book id: ");
                int bookIdToBorrow = int.Parse(Console.ReadLine());
                bookRepository.GetBookById(bookIdToBorrow);
                Console.WriteLine("Enter customer id: ");
                int customerIdToBorrow = int.Parse(Console.ReadLine());
                customerRepository.FindCustomerById(customerIdToBorrow);
                BorrowBook borrowBook = new BorrowBook();
                Console.WriteLine("Enter pay day: ");
                borrowBook.PayDay = DateTime.Parse(Console.ReadLine());
                Console.WriteLine("Enter borrow quantity: ");
                borrowBook.BookQuantity = int.Parse(Console.ReadLine());
                borrowBookService.BorrowBook(bookIdToBorrow, borrowBook, customerIdToBorrow);
                Menu.PressAnyKeyToContinue();
                break;

            //Return book
            case 7:
                Console.Clear();
                Console.WriteLine("Enter borrow id to return: ");
                int id = int.Parse(Console.ReadLine());
                borrowBookService.ReturnBook(id);
                Menu.PressAnyKeyToContinue();
                break;

            //Paging
            case 8:
                borrowBookService.Paging();
                break;
            //Find borrow book
            case 9:
                int findChoice = 0;

                do
                {
                    try
                    {
                        Menu.BorrowedBookMenu();
                        findChoice = int.Parse(Console.ReadLine());
                        switch (choice)
                        {
                            case 1:
                                Console.WriteLine("Enter customer name: ");
                                string customerName = Console.ReadLine();
                                var customerBorrowook = borrowBookService.FindBorrowedBookByCustomerName(customerName);
                                foreach (var item in customerBorrowook)
                                {
                                    var borrowedBook = Library.BorrowedBook.Where(book => book.CustomerId == item).ToList();
                                    foreach (var bookBorrowed in borrowedBook)
                                    {
                                        Console.WriteLine($"Borrow Id: {bookBorrowed.BorrowId} | Customer Id: {bookBorrowed.CustomerId} | Book Id: {bookBorrowed.BookId} | Borrow Date: {bookBorrowed.BorrowDate.ToString("dd/MM/yyyy")} | Pay Day: {bookBorrowed.PayDay.ToString("dd/MM/yyyy")} | Quantity: {bookBorrowed.BookQuantity}");
                                    }
                                }
                                Menu.PressAnyKeyToContinue();
                                break;

                            case 2:
                                Console.WriteLine("Enter book name: ");
                                string bookName = Console.ReadLine();
                                var borrowedBookByBookName = borrowBookService.FindBorrowedBookByBookName(bookName);

                                foreach (var item in borrowedBookByBookName)
                                {
                                    var bookBorrowDetail = Library.BorrowedBook.Where(book => book.BookId == item).ToList();
                                    foreach (var bookDetail in bookBorrowDetail)
                                    {
                                        Console.WriteLine($"Borrow Id: {bookDetail.BorrowId} | Customer Id: {bookDetail.CustomerId} | Book Id: {bookDetail.BookId} | Borrow Date: {bookDetail.BorrowDate.ToString("dd/MM/yyyy")} | Pay Day: {bookDetail.PayDay.ToString("dd/MM/yyyy")} | Quantity: {bookDetail.BookQuantity}");
                                    }
                                }
                                Menu.PressAnyKeyToContinue();
                                break;

                            case 3:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (findChoice < 3);

                break;
            #endregion 
            #region Fillter
            //Fillter by book title
            case 10:
                Console.WriteLine("Enter book title: ");
                string title = Console.ReadLine();
                var list = bookService.FillterByTitle(title);

                foreach (var bookItem in list)
                {
                    Console.WriteLine($"Id: {bookItem.Id} | Title: {bookItem.Title} | Author: {bookItem.Author} |  Publication Year: {bookItem.PublicationYear.ToString("dd/MM/yyyy")} | Quantity: {bookItem.Quantity} | Genre: {bookItem.Genre} | Description: {bookItem.Description}");

                    foreach (var reviewItem in bookItem.Reviews)
                    {
                        Console.WriteLine($"Review: \n\t Title: {reviewItem.ReviewTitle} \n\t Content: {reviewItem.Content}\n\t => Rate: {reviewItem.Rate}");
                    }
                }
                Menu.PressAnyKeyToContinue();
                break;

            //Fillter by book author
            case 11:
                Console.WriteLine("Enter book author: ");
                string author = Console.ReadLine();
                var filterByAuthor = bookService.FillterByAuthor(author);
                foreach (var bookByAuthor in filterByAuthor)
                {
                    Console.WriteLine($"Id: {bookByAuthor.Id} | Title: {bookByAuthor.Title} | Author: {bookByAuthor.Author} |  Publication Year: {bookByAuthor.PublicationYear.ToString("dd/MM/yyyy")} | Quantity: {bookByAuthor.Quantity} | Genre: {bookByAuthor.Genre} | Description: {bookByAuthor.Description}");

                    foreach (var reviewByAuthor in bookByAuthor.Reviews)
                    {
                        Console.WriteLine($"Review: \n\t Title: {reviewByAuthor.ReviewTitle} \n\t Content: {reviewByAuthor.Content}\n\t => Rate: {reviewByAuthor.Rate}");
                    }
                }
                Menu.PressAnyKeyToContinue();
                break;

            //Filter by publication year
            case 12:

                Console.WriteLine("Enter publication year: ");
                int year = int.Parse(Console.ReadLine());
                var filterByYear = bookService.FillterByPublicationYear(year);
                foreach (var bookByYear in filterByYear)
                {
                    Console.WriteLine($"Id: {bookByYear.Id} | Title: {bookByYear.Title} | Author: {bookByYear.Author} |  Publication Year: {bookByYear.PublicationYear.ToString("dd/MM/yyyy")} | Quantity: {bookByYear.Quantity} | Genre: {bookByYear.Genre} | Description: {bookByYear.Description}");

                    foreach (var reviewByYear in bookByYear.Reviews)
                    {
                        Console.WriteLine($"Review: \n\t Title: {reviewByYear.ReviewTitle} \n\t Content: {reviewByYear.Content}\n\t => Rate: {reviewByYear.Rate}");
                    }
                }
                Menu.PressAnyKeyToContinue();
                break;

            //Filter by book genre
            case 13:
                Console.WriteLine("Enter book genre: ");
                string genreToFilter = Console.ReadLine();

                var filterByGenre = bookService.FillterByGenre(genreToFilter);

                foreach (var bookByGenre in filterByGenre)
                {
                    Console.WriteLine($"Id: {bookByGenre.Id} | Title: {bookByGenre.Title} | Author: {bookByGenre.Author} |  Publication Year: {bookByGenre.PublicationYear.ToString("dd/MM/yyyy")} | Quantity: {bookByGenre.Quantity} | Genre: {bookByGenre.Genre} | Description: {bookByGenre.Description}");

                    foreach (var reviewByGenre in bookByGenre.Reviews)
                    {
                        Console.WriteLine($"Review: \n\t Title: {reviewByGenre.ReviewTitle} \n\t Content: {reviewByGenre.Content}\n\t => Rate: {reviewByGenre.Rate}");
                    }
                }
                Menu.PressAnyKeyToContinue();
                break;

            #endregion
            #region Customer
            //Add new customer
            case 14:
                Customer customer = new Customer();
                customer.Id = Library.Customers.Count == 0 ? 1 : Library.Customers.Last().Id + 1;
                Console.WriteLine("Enter customer name: ");
                customer.Name = Console.ReadLine();
                Console.WriteLine("Enter customer address: ");
                customer.Address = Console.ReadLine();
                customerService.AddNewCustomer(customer);
                break;

            //Paging
            case 15:
                customerService.Paging();
                break;

            //Update customer
            case 16:
                CustomerUpdateDTO customerUpdateDTO = new CustomerUpdateDTO();
                Console.WriteLine("Enter customer id to update: ");
                int customerUpdateId = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter new name: ");
                customerUpdateDTO.Name = Console.ReadLine();
                Console.WriteLine("Enter new address: ");
                customerUpdateDTO.Address = Console.ReadLine();
                customerService.UpdateCustomer(customerUpdateId, customerUpdateDTO);
                break;

            //Remove customer
            case 17:
                Console.WriteLine("Enter customer id to remove: ");
                int customerRemoveId = int.Parse(Console.ReadLine());
                Console.WriteLine("Do you really want to remove ? (Y/N): ");
                var optionRemove = customerService.RemoveOption(Console.ReadLine());
                if (optionRemove.ToLower().Equals(Constance.REMOVE_ACCEPT.ToLower()))
                {
                    customerService.DeleteCustomer(customerRemoveId);
                }
                break;
            #endregion
            #region Exit
            case 18:
                Console.Clear();
                Console.WriteLine(Constance.EXIT_PROGRAM);
                Environment.Exit(0);
                break;
            #endregion
            default:
                throw new Exception(Constance.INVALID_USER_CHOICE);
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
        Menu.PressAnyKeyToContinue();
    }
    finally
    {
        bookService.Save();
        customerService.SaveCustomer();
    }
} while (choice != 18);