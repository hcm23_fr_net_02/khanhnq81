﻿using LibraryManagement.Model;

namespace LibraryManagement.Helper
{
    public class Menu
    {
        public static void MainMenuPage1()
        {
            Console.WriteLine("=======================MENU=======================");
            Console.WriteLine("| 1. Show all book in Library                    |");
            Console.WriteLine("| 2. Add new  book in Library                    |");
            Console.WriteLine("| 3. Update book in Library                      |");
            Console.WriteLine("| 4. Remove book in Library                      |");
            Console.WriteLine("| 5. Find book and add review                    |");
            Console.WriteLine("==================================================");
        }

        public static void MainMenuPage2()
        {
            Console.WriteLine("====================BORROW-BOOK===================");
            Console.WriteLine("| 6. Borrow Book                                 |");
            Console.WriteLine("| 7. Return Book                                 |");
            Console.WriteLine("| 8. Show all borrowed book                      |");
            Console.WriteLine("| 9. Find borrowed book                          |");
            Console.WriteLine("==================================================");
        }

        public static void MainMenuPage3()
        {
            Console.WriteLine("==================FILLTTER-BOOK===================");
            Console.WriteLine("| 10. Fillter book by title                      |");
            Console.WriteLine("| 11. Fillter book by author                     |");
            Console.WriteLine("| 12.Fillter book by publication year            |");
            Console.WriteLine("| 13.Fillter book by genre                       |");
            Console.WriteLine("==================================================");
        }

        public static void MainMenuPage4()
        {
            Console.WriteLine("==================CUSTOMER-MENU===================");
            Console.WriteLine("| 14. Add new customer                           |");
            Console.WriteLine("| 15. Show all customer                          |");
            Console.WriteLine("| 16.Update customer                             |");
            Console.WriteLine("| 17.Delete customer                             |");
            Console.WriteLine("| 18.Exit                                        |");
            Console.WriteLine("==================================================");
        }

        public static void BorrowedBookMenu()
        {
            Console.WriteLine("====================BORROW-BOOK===================");
            Console.WriteLine("| 1. Find by customer name.                      |");
            Console.WriteLine("| 2. Find by book name.                          |");
            Console.WriteLine("| 3. Back to main menu.                          |");
            Console.WriteLine("==================================================");
        }

        public static void PressAnyKeyToContinue()
        {
            Console.WriteLine(Constance.PRESS_ANY_KEY_TO_CONTINUE);
            Console.ReadKey();
        }
        public static int Paging(int currentPage = 0)
        {
            string key = "";
            while (!int.TryParse(key, out int choice))
            {
                if (currentPage == 0)
                {
                    Console.Clear();
                    MainMenuPage1();
                }
                if (currentPage == 1)
                {
                    Console.Clear();
                    MainMenuPage2();
                }
                if (currentPage == 2)
                {
                    Console.Clear();
                    MainMenuPage3();
                }
                if (currentPage == 3)
                {
                    Console.Clear();
                    MainMenuPage4();
                }
                Console.WriteLine("\n[N] Next | [P] Previous | [E] Exit");
                key = Console.ReadLine();

                if (key.Equals("N".ToLower()))
                {
                    if (currentPage < 3)
                    {
                        currentPage++;
                    }
                }
                if (key.Equals("P".ToLower()))
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }
                if (key.Equals("E".ToLower()))
                {
                    return 18;
                }
                Console.Clear();
            }
            if (int.TryParse(key, out int result))
            {
                return result;
            }
            return -1;
        }
    }
}
