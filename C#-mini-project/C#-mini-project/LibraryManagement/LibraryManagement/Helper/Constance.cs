﻿namespace LibraryManagement.Helper
{
    public static class Constance
    {
        public const string BOOK_NOT_FOUND = "Can not found book !";
        public const string BOOK_NOT_FOUND_BY_GENRE = "Can not found book by that genre !";
        public const string BOOK_REMOVE_FAIL = "Remove book fail !";
        public const string BOOK_RUN_OUT = "The number of books has run out !";
        public const string BOOK_NOT_FOUND_BY_TITLE = "Can not found book by that title !";
        public const string BOOK_NOT_FOUND_BY_YEAR = "Can not found book by that year !";
        public const string BOOK_NOT_FOUND_BY_AUTHOR = "Can not found book by that author !";

        public const string INVALID_USER_CHOICE = "Invalid choice. Please choose in range !";

        public const string CUSTOMER_NOT_FOUND = "Can not found customer !";
        public const string CUSTOMER_REMOVE_FAIL = "Remove customer fail !";

        public const string PAY_DAY_EXCEPTION = "Pay day must higher than borrow date !";

        public const string REMOVE_OPTIONS = "Do you really want to remove ? (Y/N): ";
        public const string REMOVE_EXCEPTION = "Invalid choice !";
        public const string REMOVE_ACCEPT = "Y";

        public const string REVIEW_OPTIONS = "Do you want to write review ? (Y/N)";

        public const string PRESS_ANY_KEY_TO_CONTINUE = "Press any key to continue";

        public const string EXIT_PROGRAM = "Good bye !";
    }
}
