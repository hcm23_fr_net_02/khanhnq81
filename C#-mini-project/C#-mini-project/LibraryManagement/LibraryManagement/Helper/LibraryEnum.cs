﻿namespace LibraryManagement.Helper
{
    public class LibraryEnum
    {
        public enum BookGenre
        {
            Action,
            Horror,
            Romance,
            Drama,
            Science
        }
    }
}
