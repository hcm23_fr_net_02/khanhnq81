﻿using LibraryManagement.Model.Entities;

namespace LibraryManagement.IRepository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Customer FindCustomerById(int id);
    }
}
