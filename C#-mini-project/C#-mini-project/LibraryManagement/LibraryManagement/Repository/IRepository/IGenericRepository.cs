﻿namespace LibraryManagement.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> ReadDataFromJson(List<T> list, string filePath);
        void AddNew(List<T> list,T entity);
        bool Delete(List<T> list, T entity);
        void SaveFile(List<T> list, string filePath);
    }
}
