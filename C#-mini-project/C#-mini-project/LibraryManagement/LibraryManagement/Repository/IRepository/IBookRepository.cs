﻿using LibraryManagement.Model.Entities;

namespace LibraryManagement.IRepository
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        Book GetBookById(int id);
    }
}
