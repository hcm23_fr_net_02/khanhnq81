﻿using LibraryManagement.Helper;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;

namespace LibraryManagement.Repository.RepositoryImplement
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public Book GetBookById(int id)
        {
            var book = Library.Books.FirstOrDefault(book => book.Id == id);

            if (book is null)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND);
            }

            return book;
        }
    }
}
