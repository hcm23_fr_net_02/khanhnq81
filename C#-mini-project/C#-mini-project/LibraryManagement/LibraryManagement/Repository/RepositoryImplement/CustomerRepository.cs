﻿using LibraryManagement.Helper;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;

namespace LibraryManagement.Repository.RepositoryImplement
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public Customer FindCustomerById(int id)
        {
            var customer = Library.Customers.FirstOrDefault(customer => customer.Id == id);

            if (customer is null)
            {
                throw new Exception(Constance.CUSTOMER_NOT_FOUND);
            }
            return customer;
        }
    }
}

