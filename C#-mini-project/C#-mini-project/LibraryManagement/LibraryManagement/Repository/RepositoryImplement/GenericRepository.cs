﻿using LibraryManagement.IRepository;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LibraryManagement.Repository.RepositoryImplement
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public void AddNew(List<T> list, T entity) 
        { 
            list.Add(entity);
        }

        public bool Delete(List<T> list,T entity) => list.Remove(entity);

        public List<T> ReadDataFromJson(List<T> list, string filePath)
        {
            string content = File.ReadAllText(filePath);
            list = JsonConvert.DeserializeObject<List<T>>(content);
            return list;
        }

        public void SaveFile(List<T> list, string filePath)
        {
            string content = JsonConvert.SerializeObject(list, Formatting.Indented);
            File.WriteAllText(filePath, content);
        }
    }
}
