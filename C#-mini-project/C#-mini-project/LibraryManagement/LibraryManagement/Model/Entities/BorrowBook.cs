﻿using LibraryManagement.Helper;

namespace LibraryManagement.Model.Entities
{
    public class BorrowBook
    {
        private DateTime _payDay;

        public int BorrowId { get; set; }
        public int BookId { get; set; }
        public int CustomerId { get; set; }
        public DateTime BorrowDate { get; set; }
        public DateTime PayDay
        {
            get => _payDay;
            set
            {
                if (value < BorrowDate)
                {
                    throw new Exception(Constance.PAY_DAY_EXCEPTION);
                }
                _payDay = value;
            }
        }
        public int BookQuantity { get; set; }
    }
}
