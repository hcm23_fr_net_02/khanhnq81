﻿namespace LibraryManagement.Model.Entities
{
    public class Library
    {
        public static List<Book> Books { get; set; } = new List<Book>();
        public static List<BorrowBook> BorrowedBook { get; set; } = new List<BorrowBook>();
        public static List<Customer> Customers { get; set; } = new List<Customer>();

    }
}
