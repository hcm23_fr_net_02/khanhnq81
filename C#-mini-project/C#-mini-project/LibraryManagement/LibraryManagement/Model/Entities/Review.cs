﻿namespace LibraryManagement.Model.Entities
{
    public class Review
    {
        private int _rate;

        public string ReviewTitle { get; set; }
        public string Content { get; set; }
        public int Rate
        {
            get => _rate;
            set
            {
                if (value < 0 || value > 5)
                {
                    throw new Exception("Invalid rate !");
                }
                _rate = value;
            }
        }
    }
}
