﻿using LibraryManagement.Helper;

namespace LibraryManagement.Model.Entities
{
    public class Book
    {
        private int _quantity;

        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublicationYear { get; set; }
        public int Quantity
        {
            get => _quantity;
            set
            {
                if (value < 0)
                {
                    throw new Exception(Constance.BOOK_RUN_OUT);
                }
                _quantity = value;
            }
        }
        public LibraryEnum.BookGenre Genre { get; set; }
        public string Description { get; set; }
        public List<Review> Reviews { get; set; } = new List<Review>();
    }
}
