﻿namespace LibraryManagement.DTO.CustomerDTO
{
    public class CustomerUpdateDTO
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
