﻿namespace LibraryManagement.DTO.BookDTO
{
    public class BookUpdateDTO
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublicationYear { get; set; }
        public int Quantity { get; set; }
    }
}
