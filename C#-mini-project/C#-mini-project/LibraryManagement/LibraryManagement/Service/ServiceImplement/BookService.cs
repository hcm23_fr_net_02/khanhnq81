﻿using LibraryManagement.DTO.BookDTO;
using LibraryManagement.Helper;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Service.IService;

namespace LibraryManagement.Service.ServiceImplement
{
    public class BookService : IBookService
    {
        const string filePath = @"bookdata.json";
        private readonly IBookRepository _bookRepository;
        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
            Library.Books = _bookRepository.ReadDataFromJson(Library.Books, filePath);
        }

        public void Save()
        {
            _bookRepository.SaveFile(Library.Books, filePath);
        }

        public void Paging(int currentPage = 0, int pageSize = 10)
        {
            var totalPage = Math.Ceiling(Library.Books.Count / pageSize * 1.0);

            while (true)
            {
                Console.Clear();
                var books = Library.Books.Skip(currentPage * pageSize).Take(pageSize).ToList();

                foreach (var book in books)
                {
                    Console.WriteLine($"Book: {book.Id} | Title: {book.Title} | Author: {book.Author} | Pulication: {book.PublicationYear.ToString("dd/MM/yyyy")} | Quantity: {book.Quantity} | Genre: {book.Genre} | Description: {book.Description}");
                }

                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage <= totalPage - 1)
                    {
                        currentPage++;
                    }
                }

                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }

                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }
            Console.Clear();
        }

        public void UpdateBook(int id, BookUpdateDTO bookUpdate)
        {
            var book = _bookRepository.GetBookById(id);

            if (book is null)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND);
            }

            book.Author = string.IsNullOrEmpty(bookUpdate.Author) ? book.Author : bookUpdate.Author;
            book.Title = string.IsNullOrEmpty(bookUpdate.Title) ? book.Title : bookUpdate.Title;
            book.Quantity = string.IsNullOrEmpty(bookUpdate.Quantity.ToString()) ? book.Quantity : bookUpdate.Quantity;
            book.PublicationYear = bookUpdate.PublicationYear;

        }

        public void DeleteBook(int id)
        {
            var book = _bookRepository.GetBookById(id);
            Console.WriteLine(Constance.REMOVE_OPTIONS);
            var check = _bookRepository.Delete(Library.Books, book);
            if (check is false)
            {
                throw new Exception(Constance.BOOK_REMOVE_FAIL);
            }
        }

        public string RemoveOption(string option)
        {
            if (!option.ToLower().Equals("Y".ToLower()) && !option.ToLower().Equals("N".ToLower()))
            {
                throw new Exception(Constance.REMOVE_EXCEPTION);
            }
            return option;
        }

        public Book GetBookById(int id)
        {
            var book = _bookRepository.GetBookById(id);
            return book;
        }

        public Book AddReview(int id, Review review)
        {
            var book = GetBookById(id);
            book.Reviews.Add(review);
            return book;
        }

        public void AddNewBook(Book book)
        {
            _bookRepository.AddNew(Library.Books, book);
        }

        public List<Book> FillterByTitle(string title)
        {
            var list = Library.Books.Where(book => book.Title.Contains(title)).ToList();

            if (list.Count is 0)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND_BY_TITLE);
            }

            return list;
        }

        public List<Book> FillterByAuthor(string author)
        {
            var list = Library.Books.Where(book => book.Author.Equals(author)).ToList();

            if (list.Count is 0)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND_BY_AUTHOR);
            }
            return list;
        }

        public List<Book> FillterByPublicationYear(int year)
        {
            var list = Library.Books.Where(book => book.PublicationYear.Year.Equals(year)).ToList();

            if (list.Count is 0)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND_BY_YEAR);
            }

            return list;
        }

        public List<Book> FillterByGenre(string genre)
        {
            var list = Library.Books.Where(book => book.Genre.ToString().ToLower().Equals(genre.ToLower())).ToList();

            if (list.Count is 0)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND_BY_GENRE);
            }
            return list;
        }

    }
}
