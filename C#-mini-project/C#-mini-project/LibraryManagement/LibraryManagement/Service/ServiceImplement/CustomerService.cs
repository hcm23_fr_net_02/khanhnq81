﻿using LibraryManagement.DTO.CustomerDTO;
using LibraryManagement.Helper;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Service.IService;

namespace LibraryManagement.Service.ServiceImplement
{
    public class CustomerService : ICustomerService
    {
        const string filePath = @"customerdata.json";
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
            Library.Customers = _customerRepository.ReadDataFromJson(Library.Customers,filePath);
        }

        public void SaveCustomer()
        {
            _customerRepository.SaveFile(Library.Customers, filePath);
        }

        public void AddNewCustomer(Customer customer)
        {
            _customerRepository.AddNew(Library.Customers,customer);
        }

        public void Paging(int currentPage = 0, int pageSize = 10)
        {
            var totalPage = Math.Ceiling(Library.Customers.Count / pageSize * 1.0);

            while (true)
            {
                Console.Clear();
                var customers = Library.Customers.Skip(currentPage * pageSize).Take(pageSize).ToList();
                foreach (var customer in customers)
                {
                    Console.WriteLine($"Id: {customer.Id} | Name: {customer.Name} | Address: {customer.Address}");
                }

                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage <= totalPage - 1)
                    {
                        currentPage++;
                    }
                }

                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }

                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }

            Console.Clear();
        }

        public void UpdateCustomer(int id, CustomerUpdateDTO customerUpdate)
        {
            var customer = _customerRepository.FindCustomerById(id);

            if (customer is null)
            {
                throw new Exception(Constance.CUSTOMER_NOT_FOUND);
            }
            customer.Name = customerUpdate.Name;
            customer.Address = customerUpdate.Address;
        }

        public void DeleteCustomer(int id)
        {
            var customer = _customerRepository.FindCustomerById(id);
            var check = _customerRepository.Delete(Library.Customers, customer);
            if (check is false)
            {
                throw new Exception(Constance.CUSTOMER_NOT_FOUND);
            }
        }

        public string RemoveOption(string option)
        {
            if (!option.ToLower().Equals("Y".ToLower()) && !option.ToLower().Equals("N".ToLower()))
            {
                throw new Exception(Constance.REMOVE_EXCEPTION);
            }
            return option;
        }
    }
}
