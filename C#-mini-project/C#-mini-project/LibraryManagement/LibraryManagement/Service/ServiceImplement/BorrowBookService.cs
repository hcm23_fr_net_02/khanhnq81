﻿using LibraryManagement.Helper;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Service.IService;

namespace LibraryManagement.Service.ServiceImplement
{
    public class BorrowBookService : IBorrowBookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly ICustomerRepository _customerRepository;
        public BorrowBookService(IBookRepository bookRepository, ICustomerRepository customerRepository)
        {
            _bookRepository = bookRepository;
            _customerRepository = customerRepository;
        }

        public void BorrowBook(int bookId, BorrowBook borrowBook, int customerId)
        {
            var book = _bookRepository.GetBookById(bookId);
            var customer = _customerRepository.FindCustomerById(customerId);
            if (book is not null)
            {
                if (book.Quantity <= 0)
                {
                    throw new Exception(Constance.BOOK_RUN_OUT);
                }

                borrowBook.BorrowId = Library.BorrowedBook.Count == 0 ? 1 : Library.BorrowedBook.Last().BorrowId + 1;
                borrowBook.BookId = book.Id;
                borrowBook.CustomerId = customer.Id;
                borrowBook.BorrowDate = DateTime.Now;
                book.Quantity -= borrowBook.BookQuantity;

                Library.BorrowedBook.Add(borrowBook);
            }
        }

        public void ReturnBook(int borrowId)
        {
            var borrowBook = Library.BorrowedBook.FirstOrDefault(borrow => borrow.BorrowId == borrowId);
            if (borrowBook is null)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND);
            }
            var book = Library.Books.FirstOrDefault(book => book.Id == borrowBook.BookId);
            book.Quantity += borrowBook.BookQuantity;

            Library.BorrowedBook.Remove(borrowBook);
        }

        public void Paging(int currentPage = 0, int pageSize = 10)
        {
            var totalPage = Math.Ceiling(Library.BorrowedBook.Count / pageSize * 1.0);

            while (true)
            {
                Console.Clear();
                var borrowBook = Library.BorrowedBook.Skip(currentPage * pageSize).Take(pageSize).ToList();

                foreach (var book in borrowBook)
                {
                    Console.WriteLine($"Borrow Id: {book.BorrowId} | Customer Id: {book.CustomerId} | Book Id: {book.BookId} | Borrow Date: {book.BorrowDate.ToString("dd/MM/yyyy")} | Pay Day: {book.PayDay.ToString("dd/MM/yyyy")} | Quantity: {book.BookQuantity}");
                }

                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage <= totalPage - 1)
                    {
                        currentPage++;
                    }
                }

                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }

                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }

            Console.Clear();
        }

        public List<int> FindBorrowedBookByCustomerName(string customerName)
        {
            var list = Library.Customers.Where(customer => customer.Name.Equals(customerName)).Select(customer => customer.Id).ToList();

            if (list.Count is 0)
            {
                throw new Exception(Constance.CUSTOMER_NOT_FOUND);
            }

            return list;
        }

        public List<int> FindBorrowedBookByBookName(string bookName)
        {
            var books = Library.Books.Where(book => book.Title.Equals(bookName)).Select(book => book.Id).ToList();

            if (books is null)
            {
                throw new Exception(Constance.BOOK_NOT_FOUND_BY_TITLE);
            }

            return books;
        }
    }
}
