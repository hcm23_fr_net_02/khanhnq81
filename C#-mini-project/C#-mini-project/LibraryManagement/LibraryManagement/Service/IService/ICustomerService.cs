﻿using LibraryManagement.DTO.CustomerDTO;
using LibraryManagement.Model.Entities;

namespace LibraryManagement.Service.IService
{
    public interface ICustomerService
    {
        public void AddNewCustomer(Customer customer);
        public void UpdateCustomer(int id, CustomerUpdateDTO customerUpdate);
        public void DeleteCustomer(int id);
        public string RemoveOption(string option);
        public void SaveCustomer();
        public void Paging(int currentPage = 0, int pageSize = 10);
    }
}
