﻿using LibraryManagement.DTO.BookDTO;
using LibraryManagement.Model.Entities;

namespace LibraryManagement.Service.IService
{
    public interface IBookService
    {
        public void UpdateBook(int id, BookUpdateDTO bookUpdate);
        public void DeleteBook(int id);
        public Book AddReview(int id, Review review);
        public void AddNewBook(Book book);
        public string RemoveOption(string option);
        public List<Book> FillterByTitle(string title);
        public List<Book> FillterByAuthor(string author);
        public List<Book> FillterByPublicationYear(int year);
        public List<Book> FillterByGenre(string genre);
        public Book GetBookById(int id);
        public void Save();
        public void Paging(int currentPage = 0, int pageSize = 10);
    }
}
