﻿using LibraryManagement.Model.Entities;

namespace LibraryManagement.Service.IService
{
    public interface IBorrowBookService
    {
        public void BorrowBook(int bookId, BorrowBook borrowBook, int customerId);
        public void ReturnBook(int borrowId);
        public List<int> FindBorrowedBookByCustomerName(string customerName);
        public List<int> FindBorrowedBookByBookName(string bookName);
        public void Paging(int currentPage = 0, int pageSize = 10);
    }
}
