﻿using LibraryManagement.DTO.CustomerDTO;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Repository.RepositoryImplement;
using LibraryManagement.Service.IService;
using LibraryManagement.Service.ServiceImplement;

namespace UnitTestLibraryManagement
{
    public class CustomerServiceTest
    {
        [Fact]
        public void GetData_ListShouldCount20Item()
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            var expected = 20;

            Library.Customers = customerRepository.ReadDataFromJson(Library.Customers, @"customerdata.json");
            var actual = Library.Customers.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddNewCustomer_ListShouldCountTheItem()
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);
            Customer customer = new Customer();
            var expected = 21;
            customer.Id = 1;
            customer.Name = "Test";
            customer.Address = "Test";

            customerService.AddNewCustomer(customer);
            var actual = Library.Customers.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DeleteCustomer_ListShouldMinusOneUnit()
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);

            var customerId = 1;
            var expected = 19;

            customerService.DeleteCustomer(customerId);
            var actual = Library.Customers.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UpdateCustomer_CustomerEnityMustEqualCustomerDTO()
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);

            var customerId = 1;
            var customer = customerRepository.FindCustomerById(customerId);

            CustomerUpdateDTO customerUpdateDTO = new CustomerUpdateDTO();
            customerUpdateDTO.Name = "Test";
            customerUpdateDTO.Address = "Test";

            customerService.UpdateCustomer(customerId, customerUpdateDTO);

            Assert.Equal(customerUpdateDTO.Name, customer.Name);
            Assert.Equal(customerUpdateDTO.Address, customer.Address);
        }

        [Fact]
        public void RemoveOption_MustReturnCharacterY()
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);
            var expected = "Y";

            var actual = customerService.RemoveOption("Y");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveOption_MustReturnCharacterN()
        {
            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);
            var expected = "N";

            var actual = customerService.RemoveOption("N");
            Assert.Equal(expected, actual);
        }
    }
}
