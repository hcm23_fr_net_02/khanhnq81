using LibraryManagement.DTO.BookDTO;
using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Repository.RepositoryImplement;
using LibraryManagement.Service.IService;
using LibraryManagement.Service.ServiceImplement;

namespace UnitTestLibraryManagement
{
    public class BookServiceTest
    {
        [Fact]
        public void GetData_ListShouldCount50Item()
        {
            IBookRepository bookRepository = new BookRepository();
            var expected = 50;

            Library.Books = bookRepository.ReadDataFromJson(Library.Books, @"bookdata.json");
            var actual = Library.Books.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddNewBook_ListShouldCountTheItem()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            Book book = new Book();
            var expected = 51;
            book.Author = "Test";
            book.Title = "Test";
            book.Quantity = 1;
            book.Description = "Test";
            book.PublicationYear = DateTime.Now;
            book.Genre = LibraryManagement.Helper.LibraryEnum.BookGenre.Science;

            bookService.AddNewBook(book);
            var actual = Library.Books.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DeleteCustomer_ListShouldMinusOneUnit()
        {

            IBookRepository bookRepository = new BookRepository();
            IBookService service = new BookService(bookRepository);

            var bookId = 1;
            var expected = 49;

            service.DeleteBook(bookId);
            var actual = Library.Books.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddReview_ListReviewShouldCountOneMore()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService service = new BookService(bookRepository);
            var book = bookRepository.GetBookById(1);
            book.Reviews.Add(new Review { ReviewTitle = "Test", Content = "Test", Rate = 5 });

            var expected = 1;

            var actual = book.Reviews.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void UpdateBook_BookEnityMustEqualBookDTO()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);

            var bookId = 1;
            var book = bookRepository.GetBookById(bookId);

            BookUpdateDTO bookUpdateDTO = new BookUpdateDTO();
            bookUpdateDTO.Title = "Test";
            bookUpdateDTO.Author = "Test";
            bookUpdateDTO.Quantity = 5;
            bookUpdateDTO.PublicationYear = DateTime.Now;


            bookService.UpdateBook(bookId, bookUpdateDTO);

            Assert.Equal(bookUpdateDTO.Title, book.Title);
            Assert.Equal(bookUpdateDTO.Author, book.Author);
            Assert.Equal(bookUpdateDTO.Quantity, book.Quantity);
            Assert.Equal(bookUpdateDTO.PublicationYear, book.PublicationYear);
        }

        [Fact]
        public void RemoveOption_MustReturnCharacterY()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            var expected = "Y";

            var actual = bookService.RemoveOption("Y");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveOption_MustReturnCharacterN()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            var expected = "N";

            var actual = bookService.RemoveOption("N");
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void FillterByTitle_WhenBookExists_ShouldReturnListOfBooks()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            var expectedBook = new Book();
            expectedBook.Title = "C# Programming";
            expectedBook.Author = "Author1";
            Library.Books = new List<Book>
            {
                expectedBook,
                new Book{ Title = "Java Programming", Author = "Author 2"}
            };

            var result = bookService.FillterByTitle("C#");

            Assert.Collection(result, item => Assert.Contains("C#", item.Title));
        }

        [Fact]
        public void FillterByAuthor_WhenBookExists_ShouldReturnListOfBook()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            var expectedBook = new Book();
            expectedBook.Title = "C# Programming";
            expectedBook.Author = "Author1";
            Library.Books = new List<Book>
            {
                expectedBook,
                new Book{ Title = "Java Programming", Author = "Author 2"}
            };

            var result = bookService.FillterByAuthor("Author1");

            Assert.Collection(result, item => Assert.Contains("Author1", item.Author));
        }

        [Fact]
        public void FillterByPublicationYear_WhenBookExist_ShouldReturnListOfBook()

        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            var expectedBook = new Book();
            expectedBook.Title = "C# Programming";
            expectedBook.Author = "Author1";
            expectedBook.PublicationYear = new DateTime(2023, 1, 1);
            Library.Books = new List<Book>
            {
                expectedBook,
            };

            var result = bookService.FillterByPublicationYear(2023);

            Assert.Collection(result, item => Assert.Equal(2023, item.PublicationYear.Year));
        }

        [Fact]
        public void FillterByGenre_WhenBookExist_ShouldReturnListOfBook()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);
            var expectedBook = new Book();
            expectedBook.Title = "C# Programming";
            expectedBook.Author = "Author1";
            expectedBook.Genre = LibraryManagement.Helper.LibraryEnum.BookGenre.Action;
            Library.Books = new List<Book>
            {
                expectedBook,
            };

            var result = bookService.FillterByGenre("Action");

            Assert.Collection(result, item => Assert.Equal("Action", item.Genre.ToString()));
        }

        [Fact]
        public void GetBookById_ShouldReturnBook_WhenIdIs1()
        {
            // Arrange
            var bookRepository = new BookRepository();
            var expectedBook = new Book();
            expectedBook.Id = 1;
            expectedBook.Title = "C# Programming";
            bookRepository.AddNew(Library.Books, expectedBook);

            // Act
            var result = bookRepository.GetBookById(1);

            // Assert
            Assert.Equal(expectedBook.Id, result.Id);
            Assert.Equal(expectedBook.Title, result.Title);
            Assert.Equal(expectedBook.Author, result.Author);
            Assert.Equal(expectedBook.PublicationYear, result.PublicationYear);

        }
    }
}