﻿using LibraryManagement.IRepository;
using LibraryManagement.Model.Entities;
using LibraryManagement.Repository.RepositoryImplement;
using LibraryManagement.Service.IService;
using LibraryManagement.Service.ServiceImplement;

namespace UnitTestLibraryManagement
{
    public class BorrowBookTest
    {
        [Fact]
        public void BorrowBook_ListBorrowShouldAddBorrowBook()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);

            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);

            IBorrowBookService borrowBookService = new BorrowBookService(bookRepository, customerRepository);

            int bookId = 1;
            int customerId = 1;
            int borrowId = 1;
            BorrowBook borrowBook = new BorrowBook();
            borrowBook.PayDay = DateTime.Now;
            borrowBook.BookQuantity = 1;

            borrowBookService.BorrowBook(bookId, borrowBook, customerId);

            var list = Library.BorrowedBook;

            Assert.Collection(list, item => Assert.Equal(1, item.BorrowId));
        }

        [Fact]
        public void ReturnBook_WhenBorrowExists_ShouldReturnBookAndRemoveFromBorrowedBooks()
        {
            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);

            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);

            IBorrowBookService borrowBookService = new BorrowBookService(bookRepository, customerRepository);

            var borrowedBook = new BorrowBook();
            borrowedBook.BorrowId = 1;
            borrowedBook.CustomerId = 1;
            borrowedBook.BookId = 1;
            borrowedBook.BookQuantity = 7;
            var book = new Book();
            book.Id = 1;
            book.Author = "Test";
            book.Quantity = 0;
            Library.Books = new List<Book> { book };
            Library.BorrowedBook = new List<BorrowBook> { borrowedBook };

            // Act

            borrowBookService.ReturnBook(1);

            // Assert
            Assert.Equal(7, book.Quantity); // Assuming initial book quantity is 5 and borrowed quantity is 2.
            Assert.Empty(Library.BorrowedBook);
        }

        [Fact]
        public void FindBorrowedBookByCustomerName_WhenCustomerExists_ShouldReturnListOfCustomerIds()
        {
            // Arrange

            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);

            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);

            IBorrowBookService borrowBookService = new BorrowBookService(bookRepository, customerRepository);
            var customerId = 1;
            var customer = new Customer();
            customer.Id = customerId;
            customer.Name = "Test";
            Library.Customers = new List<Customer> { customer };

            // Act
            var result = borrowBookService.FindBorrowedBookByCustomerName("Test");

            // Assert
            Assert.Collection(result, id => Assert.Equal(customerId, id));
        }

        [Fact]
        public void FindBorrowedBookByBookName_WhenBookExists_ShouldReturnListOfBookIds()
        {
            // Arrange

            IBookRepository bookRepository = new BookRepository();
            IBookService bookService = new BookService(bookRepository);

            ICustomerRepository customerRepository = new CustomerRepository();
            ICustomerService customerService = new CustomerService(customerRepository);

            IBorrowBookService borrowBookService = new BorrowBookService(bookRepository, customerRepository);
            var bookId = 1;
            var book = new Book();
            book.Id = bookId;
            book.Title = "Test";
            Library.Books = new List<Book> { book };

            // Act
            var result = borrowBookService.FindBorrowedBookByBookName("Test");

            // Assert
            Assert.Collection(result, id => Assert.Equal(bookId, id));
        }

    }
}
