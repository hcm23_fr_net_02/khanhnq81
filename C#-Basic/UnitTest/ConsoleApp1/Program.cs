﻿var o = new A<string, int>();
o.a = "qq";
o.b = 1;
Console.WriteLine(o.a);
Console.WriteLine(o.b);
public class A<T, K>
{
    public T a { get; set; }
    public K b { get; set; }
}