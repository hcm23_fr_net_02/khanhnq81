﻿public class UnitTest1
{
    [Fact]
    public void GetDayOfWeek_ShouldReturnMonday_WhenParamIs1()
    {
        // arrange - chuẩn bị test
        A a = new A();
        var param = 1;
        var expected = "Monday";
        // act - chạy test
        var actual = a.GetDayOfWeek(param);
        // assert - kiểm tra kết quả sau khi chạy
        Assert.Equal(expected, actual);
    }
}