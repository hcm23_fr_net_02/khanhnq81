﻿using IntegerQueue;

DemoQueue queue = new DemoQueue(5);
int choice = 0;
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine("Enter number to enqueue: ");
                int number = int.Parse(Console.ReadLine());
                queue.Enqueue(number);
                break;
            case 2:
                var dequeueNumber = queue.Dequeue();
                Console.WriteLine($"Dequeue: {dequeueNumber}");
                break;
            case 3:
                var peekNumber = queue.Peek();
                Console.WriteLine($"Peek: {peekNumber}");
                break;
            case 4:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);