﻿namespace IntegerQueue
{
    public class DemoQueue
    {
        int[] integerQueue;
        int _maxQueue;
        int top = 0;
        int bot = -1;

        public DemoQueue(int maxQueue)
        {
            _maxQueue = maxQueue;
            integerQueue = new int[maxQueue];
        }

        public void Enqueue(int number)
        {
            bot++;
            if(bot > _maxQueue)
            {
                throw new Exception("Queue is full");
            }
            integerQueue[bot] = number;
        }
        public int Dequeue()
        {
            var valueAtBot = integerQueue[top];
            if(top == bot)
            {
                top = 0;
                bot = -1;
                throw new Exception("Queue is empty");
            }
            top++;
            return valueAtBot;
        }

        public int Peek()
        {
            var valueAtBot = integerQueue[bot];
            return valueAtBot;
        }
    }
}
