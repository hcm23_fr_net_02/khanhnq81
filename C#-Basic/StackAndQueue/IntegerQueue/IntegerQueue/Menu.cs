﻿namespace IntegerQueue
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("================MENU================");
            Console.WriteLine("| 1. Enqueue                       |");
            Console.WriteLine("| 2. Dequeue                       |");
            Console.WriteLine("| 3. Peek                          |");
            Console.WriteLine("| 4. Exit                          |");
            Console.WriteLine("====================================");
            Console.Write("Choice: ");
        }
    }
}
