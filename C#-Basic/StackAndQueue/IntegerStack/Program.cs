﻿using IntegerStack;

DemoStack stack = new DemoStack(5);
int choice = 0;
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine("Push a number: ");
                int number = int.Parse(Console.ReadLine());
                stack.Push(number);
                break;
            case 2:
                int popNumber = stack.Pop();
                Console.WriteLine($"Pop out : {popNumber}");
                break;
            case 3:
                int peekNumber = stack.Peek();
                Console.WriteLine($"Peek number: {peekNumber}");
                break;
            case 4:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);