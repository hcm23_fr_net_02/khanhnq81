﻿namespace IntegerStack
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("===========MENU===========");
            Console.WriteLine("| 1. Push                |");
            Console.WriteLine("| 2. Pop                 |");
            Console.WriteLine("| 3. Peek                |");
            Console.WriteLine("| 4. Exit                |");
            Console.WriteLine("==========================");
            Console.Write("Choice: ");
        }
    }
}
