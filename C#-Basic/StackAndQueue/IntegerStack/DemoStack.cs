﻿namespace IntegerStack
{
    public class DemoStack
    {
        int[] intArr;
        int top = -1;
        int _maxStack;
        public DemoStack(int maxStack)
        {
            _maxStack = maxStack;
            intArr = new int[maxStack];
        }

        public void Push(int value)
        {
            top++;
            if(top == _maxStack)
            {
                top--;
                throw new Exception("Stack is full");
            }
            intArr[top] = value;
        }
        public int Pop()
        {
            var valueAtTop = intArr[top];
            if(top == -1)
            {
                throw new Exception("Stack is empty");
            }
            top--;
            return valueAtTop;
        }

        public int Peek()
        {
            var valueAtTop = intArr[top];
            return valueAtTop;
        }
    }
}
