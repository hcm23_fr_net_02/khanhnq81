USE AdventureWorks2008R2
GO

--Q1
SELECT p.Name 
FROM Production.Product p
WHERE p.ProductSubcategoryID IN (SELECT p2.ProductSubcategoryID FROM Production.ProductSubcategory p2 WHERE p2.Name = 'Saddles')
--Q2
SELECT p.Name 
FROM Production.Product p
WHERE p.ProductSubcategoryID IN (SELECT p2.ProductSubcategoryID FROM Production.ProductSubcategory p2 WHERE p2.Name LIKE 'Bo%')
--Q3
SELECT p.Name, MIN(p.ListPrice)
FROM Production.Product p
WHERE p.ListPrice = ANY (SELECT MIN(p2.ListPrice) FROM Production.Product p2 WHERE p2.ProductSubcategoryID =3)
GROUP BY p.Name
--Q4
SELECT pc.Name
FROM Person.CountryRegion pc
INNER JOIN Person.StateProvince ps ON pc.CountryRegionCode = ps.CountryRegionCode
GROUP BY pc.Name
HAVING COUNT(ps.CountryRegionCode) < 10

SELECT pc.Name
FROM Person.CountryRegion pc
WHERE pc.CountryRegionCode = ANY (SELECT ps.CountryRegionCode FROM Person.StateProvince ps GROUP BY ps.CountryRegionCode HAVING COUNT(ps.CountryRegionCode) < 10)

--Q5
SELECT
FROM Sales.SalesOrderHeader 