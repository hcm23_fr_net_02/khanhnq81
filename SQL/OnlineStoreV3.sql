﻿USE master
GO

DROP DATABASE IF EXISTS OnlineStoreV3
GO

CREATE DATABASE OnlineStoreV3
GO

USE OnlineStoreV3
GO

CREATE TABLE Customers
(
	ID INT PRIMARY KEY IDENTITY (1,1),
	FullName NVARCHAR (100),
	Email NVARCHAR (100),
	Address NVARCHAR (100)
)

CREATE TABLE Products
(
	ID INT PRIMARY KEY IDENTITY (1,1),
	ProductName NVARCHAR(100),
	Price DECIMAL(10,2) CHECK (Price > 0),
	StockQuantity INT CHECK (StockQuantity >= 0)
)

CREATE TABLE Orders
(
	ID INT PRIMARY KEY IDENTITY (1,1),
	CustomerID INT FOREIGN KEY REFERENCES Customers(ID),
	OrderDate DATETIME,
	TotalPrice DECIMAL(10,2)
)

CREATE TABLE OrderDetails
(
	ID INT PRIMARY KEY IDENTITY (1,1),
	OrderID INT FOREIGN KEY REFERENCES Orders(ID),
	ProductID INT FOREIGN KEY REFERENCES Products(ID),
	Quantity INT
)

--Insert records into Customers
insert into Customers (FullName, Email, Address) values ('Elisha Saiger', 'esaiger0@slashdot.org', 'Room 977');
insert into Customers (FullName, Email, Address) values ('Hans Mackstead', 'hmackstead1@csmonitor.com', 'PO Box 477');
insert into Customers (FullName, Email, Address) values ('Oriana Ciccerale', 'ociccerale2@dmoz.org', 'Suite 37');
insert into Customers (FullName, Email, Address) values ('Hoebart Dax', 'hdax3@nhs.uk', 'Room 209');
insert into Customers (FullName, Email, Address) values ('Nonie Tezure', 'ntezure4@google.com.hk', 'Suite 50');
insert into Customers (FullName, Email, Address) values ('Margot Linnane', 'mlinnane5@ox.ac.uk', 'Suite 49');
insert into Customers (FullName, Email, Address) values ('Barnabe Kebbell', 'bkebbell6@i2i.jp', 'Apt 1344');
insert into Customers (FullName, Email, Address) values ('Gilbertina Daughtry', 'gdaughtry7@chron.com', 'Room 41');
insert into Customers (FullName, Email, Address) values ('Edwin Sigart', 'esigart8@accuweather.com', 'Suite 49');
insert into Customers (FullName, Email, Address) values ('Veriee Verni', 'vverni9@people.com.cn', 'Apt 1308');
--Insert records into Products
insert into Products (ProductName, Price, StockQuantity) values ('Sauce - Marinara', 1101.1, 27);
insert into Products (ProductName, Price, StockQuantity) values ('Onions - Dried, Chopped', 1609.64, 96);
insert into Products (ProductName, Price, StockQuantity) values ('Water - Tonic', 1507.95, 0);
insert into Products (ProductName, Price, StockQuantity) values ('Cheese - Roquefort Pappillon', 1022.88, 11);
insert into Products (ProductName, Price, StockQuantity) values ('Wine - Sauvignon Blanc', 1288.19, 64);
insert into Products (ProductName, Price, StockQuantity) values ('Beer - Heinekin', 1958.43, 81);
insert into Products (ProductName, Price, StockQuantity) values ('Wine - Rubyport', 1386.11, 31);
insert into Products (ProductName, Price, StockQuantity) values ('Cheese - St. Andre', 1340.71, 85);
insert into Products (ProductName, Price, StockQuantity) values ('Skirt - 24 Foot', 1590.0, 87);
insert into Products (ProductName, Price, StockQuantity) values ('Sugar Thermometer', 1457.88, 10);
--Insert records into Orders
insert into Orders (CustomerID, OrderDate, TotalPrice) values (2, '01/09/2023', 3962.31);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (7, '11/24/2022', 2139.71);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (6, '10/12/2022', 3888.2);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (3, '01/06/2023', 2156.07);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (9, '01/10/2023', 2914.27);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (8, '01/20/2023', 3677.65);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (4, '01/23/2023', 3448.44);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (1, '04/19/2023', 2892.94);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (10, '08/14/2023', 3394.22);
insert into Orders (CustomerID, OrderDate, TotalPrice) values (1, '10/21/2022', 2326.67);
--Insert records into OrderDetails
insert into OrderDetails (OrderID, ProductID, Quantity) values (2, 1, 2);
insert into OrderDetails (OrderID, ProductID, Quantity) values (3, 1, 7);
insert into OrderDetails (OrderID, ProductID, Quantity) values (9, 7, 1);
insert into OrderDetails (OrderID, ProductID, Quantity) values (6, 4, 5);
insert into OrderDetails (OrderID, ProductID, Quantity) values (6, 10, 5);
insert into OrderDetails (OrderID, ProductID, Quantity) values (3, 8, 3);
insert into OrderDetails (OrderID, ProductID, Quantity) values (4, 10, 7);
insert into OrderDetails (OrderID, ProductID, Quantity) values (9, 10, 9);
insert into OrderDetails (OrderID, ProductID, Quantity) values (9, 7, 10);
insert into OrderDetails (OrderID, ProductID, Quantity) values (3, 10, 9);
--3.Truy vấn SQL để lấy danh sách tên đầy đủ và email của tất cả khách hàng có địa chỉ chứa thông tin là ở thành phố hoặc tỉnh bắt đầu bằng chữ "H" 
--(ví dụ: Hà Nội, Hồ Chí Minh, Hà Tĩnh...) và sắp xếp theo tên đầy đủ của khách hàng tăng dần (10 điểm).
SELECT c.ID, c.FullName, c.Email, c.Address
FROM Customers c
WHERE c.Address LIKE 'H%'
--4.Truy vấn SQL để lấy danh sách tên sản phẩm và giá của tất cả sản phẩm có giá lớn hơn 100 và số lượng trong kho ít hơn 50, sắp xếp theo giá sản phẩm giảm dần (10 điểm).
SELECT p.ID, p.ProductName, p.Price, p.StockQuantity
FROM Products p
WHERE p.Price > 100 AND p.StockQuantity < 50
ORDER BY p.Price DESC
--5. Truy vấn SQL để lấy danh sách tên đầy đủ và email của tất cả khách hàng chưa đặt đơn hàng nào, sắp xếp theo tên đầy đủ của khách hàng (tăng dần) .(10 điểm)
SELECT c.ID, c.FullName, c.Email, c.Address
FROM Customers c
LEFT JOIN Orders o ON c.ID = o.CustomerID
WHERE o.ID IS NULL
--6. Truy vấn SQL để lấy tên sản phẩm (ProductName), giá sản phẩm (Price), số lượng (Quantity) 
--Thông tin về các sản phẩm đã được mua trong các đơn hàng có giá trị tổng (TotalPrice) lớn hơn 500,000. (10 điểm)
SELECT p.ID, p.ProductName, p.Price, p.StockQuantity
FROM Products p
INNER JOIN OrderDetails od ON p.ID = od.ProductID
INNER JOIN Orders o ON od.OrderID = o.ID
WHERE o.TotalPrice > 500.000
--7.Lấy danh sách tên khách hàng và tháng. Mà trong tháng đó khách hàng này đó đã chi tiêu hơn 500,000 đồng cho tổng các đơn hàng của họ.(20 điểm)
SELECT c.ID, c.FullName, MONTH(o.OrderDate) as 'Month', SUM(o.TotalPrice) AS 'Total Price', COUNT(o.CustomerID) AS 'Number of Order'
FROM Customers c
INNER JOIN Orders o ON c.ID = o.CustomerID
GROUP BY c.ID, c.FullName, MONTH(o.OrderDate)
HAVING SUM(o.TotalPrice) > 500.000
