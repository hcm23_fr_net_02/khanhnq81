USE master
GO

DROP DATABASE IF EXISTS DMS
GO

CREATE DATABASE DMS
GO

USE DMS
GO

CREATE TABLE [dbo].[EMPMAJOR](
	[emp_no] [char](6) NOT NULL,
	[major] [char](3) NOT NULL,
	[major_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Major] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[major] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMP]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMP](
	[emp_no] [char](6) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[dept_no] [char](3) NOT NULL,
	[job] [varchar](50) NULL,
	[salary] [money] NOT NULL,
	[bonus] [money] NULL,
	[ed_level] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEPT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DEPT](
	[dept_no] [char](3) NOT NULL,
	[dept_name] [varchar](50) NOT NULL,
	[mgn_no] [char](6) NULL,
	[admr_dept] [char](3) NOT NULL,
	[location] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[dept_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[dept_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMPPROJACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMPPROJACT](
	[emp_no] [char](6) NOT NULL,
	[proj_no] [char](6) NOT NULL,
	[act_no] [int] NOT NULL,
 CONSTRAINT [PK_EPA] PRIMARY KEY CLUSTERED 
(
	[emp_no] ASC,
	[proj_no] ASC,
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ACT]    Script Date: 06/01/2015 17:22:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT](
	[act_no] [int] NOT NULL,
	[act_des] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[act_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Dept]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[DEPT]  WITH CHECK ADD  CONSTRAINT [FK_Dept] FOREIGN KEY([mgn_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[DEPT] CHECK CONSTRAINT [FK_Dept]
GO
/****** Object:  ForeignKey [FK__EMP__dept_no__3E52440B]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMP]  WITH CHECK ADD FOREIGN KEY([dept_no])
REFERENCES [dbo].[DEPT] ([dept_no])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_Major]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPMAJOR]  WITH CHECK ADD  CONSTRAINT [FK_Major] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPMAJOR] CHECK CONSTRAINT [FK_Major]
GO
/****** Object:  ForeignKey [FK_EPA1]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA1] FOREIGN KEY([emp_no])
REFERENCES [dbo].[EMP] ([emp_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA1]
GO
/****** Object:  ForeignKey [FK_EPA2]    Script Date: 06/01/2015 17:22:33 ******/
ALTER TABLE [dbo].[EMPPROJACT]  WITH CHECK ADD  CONSTRAINT [FK_EPA2] FOREIGN KEY([act_no])
REFERENCES [dbo].[ACT] ([act_no])
GO
ALTER TABLE [dbo].[EMPPROJACT] CHECK CONSTRAINT [FK_EPA2]
GO

INSERT INTO [dbo].[EMPMAJOR] ([emp_no], [major], [major_name])
VALUES
    ('EMP001', 'M01', 'Computer Science'),
    ('EMP002', 'M03', 'Mechanical Engineering'),
    ('EMP003', 'M01', 'Computer Science'),
    ('EMP004', 'M02', 'Electrical Engineering'),
    ('EMP005', 'M05', 'Chemistry'),
    ('EMP006', 'M06', 'Math');

 

-- Insert records into EMP
INSERT INTO [dbo].[EMP] ([emp_no], [last_name], [first_name], [dept_no], [job], [salary], [bonus], [ed_level])
VALUES
    ('EMP001', 'Smith', 'John', 'D01', 'Manager', 75000.00, 5000.00, 4),
    ('EMP002', 'Johnson', 'Alice', 'D02', 'Engineer', 65000.00, NULL, 3),
    ('EMP003', 'Williams', 'David', 'D03', 'Analyst', 60000.00, 3000.00, 4),
    ('EMP004', 'Jones', 'Mary', 'D02', 'Engineer', 70000.00, 2500.00, 3),
    ('EMP005', 'Brown', 'Sarah', 'D01', 'Manager', 80000.00, 6000.00, 4),
    ('EMP006', 'Davis', 'Michael', 'D03', 'Analyst', 62000.00, 3500.00, 4),
    ('EMP007', 'Wilson', 'Laura', 'D02', 'Engineer', 67000.00, NULL, 3),
    ('EMP008', 'Taylor', 'Robert', 'D01', 'Manager', 78000.00, 5500.00, 4);

 

-- Insert records into DEPT
INSERT INTO [dbo].[DEPT] ([dept_no], [dept_name], [mgn_no], [admr_dept], [location])
VALUES
    ('D01', 'Engineering', NULL, 'D01', 'Building A'),
    ('D02', 'Research', NULL, 'D01', 'Building B'),
    ('D03', 'Development', NULL, 'D01', 'Building C'),
    ('D04', 'Sales', NULL, 'D04', 'Building D'),
    ('D05', 'Marketing', NULL, 'D04', 'Building E');

 

-- Insert records into EMPPROJACT
INSERT INTO [dbo].[EMPPROJACT] ([emp_no], [proj_no], [act_no])
VALUES
    ('EMP001', 'PROJ01', 1),
    ('EMP001', 'PROJ02', 2),
    ('EMP002', 'PROJ01', 1),
    ('EMP003', 'PROJ03', 3),
    ('EMP003', 'PROJ02', 2),
    ('EMP004', 'PROJ04', 4),
    ('EMP005', 'PROJ05', 5),
    ('EMP006', 'PROJ03', 3);

 

-- Insert records into ACT
INSERT INTO [dbo].[ACT] ([act_no], [act_des])
VALUES
    (1, 'Design'),
    (2, 'Coding'),
    (3, 'Testing'),
    (4, 'Documentation'),
    (5, 'Review');

SELECT * FROM ACT
SELECT * FROM DEPT
SELECT * FROM EMP
SELECT * FROM EMPMAJOR
SELECT * FROM EMPPROJACT

--Q2--Find employees who are currently working on a project or projects. Employees working on projects will have a row(s) on the EMPPROJACT table.
SELECT e.emp_no, e.last_name, e.first_name, e.job 
FROM EMP e 
INNER JOIN EMPPROJACT ep ON e.emp_no = ep.emp_no
GROUP BY e.emp_no, e.last_name, e.first_name, e.job 
--Q3--Find all employees who major in math (MAT) and computer science (CSI)
SELECT e.emp_no, e.first_name, e.last_name
FROM EMP e
INNER JOIN [EMPMAJOR] em ON e.emp_no = em.emp_no
WHERE em.major_name = 'Math' OR em.major_name = 'Computer Science'
--Q4--Find employees who work on all activities between 90 and 110
SELECT e.emp_no, e.first_name, e.last_name
FROM EMP e
--Q5--Provide a report of employees with employee detail information along with department aggregate information. Give >=2 solutions (Scalar Fullselect, Join, CTE, ect).
SELECT e.emp_no AS EMPNO, e.last_name AS LASTNAME, e.first_name AS FIRSTNAME, e.salary AS SALARY, e.dept_no AS DEPTNO, AVG(e.salary)
FROM EMP e
GROUP BY e.emp_no , e.last_name, e.first_name , e.salary , e.dept_no;
--Q6--Use CTE technique to provide a report of employees whose education levels are higher than the average education level of their respective department.
WITH DeptAvgEdLevel AS (
    SELECT e.dept_no, AVG(e.ed_level) AS AvgEdLevel
    FROM dbo.EMP e
    GROUP BY e.dept_no
)

SELECT e.emp_no AS EMPNO, e.last_name AS LASTNAME, e.first_name AS FIRSTNAME, e.ed_level AS ED_LEVEL, e.dept_no AS DEPTNO
FROM dbo.EMP e
JOIN DeptAvgEdLevel DAE ON e.dept_no = DAE.dept_no
WHERE e.ed_level > DAE.AvgEdLevel
--Q7--Return the department number, department name and the total payroll for the department that has the highest payroll. Payroll will be defined as the sum of all salaries and bonuses for the department.
SELECT TOP 1 d.dept_no, d.dept_name, SUM(e.salary + ISNULL(e.bonus, 0)) AS TotalPayroll
FROM DEPT d
LEFT JOIN EMP e ON d.dept_no = e.dept_no
GROUP BY d.dept_no, d.dept_name
ORDER BY TotalPayroll DESC
--Q8--Return the employees with the top 5 salaries.Could be 5 employees with different salaries. Could be many employees having the same salaries.
SELECT TOP 5 e.emp_no, e.last_name, e.first_name, e.salary 
FROM EMP e
ORDER BY salary DESC
