USE AdventureWorks2008R2
go

---Q1
SELECT p.ProductID, p.Name, p.Color, p.ListPrice
FROM Production.Product p
---Q2
SELECT p.ProductID, p.Name, p.Color, p.ListPrice
FROM Production.Product p
WHERE p.ListPrice > 0
--Q3
SELECT p.ProductID, p.Name, p.Color, p.ListPrice
FROM Production.Product p
WHERE p.Color is null
--Q4
SELECT p.ProductID, p.Name, p.Color, p.ListPrice
FROM Production.Product p
WHERE p.Color is not null
--Q5
SELECT p.ProductID, p.Name, p.Color, p.ListPrice
FROM Production.Product p
WHERE p.Color is not null AND p.ListPrice > 0
--Q6
SELECT p.Name, p.Color
FROM Production.Product p
WHERE p.Color is not null 
--Q7
SELECT CONCAT('NAME: ',p.Name) AS NAME, CONCAT('COLOR: ', p.Color) AS COLOR
FROM Production.Product p
WHERE p.Color is not null 
--Q8
SELECT p.ProductID, p.Name
FROM Production.Product p
WHERE p.ProductID >= 400 AND p.ProductID <=500
--Q9
SELECT p.ProductID, p.Name, p.Color
FROM Production.Product p
WHERE p.Color = 'Black' OR p.Color = 'Blue'
--Q10
SELECT p.Name, p.ListPrice
FROM Production.Product p
WHERE p.Name LIKE 'S%'
--Q11
SELECT p.Name, p.ListPrice
FROM Production.Product p
WHERE p.Name LIKE 'S%' OR p.Name LIKE 'A%'
--Q12
SELECT p.Name, p.ListPrice
FROM Production.Product p
WHERE p.Name LIKE 'SPO%' AND p.Name NOT LIKE '%k%'
--Q13
SELECT DISTINCT p.Color
FROM Production.Product p
--Q14
SELECT DISTINCT p.ProductSubcategoryID, p.Color
FROM Production.Product p
WHERE p.Color IS NOT NULL AND p.ProductSubcategoryID IS NOT NULL
--Q15
SELECT p.ProductSubcategoryID, p.Name, p.Color, p.ListPrice
FROM Production.Product p
WHERE  p.ProductSubcategoryID = 1 AND p.Color IN ('Red','Black') OR ListPrice > = 1000 AND ListPrice <= 2000
--Q16
SELECT p.Name, ISNULL(p.Color,'Unknown'), p.ListPrice
FROM Production.Product p
----------------------------------EXERCISE2-----------------------------------------------
--Q1
SELECT COUNT(*)
FROM Production.Product
--Q2
SELECT COUNT(p.ProductSubcategoryID)
FROM Production.Product p
WHERE p.ProductSubcategoryID IS NOT NULL
--Q3
SELECT p.ProductSubcategoryID, COUNT(*) as 'Count Product'
FROM Production.Product p
GROUP BY p.ProductSubcategoryID
--Q4
SELECT COUNT(*)
FROM Production.Product p
WHERE p.ProductSubcategoryID IS NULL

SELECT COUNT(
			CASE 
				WHEN p.ProductSubcategoryID IS NULL THEN 1 
				ELSE NULL 
			END
			)
FROM Production.Product p;
--Q5
	SELECT p.ProductID, SUM(p.Quantity) AS InStock
	FROM Production.ProductInventory  p
	GROUP BY p.ProductID
--Q6
	SELECT p.ProductID, SUM(p.Quantity) AS InStock
	FROM Production.ProductInventory  p
	WHERE p.LocationID = 40
	GROUP BY p.ProductID
	HAVING SUM(p.Quantity) < 100
--Q7
	SELECT p.Shelf,p.ProductID, SUM(p.Quantity)
	FROM Production.ProductInventory  p
	WHERE p.LocationID = 40
	GROUP BY p.Shelf, p.ProductID
	HAVING SUM(p.Quantity) < 100
--Q8
	SELECT AVG(p.Quantity) as 'AVG'
	FROM Production.ProductInventory p
	WHERE p.LocationID = 10
--Q9
	SELECT p.Shelf,p.ProductID,AVG(p.Quantity) as 'AVG'
	FROM Production.ProductInventory p
	WHERE p.LocationID = 10
	GROUP BY p.Shelf, p.ProductID
--Q10
	SELECT p.Color, p.Class, COUNT(*) as 'The Count', AVG(p.ListPrice) as 'Price'
	FROM Production.Product p
	WHERE p.Color IS NOT NULL AND p.Class IS NOT NULL
	GROUP BY p.Color, p.Class
--Q11
	SELECT
    CASE
        WHEN GROUPING(ProductSubcategoryID) = 1 THEN NULL
        ELSE ProductSubcategoryID
    END AS ProductSubcategoryID,
    COUNT(Name) as Counted,
    GROUPING(ProductSubcategoryID) AS IsGrandTotal
FROM Production.Product
GROUP BY ROLLUP (ProductSubcategoryID);