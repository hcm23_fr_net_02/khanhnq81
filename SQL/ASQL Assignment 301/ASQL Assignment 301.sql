USE master
GO

DROP DATABASE IF EXISTS EMS
GO

CREATE DATABASE EMS
GO

USE EMS
GO

CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO 

insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (1, 'Desi', '03/25/2013', 1, 1, '07/04/2023', 1551, 1, 'Phasellus sit amet erat.', 2, 'doller0@paypal.com');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (2, 'Aretha', '08/29/2007', 2, 2, '06/13/2023', 1936, 0, 'Suspendisse potenti.', 1, 'aglasby1@constantcontact.com');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (3, 'Emelyne', '10/05/2008', 3, 3, '11/16/2022', 1189, 0, 'Vivamus tortor.', 4, 'ekeyho2@ocn.ne.jp');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (4, 'Elana', '06/21/2020', 1, 1, '01/02/2023', 1642, 0, 'Donec vitae nisi.', 1, 'ematresse3@nsw.gov.au');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (5, 'Frannie', '08/27/2009', 2, 2, '04/06/2023', 1956, 0, 'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 4, 'fchantree4@is.gd');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (6, 'Caldwell', '02/04/2016', 3, 3, '04/07/2023', 1065, 1, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1, 'cstallibrass5@twitter.com');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (7, 'Morgan', '01/19/2023', 2, 2, '07/21/2023', 1107, 0, 'Curabitur convallis.', 2, 'merangey6@cbc.ca');
insert into Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email) values (8, 'Pamella', '08/01/2002', 3, 1, '11/09/2022', 1493, 1, 'Nulla facilisi.', 2, 'peasterfield7@hatena.ne.jp');


insert into Department (DeptName, Note) values ('FTOWN 1', 'Pellentesque at nulla.');
insert into Department (DeptName, Note) values ('FTOWN 2', 'Pellentesque at nulla.');
insert into Department (DeptName, Note) values ('FTOWN 3', 'Proin leo odio.')

insert into Skill (SkillName, Note) values ('C#', 'Vestibulum ante ipsum primis in faucibus.');
insert into Skill (SkillName, Note) values ('C++', 'Mauris enim leo, rhoncus sed.');
insert into Skill (SkillName, Note) values ('JAVA', 'Donec dapibus.');


insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (1, 1, 2, '09/26/2023', 'Donec dapibus.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (2, 8, 1, '07/29/2023', 'Integer non velit.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (1, 2, 3, '04/30/2023', 'Cras in purus eu magna vulputate luctus.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (2, 4, 2, '04/27/2023', 'Maecenas ut massa quis augue luctus tincidunt.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (3, 3, 2, '06/23/2023', 'In eleifend quam a odio.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (2, 6, 3, '11/18/2022', 'Nam tristique tortor eu pede.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (2, 7, 1, '03/19/2023', 'Morbi quis tortor id nulla ultrices aliquet.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (1, 8, 4, '06/17/2023', 'Proin interdum mauris non ligula pellentesque ultrices.');


--Q2--Specify the name, email and department name of the employees that have been working at least six months.
SELECT e.EmpName, e.Email, d.DeptName, DATEDIFF(MONTH, e.StartDate, GETDATE()) AS 'Working Month'
FROM Employee e
INNER JOIN Department d ON e.DeptNo = d.DeptNo
GROUP BY e.EmpName, e.Email, d.DeptName, DATEDIFF(MONTH, e.StartDate, GETDATE())
HAVING DATEDIFF(MONTH, e.StartDate, GETDATE()) >= 6
--Q3--Specify the names of the employees whore have either �C++� or �.NET� skills.
SELECT e.EmpName, s.SkillName
FROM Employee e
INNER JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
INNER JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET'
--Q4--List all employee names, manager names, manager emails of those employees.
SELECT e.EmpName, m.EmpName AS 'Manager', m.Email AS 'Manager Email'
FROM Employee e
INNER JOIN Employee m ON m.EmpNo = e.MgrNo
--Q5--Specify the departments which have >=2 employees, print out the list of departments� employees right after each department.
SELECT d.DeptNo, d.DeptName, COUNT(*) as 'Number of Employee'
FROM Department d
LEFT JOIN Employee e ON d.DeptNo = e.DeptNo
GROUP BY d.DeptNo, d.DeptName
ORDER BY COUNT(*) ASC
--Q6--List all name, email and number of skills of the employees and sort ascending order by employee�s name.
SELECT e.EmpName, e.Email, COUNT(*) AS 'Number Of Skill'
FROM Employee e
LEFT JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName ASC
--Q7--Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills.
SELECT e.EmpNo, e.EmpName, e.Email, e.BirthDay
FROM Employee e
WHERE e.Status = 1 AND e.EmpNo = ANY (SELECT e2.EmpNo FROM Employee e2 INNER JOIN Emp_Skill es ON es.EmpNo = e2.EmpNo)
--Q8--Create a view to list all employees are working (include: name of employee and skill name, department name).
CREATE VIEW WorkingEmployee AS
SELECT e.EmpName, s.SkillName, d.DeptName
FROM Employee e
INNER JOIN Department d ON e.DeptNo = d.DeptNo
INNER JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
INNER JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE e.Status = 1


SELECT *FROM WorkingEmployee