USE master
GO

DROP DATABASE IF EXISTS ASQL_Assignment2_Opt1
GO

CREATE DATABASE ASQL_Assignment2_Opt1
GO

USE ASQL_Assignment2_Opt1
GO

CREATE TABLE Projects
(
	ProjectID INT PRIMARY KEY IDENTITY (1,1),
	ProjectName VARCHAR(50),
	ProjectStartDate DATETIME,
	ProjectDescription VARCHAR(255),
	ProjectDetails VARCHAR(255),
	ProjectCompleteOn DATETIME
)

CREATE TABLE Employee
(
	EmployeeID  INT PRIMARY KEY IDENTITY (1,1),
	EmployeeLastName NVARCHAR(50),
	EmployeeFirstName NVARCHAR(50),
	EmployeeHireDate DATETIME,
	EmployeeStatus BIT,
	SuperVisorID INT,
	SocialSecurityNumber INT
)

CREATE TABLE Projects_Modules
(
	ModuleID INT PRIMARY KEY IDENTITY (1,1),
	ProjectID INT FOREIGN KEY REFERENCES Projects(ProjectID) NOT NULL,
	EmployeeID INT FOREIGN KEY REFERENCES Employee(EmployeeID) NOT NULL,
	ProjectModuleDate DATETIME,
	ProjectModuleCompleteON DATETIME,
	ProjectModuleDescription VARCHAR(255)
)


CREATE TABLE Work_Done
(
	WorkDoneID INT PRIMARY KEY,
	ModuleID INT NOT NULL,
	WorkDoneDate DATETIME,
	WorkDoneDescription VARCHAR(255),
	WorkDoneStatus BIT
)
ALTER TABLE Work_Done ADD FOREIGN KEY (ModuleID) REFERENCES Projects_Modules(ModuleID) 
ALTER TABLE Work_Done ADD EmployeeID INT FOREIGN KEY REFERENCES Employee(EmployeeID)
ALTER TABLE Projects ADD ManagerID INT FOREIGN KEY REFERENCES Employee(EmployeeID)

insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SuperVisorID, SocialSecurityNumber) values ('Mandrier', 'Alford', '2/25/2015', 1, NULL, '93495');
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SuperVisorID, SocialSecurityNumber) values ('Tedstone', 'Maurie', '8/16/2022', 0, 1, '270');
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SuperVisorID, SocialSecurityNumber) values ('Windybank', 'Joanne', '8/31/2019', 1, 1, '9');
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SuperVisorID, SocialSecurityNumber) values ('Dearsley', 'Donny', '6/27/2012', 1, 1, '717');
insert into Employee (EmployeeLastName, EmployeeFirstName, EmployeeHireDate, EmployeeStatus, SuperVisorID, SocialSecurityNumber) values ('Goschalk', 'Robinett', '11/15/2014', 1, 1, '9');

insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Fisher, Cole and Cummerata', '7/1/2023', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', 'In hac habitasse platea dictumst.', '9/20/2023', 1);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Kihn-Thiel', '7/22/2023', 'Duis ac nibh.', 'Vivamus tortor.', '9/10/2023', 2);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Mayert, McDermott and Homenick', '8/17/2023', 'Morbi non lectus.', 'Quisque ut erat.', '5/29/2023', 1);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Volkman-Williamson', '4/27/2023', 'Curabitur at ipsum ac tellus semper interdum.', 'Nulla suscipit ligula in lacus.', '1/15/2023', 1);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Rau, Baumbach and Ebert', '9/24/2023', 'Phasellus sit amet erat.', 'Maecenas ut massa quis augue luctus tincidunt.', '5/12/2023', 2);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Fadel-Ruecker', '7/24/2023', 'In hac habitasse platea dictumst.', 'Nam tristique tortor eu pede.', '2/15/2023', 2);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Bruen Group', '1/18/2023', 'Quisque porta volutpat erat.', 'Aenean fermentum.', '4/16/2023', 1);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Schamberger-Hegmann', '5/4/2023', 'Nam nulla.', 'Integer tincidunt ante vel ipsum.', '8/11/2023', 1);
insert into Projects (ProjectName, ProjectStartDate, ProjectDescription, ProjectDetails, ProjectCompleteOn, ManagerID) values ('Bayer, Sanford and Mante', '6/14/2023', 'Proin interdum mauris non ligula pellentesque ultrices.', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.', '12/14/2022', 2);

insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (3, 5, '2/5/2021', NULL, 'In sagittis dui vel nisl.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (8, 5, '5/9/2022', NULL, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (3, 1, '11/8/2021', NULL, 'Nulla ut erat id mauris vulputate elementum.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (1, 2, '2/26/2021', NULL, 'Curabitur convallis.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (2, 5, '2/28/2022', NULL, 'Donec posuere metus vitae ipsum.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (4, 3, '8/28/2021', NULL, 'Cras pellentesque volutpat dui.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (4, 2, '9/25/2021', NULL, 'Nunc nisl.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (4, 4, '1/15/2022', NULL, 'Mauris lacinia sapien quis libero.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (4, 2, '7/1/2021', NULL, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (7, 5, '2/12/2021','11/18/2022', 'Suspendisse potenti.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (6, 4, '9/23/2022','10/21/2022', 'Duis ac nibh.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (3, 4, '2/9/2021', '2/27/2023', 'Nulla tempus.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (6, 1, '2/18/2021', '9/13/2023', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.');
insert into Projects_Modules (ProjectID, EmployeeID, ProjectModuleDate, ProjectModuleCompleteON, ProjectModuleDescription) values (4, 3, '5/9/2022', '10/15/2022', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.');

insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (1, 9, '4/21/2022', 'Nulla tellus.', 0, 2);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (2, 11, '10/25/2021', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1, 2);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (3, 3, '5/14/2022', 'Morbi ut odio.', 1, 1);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (4, 4, '5/1/2021', 'Curabitur gravida nisi at nibh.', 1, 3);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (5, 13, '1/27/2021', 'Proin at turpis a pede posuere nonummy.', 1, 5);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (6, 12, '10/10/2020', 'Etiam pretium iaculis justo.', 1, 5);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (7, 6, '12/26/2020', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.', 0, 2);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (8, 14, '1/30/2022', 'Cras non velit nec nisi vulputate nonummy.', 1, 5);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (9, 10, '12/30/2021', 'Aliquam sit amet diam in magna bibendum imperdiet.', 1, 3);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (10, 4, '10/15/2020', 'Maecenas tincidunt lacus at velit.', 0, 4);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (11, 6, '12/31/2021', 'Nullam varius.', 1, 1);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (12, 12, '9/2/2021', 'Nullam sit amet turpis elementum ligula vehicula consequat.', 1, 1);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (13, 6, '2/2/2022', 'Fusce consequat.', 1, 3);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (14, 7, '3/5/2021', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.', 0, 4);
insert into Work_Done (WorkDoneID, ModuleID, WorkDoneDate, WorkDoneDescription, WorkDoneStatus, EmployeeID) values (15, 8, '9/30/2020', 'Quisque id justo sit amet sapien dignissim vestibulum.', 0, 1);

SELECT * FROM Employee
SELECT * FROM Projects
SELECT * FROM Projects_Modules
SELECT * FROM Work_Done

--Write a stored procedure (with parameter) to print out the modules that a specific employee has been working on.
CREATE PROCEDURE GetModulesForEmployee
    @EmployeeID INT
AS
BEGIN
    SELECT pm.ModuleID, p.ProjectName, pm.ProjectModuleDate, pm.ProjectModuleDescription
    FROM Projects_Modules pm
    INNER JOIN Projects p ON pm.ProjectID = p.ProjectID
    WHERE pm.EmployeeID = @EmployeeID;
END

EXEC GetModulesForEmployee @EmployeeID = 1
--Write a user function (with parameter) that return the Works information that a specific employee has been involving though those were not assigned to him/her.
CREATE FUNCTION GetWorkForEmployee
(
    @EmployeeID INT
)
RETURNS TABLE
AS
RETURN
(
    SELECT wd.WorkDoneID, wd.ModuleID, pm.ProjectID, p.ProjectName, wd.WorkDoneDate, wd.WorkDoneDescription, wd.WorkDoneStatus
    FROM Work_Done wd
    INNER JOIN Projects_Modules pm ON wd.ModuleID = pm.ModuleID
    INNER JOIN Projects p ON pm.ProjectID = p.ProjectID
    WHERE pm.EmployeeID != @EmployeeID
);

SELECT * FROM dbo.GetWorkForEmployee(1);
--Write the trigger(s) to prevent the case that the end user to input invalid Projects and Project Modules information
CREATE TRIGGER Projects_Modules_Validate
ON Projects_Modules
FOR INSERT, UPDATE
AS
BEGIN
    IF EXISTS (
        SELECT 1
        FROM inserted i
        LEFT JOIN Projects p ON i.ProjectID = p.ProjectID
        LEFT JOIN Employee e ON i.EmployeeID = e.EmployeeID
        WHERE p.ProjectID IS NULL OR e.EmployeeID IS NULL
    )
    BEGIN
        ROLLBACK TRANSACTION;
        RETURN;
    END
END;


