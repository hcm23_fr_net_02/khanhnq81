USE master 
GO

DROP DATABASE IF EXISTS MOVIECOLLECTION
GO

CREATE DATABASE MOVIECOLLECTION
GO

USE MOVIECOLLECTION
GO

CREATE TABLE Movie
(
	MovieID int PRIMARY KEY IDENTITY (1,1),
	MovieName NVARCHAR (50),
	Duration INT CHECK(Duration >= 60),
	Genre TINYINT CHECK (Genre >=1 AND Genre <= 8),
	Director NVARCHAR (50),
	AmountOfMoney MONEY
)

CREATE TABLE Actor
(
	ActorID INT PRIMARY KEY IDENTITY(1,1),
	ActorName NVARCHAR(50),
	Age TINYINT,
	AvgMovieSalary FLOAT,
	Nationaly NVARCHAR(50)
)

CREATE TABLE ActedIn 
(
	MovieID INT FOREIGN KEY REFERENCES Movie(MovieID) NOT NULL,
	ActorID INT FOREIGN KEY REFERENCES Actor(ActorID) NOT NULL
)

ALTER TABLE ActedIn ADD PRIMARY KEY(MovieID,ActorID)
ALTER TABLE Movie  ADD ImageLink VARCHAR(100)

insert into Movie (MovieName, Duration, Genre, Director, AmountOfMoney, ImageLink) values ('Winter Break', 78.3, 2, 'Kaylyn Lapley', '$95928.35', 'Etiam faucibus cursus urna.');
insert into Movie (MovieName, Duration, Genre, Director, AmountOfMoney, ImageLink) values ('Palais royal !', 64.1, 2, 'Malachi Axelbey', '$87507.36', 'Fusce lacus purus, aliquet at.');
insert into Movie (MovieName, Duration, Genre, Director, AmountOfMoney, ImageLink) values ('Sybil', 79.7, 5, 'Gradeigh Earpe', '$51171.47', 'Aenean lectus.');
insert into Movie (MovieName, Duration, Genre, Director, AmountOfMoney, ImageLink) values ('The Magic Crystal', 80.9, 5, 'Germaine Solomon', '$62573.80', 'Vivamus metus arcu.');
insert into Movie (MovieName, Duration, Genre, Director, AmountOfMoney, ImageLink) values ('The Private Life of Deer', 70.3, 5, 'Rickey Petto', '$26771.23', 'Nullam varius.');

insert into Actor (ActorName, Age, AvgMovieSalary, Nationaly) values ('Merrilee Alphege', 32.05367, 5632.1, 'Portugal');
insert into Actor (ActorName, Age, AvgMovieSalary, Nationaly) values ('Nolie Gemnett', 29.7954, 8014.0, 'China');
insert into Actor (ActorName, Age, AvgMovieSalary, Nationaly) values ('Blaire Cornau', 20.12703, 9490.6, 'Russia');
insert into Actor (ActorName, Age, AvgMovieSalary, Nationaly) values ('Jaynell Boaler', 51.20404, 8250.6, 'Indonesia');
insert into Actor (ActorName, Age, AvgMovieSalary, Nationaly) values ('Siffre Levitt', 27.07642, 7336.4, 'Indonesia');

insert into ActedIn (MovieID, ActorID) values (1, 4);
insert into ActedIn (MovieID, ActorID) values (2, 4);
insert into ActedIn (MovieID, ActorID) values (5, 2);
insert into ActedIn (MovieID, ActorID) values (5, 1);
insert into ActedIn (MovieID, ActorID) values (2, 3);

SELECT * FROM Movie
SELECT * FROM Actor
SELECT * FROM ActedIn


-----------------------------------------------
--Q3--
----1. Write a query to retrieve all the data in the Actor table for actors that are older than 50.
SELECT *
FROM Actor a
WHERE a.Age > 50
----2. Write a query to retrieve all actor names and average salaries from ACTOR and sort the results by average salary
SELECT a.ActorName, a.AvgMovieSalary 
FROM Actor a
ORDER BY (a.AvgMovieSalary) ASC
----3. Using an actor name from your table, write a query to retrieve the names of all the movies that the actor has acted in.
SELECT m.MovieName, a.ActorName
FROM Movie m
INNER JOIN ActedIn ai ON m.MovieID = ai.MovieID
INNER JOIN Actor a ON ai.ActorID = a.ActorID
WHERE a.ActorName = 'Jaynell Boaler'
----4. Write a query to retrieve the names of all the action movies that amount of actor be greater than 3
SELECT m.MovieName, COUNT(a.ActorID) as 'Number of actor'
FROM Movie m
INNER JOIN ActedIn ai ON m.MovieID = ai.MovieID
INNER JOIN Actor a ON ai.ActorID = a.ActorID
GROUP BY m.MovieName
HAVING COUNT(a.ActorID) > 3

