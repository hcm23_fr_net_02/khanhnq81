USE master
go

DROP DATABASE IF EXISTS FresherTrainingManagement
GO

CREATE DATABASE FresherTrainingManagement
GO

USE FresherTrainingManagement
GO

CREATE TABLE Trainee
(
	TraineeID INT PRIMARY KEY IDENTITY (1,1),
	FullName NVARCHAR(50),
	BirthDate DATETIME,
	Gender bit,
	ET_IQ TINYINT CHECK(ET_IQ >= 0 AND ET_IQ <=20),
	ET_Gmath TINYINT CHECK(ET_Gmath >= 0 AND ET_Gmath <=20),
	ET_English TINYINT CHECK(ET_English >= 0 AND ET_English <=50),
	Training_Class NVARCHAR(50),
	Evaluation_Notes NVARCHAR(100)
)

insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Bili Esposi', '6/5/1994', 1, 12, 13, 0, 'KBYS', 'Aliquam erat volutpat.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Raynor Higford', '10/21/1999', 1, 8, 10, 19, 'KBYS', 'Sed vel enim sit amet nunc viverra dapibus.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Ertha Doutch', '8/12/1993', 0, 9, 11, 34, 'KBYS', 'Cras pellentesque volutpat dui.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Odele Fessby', '9/25/1991', 1, 7, 3, 18, 'MUMJ', 'Morbi quis tortor id nulla ultrices aliquet.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Ellwood Dawidsohn', '9/2/1994', 1, 14, 13, 24, 'KNRB', 'Mauris sit amet eros.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Adelind Osgodby', '12/31/1997', 0, 8, 8, 31, 'MUMJ', 'Maecenas tincidunt lacus at velit.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Billie Wanjek', '10/20/1995', 0, 2, 18, 23, 'YSTI', 'Nunc purus.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Evita Deesly', '6/17/2001', 0, 12, 9, 26, 'FAKD', 'Pellentesque viverra pede ac diam.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Shellysheldon Leonardi', '5/23/1996', 0, 15, 16, 21, 'KUKF', 'Integer aliquet.');
insert into Trainee (FullName, BirthDate, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Evaluation_Notes) values ('Theadora Murrow', '3/26/1991', 0, 19, 15, 1, 'VTUK', 'Etiam vel augue.');

SELECT * FROM Trainee

ALTER TABLE Trainee ADD Fsoft_Account VARCHAR(50) 
UPDATE Trainee Set Fsoft_Account = 'N/A' WHERE Fsoft_Account IS NULL
ALTER TABLE Trainee ALTER COLUMN Fsoft_Account VARCHAR (50) NOT NULL
ALTER TABLE Trainee ADD UNIQUE(Fsoft_Account)

CREATE VIEW ET_passed AS
SELECT t.TraineeID, t.FullName, t.BirthDate
FROM Trainee t
WHERE t.ET_IQ >=8 AND t.ET_Gmath >=8 AND (t.ET_IQ + t.ET_Gmath) >=20 AND t.ET_English >= 18

SELECT * FROM ET_passed

SELECT t.TraineeID, t.FullName, MONTH(t.BirthDate) as 'Birth Month'
FROM Trainee t
WHERE t.ET_IQ >=8 AND t.ET_Gmath >=8 AND (t.ET_IQ + t.ET_Gmath) >=20 AND t.ET_English >= 18
GROUP BY t.TraineeID, t.FullName, MONTH(t.BirthDate)
ORDER BY MONTH(t.BirthDate)

SELECT TOP 1 t.TraineeID, t.FullName,  t.BirthDate, DATEDIFF(YEAR, t.BirthDate, GETDATE()) as 'AGE', LEN(t.FullName) as 'Name lenght'
FROM Trainee t
ORDER BY LEN(t.FullName) DESC

