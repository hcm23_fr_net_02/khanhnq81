﻿using StudentManagement.Model;

namespace StudentManagement.Helper
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("=================STUDENT MANAGEMENT=================");
            Console.WriteLine("| 1. Show all student                              |");
            Console.WriteLine("| 2. Find student by id                            |");
            Console.WriteLine("| 3. Add new Student                               |");
            Console.WriteLine("| 4. Update Student by id                          |");
            Console.WriteLine("| 5. Delete Student by id                          |");
            Console.WriteLine("| 6. Exit                                          |");
            Console.WriteLine("====================================================");
            Console.Write("User choice: ");
        }

        public static Student AddStudentMenu()
        {
            Student student = new Student();
            Console.WriteLine("Enter name of student: ");
            student.Name = Console.ReadLine();
            Console.WriteLine("Enter student GPA: ");
            student.GPA = double.Parse(Console.ReadLine());
            return student;
        }
        public static Student UpdateStudentMenu (Student student)
        {
            Console.WriteLine("Enter new student name: ");
            student.Name = Console.ReadLine();
            Console.WriteLine("Enter new GPA: ");
            student.GPA = double.Parse(Console.ReadLine());
            return student;
        }
        public static int GetStudentByIdMenu ()
        {
            Console.WriteLine("Enter student id: ");
            int id = int.Parse(Console.ReadLine());
            return id;
        }
    }
}
