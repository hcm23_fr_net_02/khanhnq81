﻿using Microsoft.EntityFrameworkCore;
using StudentManagement.DataAccess;
using System.Data.Common;

namespace StudentManagement.Helper
{
    public class AccessHelper
    {
        private const string ConnectionString = "Data Source= KHANHS; Database = StudentManagementEFCore-DI; Trusted_Connection = True; TrustServerCertificate = True";
        public static AppDbContext GetContext()
        {
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionBuilder.UseSqlServer(ConnectionString);
            var context = new AppDbContext(optionBuilder.Options);
            return context;
        }
    }
}
