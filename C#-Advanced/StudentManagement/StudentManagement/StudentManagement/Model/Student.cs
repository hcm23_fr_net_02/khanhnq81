﻿namespace StudentManagement.Model
{
    public class Student
    {
        private double gpa;
        public int ID { get; set; }
        public string Name { get; set; }
        public double GPA
        {
            get => gpa;
            set
            {
                if(value < 0 || value > 10)
                {
                    throw new Exception("Invalid GPA !");
                }
                gpa = value;
            }
                    
        }
    }
}
