﻿using StudentManagement.Helper;
using StudentManagement.Service;

var context = AccessHelper.GetContext();
StudentService studentService = new StudentService(context);
int choice = 0;
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                var students = studentService.GetAllStudents();
                foreach (var item in students)
                {
                    Console.WriteLine($"{item.ID}-{item.Name}-{item.GPA}");
                }
                break;
            case 2:
                int findStudenById = Menu.GetStudentByIdMenu();
                var student = studentService.FindStudentById(findStudenById);
                Console.WriteLine($"{student.ID} - {student.Name} - {student.GPA}");
                break;
            case 3:
                var addStudent = Menu.AddStudentMenu();
                studentService.AddNewStudent(addStudent);
                break;
            case 4:
                var findStudentForUpdate = Menu.GetStudentByIdMenu();
                var getStudentByIdForUpdate = studentService.FindStudentById(findStudentForUpdate);
                var updateStudent = Menu.UpdateStudentMenu(getStudentByIdForUpdate);
                studentService.UpdateStudent(updateStudent);
                break;
            case 5:
                var findStudentForDelete = Menu.GetStudentByIdMenu();
                var getStudentByIdForDelete = studentService.FindStudentById(findStudentForDelete);
                studentService.DeleteStudent(getStudentByIdForDelete);
                break;
            case 6:
                Console.WriteLine("Bye bye");
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 6");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);