﻿using StudentManagement.DataAccess.IRepository;
using StudentManagement.Model;

namespace StudentManagement.DataAccess.Repository
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        public StudentRepository(AppDbContext context) : base(context)
        {
        }
    }
}
