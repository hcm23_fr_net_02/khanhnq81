﻿using Microsoft.EntityFrameworkCore;
using StudentManagement.DataAccess.IRepository;

namespace StudentManagement.DataAccess.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly DbSet<T> _dbSet;
        public GenericRepository(AppDbContext context)
        { 
            _dbSet = context.Set<T>();
        }
        public void AddNew(T entity) => _dbSet.Add(entity);

        public void Delete(T entity) => _dbSet.Remove(entity);

        public IEnumerable<T> GetAll() => _dbSet.ToList();

        public T GetById(int id) => _dbSet.Find(id);

        public void Update(T entity) => _dbSet.Update(entity);
    }
}
