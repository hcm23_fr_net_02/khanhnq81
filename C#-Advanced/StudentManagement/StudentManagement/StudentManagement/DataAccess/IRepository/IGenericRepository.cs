﻿namespace StudentManagement.DataAccess.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        T GetById(int id);
        IEnumerable<T> GetAll();
        void AddNew(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
