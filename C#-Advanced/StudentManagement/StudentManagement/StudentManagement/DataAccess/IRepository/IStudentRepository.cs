﻿using StudentManagement.Model;

namespace StudentManagement.DataAccess.IRepository
{
    public interface IStudentRepository : IGenericRepository<Student>
    {
    }
}
