﻿using Microsoft.EntityFrameworkCore;
using StudentManagement.Model;

namespace StudentManagement.DataAccess
{
    public class AppDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options){}

    }
}
