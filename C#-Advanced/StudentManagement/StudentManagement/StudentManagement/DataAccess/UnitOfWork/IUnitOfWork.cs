﻿using StudentManagement.DataAccess.IRepository;

namespace StudentManagement.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        int SaveChange();
        IStudentRepository StudentRepository { get; }
    }
}
