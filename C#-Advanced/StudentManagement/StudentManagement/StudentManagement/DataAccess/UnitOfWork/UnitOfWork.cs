﻿using StudentManagement.DataAccess.IRepository;
using StudentManagement.DataAccess.Repository;

namespace StudentManagement.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IStudentRepository _studentRepository;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _studentRepository = new StudentRepository(context);

        }
        public IStudentRepository StudentRepository { get =>  _studentRepository; }

        public int SaveChange() => _context.SaveChanges();
    }
}
