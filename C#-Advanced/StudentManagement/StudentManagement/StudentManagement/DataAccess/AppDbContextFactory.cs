﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace StudentManagement.DataAccess
{
    //class này chỉ có công dụng giúp ta khởi tạo Migration khi sử dụng phương pháp DI trong AppDbContext
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            string connectionString = "Data Source= KHANHS; Database = StudentManagementEFCore-DI; Trusted_Connection = True; TrustServerCertificate = True";
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionBuilder.UseSqlServer(connectionString);
            return new AppDbContext(optionBuilder.Options);
        }
    }
}
