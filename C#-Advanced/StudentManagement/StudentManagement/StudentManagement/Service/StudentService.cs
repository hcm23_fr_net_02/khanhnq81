﻿using StudentManagement.DataAccess;
using StudentManagement.DataAccess.UnitOfWork;
using StudentManagement.Model;

namespace StudentManagement.Service
{
    public class StudentService : UnitOfWork
    {
        private readonly IUnitOfWork _unitOfWork;
        public StudentService(AppDbContext context) : base(context)
        {
            _unitOfWork = new UnitOfWork(context);
        }

        public void AddNewStudent(Student student)
        {
            _unitOfWork.StudentRepository.AddNew(student);
            var check = _unitOfWork.SaveChange();
            if(check == 0)
            {
                throw new Exception("Add fail");
            }
        }

        public Student FindStudentById(int id)
        {
            var student = _unitOfWork.StudentRepository.GetById(id);
            if(student is null)
            {
                throw new Exception($"Can not find student by id: {id}");
            }
            return student;
        }

        public void UpdateStudent(Student student)
        {
            _unitOfWork.StudentRepository.Update(student);
            var check = _unitOfWork.SaveChange();
            if(check == 0)
            {
                throw new Exception("Update fail");
            }
        }
        public void DeleteStudent(Student student) 
        {
            _unitOfWork.StudentRepository.Delete(student);
            var check = _unitOfWork.SaveChange();
            if(check == 0)
            {
                throw new Exception("Delete fail");
            }
        }
        public List<Student> GetAllStudents()
        {
            var students = _unitOfWork.StudentRepository.GetAll();
            if(students.Count() == 0)
            {
                throw new Exception("Student list is empty");
            }
            return students.ToList();
        }
    }
}
