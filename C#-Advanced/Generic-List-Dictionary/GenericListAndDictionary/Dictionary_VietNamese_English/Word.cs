﻿namespace Dictionary_English_Vietnamese
{
    public class Word
    {
        private string english;
        private string vietnamese;

        public string English
        {
            get => english;
            set => english = value;
        }
        public string Vietnamese
        {
            get => vietnamese;
            set => vietnamese = value;
        }
    }
}
