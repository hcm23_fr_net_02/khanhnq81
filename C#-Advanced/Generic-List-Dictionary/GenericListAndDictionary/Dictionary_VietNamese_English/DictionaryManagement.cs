﻿using System.Text;
namespace Dictionary_English_Vietnamese
{
    public class DictionaryManagement
    {
        Dictionary<string, string> dictionary;
        Predicate<string> predicate;

        public DictionaryManagement()
        {
            dictionary = new Dictionary<string, string>();
            predicate = FindWord;
        }
        private bool FindWord(string word)
        {
            foreach (var value in dictionary)
            {
                if (value.Key.Contains(word) || value.Value.Contains(word))
                {
                    return true;
                }
            }
            return false;
        }
        public string GetWord(string word)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in dictionary)
            {
                if (!predicate(word))
                {
                    throw new Exception("Can not found");
                }
                sb.Append($"{item.Key} | {item.Value}");
            }
            return sb.ToString();
        }
        public void AddNewWord(Word word)
        {
            dictionary.Add(word.English, word.Vietnamese);
        }
        public void RemoveWord(string remove)
        {
            if (!predicate(remove))
            {
                throw new Exception("Can not found");

            }
            dictionary.Remove(remove);
        }
        public void EditKey(string word)
        {
            KeyValuePair<string, string>? keyValuePair = null;
            foreach (var item in dictionary)
            {
                if (predicate(word))
                {
                    keyValuePair = item;
                    break;
                }
                else
                {
                    throw new Exception("Can not found");
                }
            }
            if (keyValuePair.HasValue)
            {
                dictionary.Remove(keyValuePair.Value.Key);
                Word editKey = new Word();
                editKey.Vietnamese = keyValuePair.Value.Value;
                Console.WriteLine("Enter new key: ");
                editKey.English = Console.ReadLine()!;
                AddNewWord(editKey);
            }
        }

        public void EditValue(string word)
        {
            KeyValuePair<string, string>? keyValuePair = null;
            foreach (var item in dictionary)
            {
                if (predicate(word))
                {
                    keyValuePair = item;
                    break;
                }
                else
                {
                    throw new Exception("Can not found");
                }
            }
            if (keyValuePair.HasValue)
            {
                dictionary.Remove(keyValuePair.Value.Key);
                Word editValue = new Word();
                Console.WriteLine("Enter new value: ");
                editValue.Vietnamese = Console.ReadLine()!;
                editValue.English = keyValuePair.Value.Key;
                AddNewWord(editValue);
            }
        }
        public void EditKeyAndValue(string key)
        {
            KeyValuePair<string, string>? keyValuePair = null;
            foreach (var item in dictionary)
            {
                if (!predicate(key))
                {
                    throw new Exception("Can not found");
                }
                keyValuePair = item;
                break;
            }

            if (keyValuePair.HasValue)
            {
                dictionary.Remove(keyValuePair.Value.Key);
                Word editKeyAndValue = new Word();
                Console.WriteLine("Enter new key: ");
                editKeyAndValue.English = Console.ReadLine()!;
                Console.WriteLine("Enter new value: ");
                editKeyAndValue.Vietnamese = Console.ReadLine()!;
                AddNewWord(editKeyAndValue);
            }
        }
        public string DisplayAllKeyAndValue()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var word in dictionary)
            {
                sb.Append($"\n{word.Key} - {word.Value}");
            }
            return sb.ToString();
        }
        public string DisplayAllKey()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var word in dictionary.Keys)
            {
                sb.Append($"\n{word}");
            }
            return sb.ToString();
        }
        public string DisplayAllValue()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var word in dictionary.Values)
            {
                sb.Append($"\n{word}");
            }
            return sb.ToString();
        }
    }
}
