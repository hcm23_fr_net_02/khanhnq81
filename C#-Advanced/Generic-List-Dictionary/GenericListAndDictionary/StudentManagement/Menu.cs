﻿namespace StudentManagement
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("====================MENU====================");
            Console.WriteLine("| 1. Add new student                       |");
            Console.WriteLine("| 2. Find student by id                    |");
            Console.WriteLine("| 3. Find student by name                  |");
            Console.WriteLine("| 4. Show all student                      |");
            Console.WriteLine("| 5. Remove student by id                  |");
            Console.WriteLine("| 6. Exit                                  |");
            Console.WriteLine("=============================================");
            Console.Write("Choice: ");
        }
    }
}
