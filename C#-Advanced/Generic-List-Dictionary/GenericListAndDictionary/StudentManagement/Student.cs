﻿namespace StudentManagement
{
    public class Student
    {
        private int age;
        private double gpa;
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age
        {
            get => age;
            set
            {
                age = value;
                if (value == 0 || value < 0)
                {
                    throw new Exception("Age must bigger than 0");
                }
            }
        }
        public double GPA
        {
            get => gpa;
            set
            {
                gpa = value;
                if (value < 0)
                {
                    throw new Exception("GPA must bigger than 0");
                }
            }
        }
    }
}
