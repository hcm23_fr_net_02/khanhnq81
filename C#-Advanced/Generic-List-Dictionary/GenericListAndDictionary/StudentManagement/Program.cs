﻿using StudentManagement;

int choice = 0;
StudentManagements managements = new StudentManagements();
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Student student = new Student();
                Console.WriteLine("Enter student id: ");
                student.ID = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter student name: ");
                student.Name = Console.ReadLine();
                Console.WriteLine("Enter student age: ");
                student.Age = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter student GPA: ");
                student.GPA = double.Parse(Console.ReadLine());
                managements.AddStudent(student);
                break;
            case 2:
                Console.WriteLine("Enter student id: ");
                int id = int.Parse(Console.ReadLine());
                var studentById = managements.GetStudentById(id);
                Console.WriteLine($"{studentById.ID} - {studentById.Name} - {studentById.Age} - {studentById.GPA}");
                break;
            case 3:
                Console.WriteLine("Enter student name: ");
                string name = Console.ReadLine();
                var studentByName = managements.GetStudentByName(name);
                Console.WriteLine(studentByName);
                break;
            case 4:
                var allStudent = managements.ShowAllStudent();
                Console.WriteLine(allStudent);
                break;
            case 5:
                Console.WriteLine("Enter student id: ");
                int removeId = int.Parse(Console.ReadLine());
                var removed = managements.RemoveStudent(removeId);
                Console.WriteLine(removed);
                break;
            case 6:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 6");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 6);