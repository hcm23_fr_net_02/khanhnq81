﻿using System.Text;

namespace StudentManagement
{
    public class StudentManagements
    {
        List<Student> students;
        public StudentManagements()
        {
            students = new List<Student>();
            students.Add(new Student { ID = 1, Age = 18, Name = "Khanh", GPA = 10});
            students.Add(new Student { ID = 2, Age = 15, Name = "A", GPA = 8});
            students.Add(new Student { ID = 3, Age = 13, Name = "K", GPA = 7});
        }

        public void AddStudent(Student student)
        {
            students.Add(student);
        }

        public string ShowAllStudent()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Student student in students)
            {
                sb.Append($"{student.ID} - {student.Name} - {student.Age} - {student.GPA}\n");
            }
            return sb.ToString();
        }

        public Student GetStudentById(int id)
        {
            var student = students.SingleOrDefault(student => student.ID == id);
            if(student is null)
            {
                throw new Exception($"Student id: {id} does not exist");
            }
            return student;
        }

        public string GetStudentByName(string name) 
        {
            StringBuilder sb = new StringBuilder();
            var studentByName = students.Where(student => student.Name.ToUpper().Contains(name.ToUpper())).ToList();
            foreach (var student in studentByName)
            {
                sb.Append($"{student.ID} - {student.Name} - {student.Age} - {student.GPA}\n");
            }
            return sb.ToString();
        }

        public string RemoveStudent(int id)
        {
            var student = GetStudentById(id);
            var remove = students.Remove(student);
            if (remove is false)
            {
                return "Remove fail";
            }
            return "Remove successful";
        }
    }
}
