﻿namespace GetAverage
{
    public static class MyExtensions
    {
        public static double GetAverageForListInt(this List<int> ints)
        {
            double average = 0;
            for (int i = 0; i < ints.Count; i++)
            {
                average += ints[i];
            }
            return average / ints.Count;
        }
    }
}
