﻿using GetAverage;

List<int> ints = new List<int> { 1, 2, 3 };
double average = ints.GetAverageForListInt();
Console.WriteLine($"Average: {average}");