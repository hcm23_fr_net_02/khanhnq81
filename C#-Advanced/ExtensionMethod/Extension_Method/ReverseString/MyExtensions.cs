﻿using System.Text;

namespace ReverseString
{
    public static class MyExtensions
    {
        public static string ReverseString(this string s)
        {
            StringBuilder sb = new StringBuilder();
            var reverseStrings = s.ToCharArray();
            for (int i = reverseStrings.Length; i > 0; i--)
            {
                sb.Append(reverseStrings[i - 1]);
            }
            return sb.ToString();
        }
    }
}
