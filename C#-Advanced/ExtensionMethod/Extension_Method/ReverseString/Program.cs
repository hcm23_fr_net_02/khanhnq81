﻿using ReverseString;

try
{
    Console.WriteLine("Enter a string to reverse: ");
    string text = Console.ReadLine();
    string reverse = text.ReverseString();
    Console.WriteLine(reverse);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}