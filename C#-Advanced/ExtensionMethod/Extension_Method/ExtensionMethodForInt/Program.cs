﻿using ExtensionMethodForInt;

try
{
    Console.WriteLine("Enter a number to double it: ");
    int number = int.Parse(Console.ReadLine());
    int doubleNumber = number.Square();
    Console.WriteLine($"{number} => {doubleNumber}");
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}