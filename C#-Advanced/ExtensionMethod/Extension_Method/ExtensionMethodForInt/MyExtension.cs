﻿namespace ExtensionMethodForInt
{
    public static class MyExtension
    {
        public static int Square(this int x) => x * x;
    }
}
