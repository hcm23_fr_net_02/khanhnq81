﻿using System.Text;

namespace RemoveDuplicates
{
    public static class MyExtensions
    {
        public static string RemoveDuplicate(this string s)
        {
            StringBuilder sb = new StringBuilder();
            HashSet<char> seenChars = new HashSet<char>();
            foreach (char c in s)
            {
                if (!seenChars.Contains(c))
                {
                    seenChars.Add(c);
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}
 