﻿namespace Todo_MockApi
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("============MENU===============");
            Console.WriteLine("| 1. Show task by date        |");
            Console.WriteLine("| 2. Show task by priority    |");
            Console.WriteLine("| 3. Create task              |");
            Console.WriteLine("| 4. Complete task            |");
            Console.WriteLine("| 5. Update priority          |");
            Console.WriteLine("| 6. Delete task              |");
            Console.WriteLine("| 7. Print complete task      |");
            Console.WriteLine("| 8. Print uncomplete task    |");
            Console.WriteLine("| 9. Exit                     |");
            Console.WriteLine("===============================");
            Console.Write("Choice ");
        }
    }
}
