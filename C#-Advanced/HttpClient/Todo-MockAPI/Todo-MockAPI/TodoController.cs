﻿using Newtonsoft.Json;
using System.Net.Http.Json;
using System.Text;
using Todo_MockApi;

namespace Todo_MockAPI
{
    public class TodoController : ITodo
    {
        HttpClient client;
        const string urlPath = "https://64c0b9550d8e251fd11271fe.mockapi.io/api/todoApp/";
        const string endPoint = "Todo";
        List<Todo> tasks;
        public TodoController()
        {
            client = new HttpClient();
            tasks = new List<Todo>();
        }

        public async Task GetData()
        {
            string url = string.Concat(urlPath, endPoint);
            try
            {
                string data = await client.GetStringAsync(url);
                tasks = JsonConvert.DeserializeObject<List<Todo>>(data);
            }
            catch (Exception ex)
            {
                await Console.Out.WriteLineAsync(ex.Message);
            }

        }

        public async Task<string> ShowAllTaskOrderByDateDesc()
        {
            StringBuilder sb = new StringBuilder();
            var orderByDate = tasks.OrderByDescending(task => task.CreateAt).ToList();
            foreach (var todo in orderByDate)
            {
                sb.Append($"{todo.Id} - {todo.Title} - {todo.CreateAt.ToString("dd/MM/yyyy HH:mm")} - {todo.FinalAt?.ToString("dd/MM/yyyy HH:mm")} - {todo.Priority} -{todo.IsComplete}\n");
            }
            return sb.ToString();
        }

        public async Task CreateTodo(Todo todo)
        {
            string url = string.Concat(urlPath, endPoint);
            await client.PostAsJsonAsync(url, todo);
        }

        public async Task<Todo> GetTaskById(int id)
        {
            var task = tasks.SingleOrDefault(task => task.Id == id);
            if (task is null)
            {
                throw new Exception($"Task can not found by id : {id}");
            }
            return task;
        }

        public async Task CompleteTask(int id)
        {
            string url = string.Concat(urlPath, endPoint, $"/{id}");
            var task = await GetTaskById(id);
            task.IsComplete = true;
            task.FinalAt = DateTime.Now;
            await client.PutAsJsonAsync(url, task);
        }

        public async Task DeleteTask(int id)
        {
            var task = await GetTaskById(id);
            string url = string.Concat(urlPath, endPoint, $"/{id}");
            await client.DeleteAsync(url);
        }

        public async Task<string> ShowCompletedTask()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var todo in tasks)
            {
                if(todo.IsComplete is true)
                {
                    sb.Append($"{todo.Id} - {todo.Title} - {todo.CreateAt.ToString("dd/MM/yyyy HH:mm")} - {todo.FinalAt?.ToString("dd/MM/yyyy HH:mm")} - {todo.Priority} - {todo.IsComplete}\n");
                }
            }
            return sb.ToString();
        }

        public async Task<string> ShowUnCompletedTask()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var todo in tasks)
            {
                if (todo.IsComplete is false)
                {
                    sb.Append($"{todo.Id} - {todo.Title} - {todo.CreateAt.ToString("dd/MM/yyyy HH:mm")} - {todo.FinalAt?.ToString("dd/MM/yyyy HH:mm")} - {todo.Priority} - {todo.IsComplete}\n");
                }
            }
            return sb.ToString();
        }

        public async Task UpdatePriority(int id,Todo task)
        {
            string url = string.Concat(urlPath, endPoint, $"/{id}");
            await client.PutAsJsonAsync(url, task);
        }

        public async Task<string> ShowAllTaskOrderByPriorityDesc()
        {
            StringBuilder sb = new StringBuilder();
            var orderByPriority = tasks.OrderByDescending(task => task.Priority).ToList();
            foreach (var todo in orderByPriority)
            {
                sb.Append($"{todo.Id} - {todo.Title} - {todo.CreateAt.ToString("dd/MM/yyyy HH:mm")} - {todo.FinalAt?.ToString("dd/MM/yyyy HH:mm")} - {todo.Priority} - {todo.IsComplete}\n");
            }
            return sb.ToString();
        }
    }
}
