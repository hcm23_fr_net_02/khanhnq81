﻿using Todo_MockApi;

namespace Todo_MockAPI
{
    public interface ITodo
    {
        Task GetData();
        Task CreateTodo(Todo todo);
        Task<Todo> GetTaskById(int id);
        Task CompleteTask(int id);
        Task DeleteTask(int id);
        Task<string> ShowAllTaskOrderByDateDesc();
        Task<string> ShowAllTaskOrderByPriorityDesc();
        Task<string> ShowCompletedTask();
        Task<string> ShowUnCompletedTask();
        Task UpdatePriority(int id, Todo task);

    }
}
