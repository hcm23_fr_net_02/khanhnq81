﻿using Todo_MockApi;
using Todo_MockAPI;

TodoController controller = new TodoController();
int choice = 0;
do
{
    try
    {

        Menu.MainMenu();
        await controller.GetData();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                var tasks = await controller.ShowAllTaskOrderByDateDesc();
                Console.WriteLine(tasks);
                break;
            case 2:
                var tasksByPriority = await controller.ShowAllTaskOrderByPriorityDesc();
                Console.WriteLine(tasksByPriority);
                break;
            case 3:
                Todo todo = new Todo();
                Console.WriteLine("Enter task: ");
                todo.Title = Console.ReadLine();
                Console.WriteLine("Enter priority: ");
                todo.Priority = int.Parse(Console.ReadLine());
                await controller.CreateTodo(todo);
                break;
            case 4:
                Console.WriteLine("Enter task id to complete: ");
                int id = int.Parse(Console.ReadLine());
                await controller.CompleteTask(id);
                break;
            case 5:
                Console.WriteLine("Enter task id to update priority: ");
                int idUpdatePriority = int.Parse(Console.ReadLine());
                var updatePriority = await controller.GetTaskById(idUpdatePriority);
                Console.WriteLine("Enter priority");
                updatePriority.Priority = int.Parse(Console.ReadLine());
                await controller.UpdatePriority(idUpdatePriority, updatePriority);
                break;
            case 6:
                Console.WriteLine("Enter task id to delete: ");
                int deleteTask = int.Parse(Console.ReadLine());
                await controller.DeleteTask(deleteTask);
                break;
            case 7:
                var completedTask = await controller.ShowCompletedTask();
                Console.WriteLine(completedTask);
                break;
            case 8:
                var unCompletedTask = await controller.ShowUnCompletedTask();
                Console.WriteLine(unCompletedTask);
                break;
            case 9:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 7");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.ToString());
    }
} while (choice != 9);