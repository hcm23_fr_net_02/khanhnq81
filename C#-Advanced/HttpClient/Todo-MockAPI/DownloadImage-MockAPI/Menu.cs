﻿namespace DownloadImage_MockAPI
{
    public class Menu
    {
        public void MainMenu()
        {
            Console.WriteLine("==========MENU===========");
            Console.WriteLine("| 1. Add link image     |");
            Console.WriteLine("| 2. Download image     |");
            Console.WriteLine("| 3. Show list          |");
            Console.WriteLine("| 4. Exit               |");
            Console.WriteLine("=========================");
            Console.Write("Choose: ");
        }
    }
}
