﻿namespace DownloadImage_MockAPI
{
    public interface IRepository
    {
        Task Add (Image image);
        Task GetData();
        Task DownLoad(int id);
        string ShowListImage();
    }
}
