﻿using DownloadImage_MockAPI;

Menu menu = new Menu();
HttpClient client = new HttpClient();
IRepository controller = new DownloadImage(client);
int choice = 0;
do
{
    try
    {
        await controller.GetData();
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Image image = new Image();
                Console.WriteLine("Enter url: ");
                image.Url = Console.ReadLine();
                Console.WriteLine("Enter Description: ");
                image.Description = Console.ReadLine();
                await controller.Add(image);
                break;
            case 2:
                Console.WriteLine(controller.ShowListImage());
                Console.WriteLine("Enter id to download");
                int id = int.Parse(Console.ReadLine());
                await controller.DownLoad(id);
                Console.WriteLine("Download successful");
                break;
            case 3:
                var list = controller.ShowListImage();
                Console.WriteLine(list);
                break;
            case 4:
                client.Dispose();
                Environment.Exit(0);
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);