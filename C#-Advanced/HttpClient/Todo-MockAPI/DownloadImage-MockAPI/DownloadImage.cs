﻿using System.Net.Http.Json;
using Newtonsoft.Json;
using System.Text;

namespace DownloadImage_MockAPI
{
    public class DownloadImage : IRepository
    {
        const string url = "https://64c0b9550d8e251fd11271fe.mockapi.io/api/todoApp/";
        const string directoryPath = @"Directory";
        const string endPoint = "Image";
        List<Image> images;
        private readonly HttpClient _client;
        public DownloadImage(HttpClient client)
        {
            images = new List<Image>();
            _client = client;
        }
        public async Task Add(Image image)
        {
            string urlPath = string.Concat(url, endPoint);
            await _client.PostAsJsonAsync(urlPath, image);
        }
        public async Task GetData()
        {
            string urlPath = string.Concat(url, endPoint);
            var access = await _client.GetAsync(urlPath);
            var data = await access.Content.ReadAsStringAsync();
            images = JsonConvert.DeserializeObject<List<Image>>(data);
        }
        public async Task DownLoad(int id)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            var image = images.SingleOrDefault(image => image.Id == id);
            if (image is null)
            {
                throw new Exception("Image does not exist in collection");
            }
            string fileName = Path.GetFileName(image.Url);
            var data = await _client.GetByteArrayAsync(image.Url);
            string location = string.Concat(directoryPath, "/" + fileName);
            await File.WriteAllBytesAsync(location, data);
            await UpdateIsDownload(image, image.Id);
        }
        public string ShowListImage()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in images!)
            {
                sb.AppendLine($"{item.Id} | {item.Description}");
            }
            return sb.ToString();
        }

        private async Task UpdateIsDownload(Image image, int id)
        {
            string urlUpdate = string.Concat(url, endPoint, $"{id}");
            await _client.PutAsJsonAsync(urlUpdate, image);
        }
    }
}
