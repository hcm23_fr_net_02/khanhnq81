﻿
// Yêu cầu 1: Lấy ra "tên" các sản phẩm có giá trị lớn hơn 100, sắp xếp giảm dần theo giá.
// Yêu cầu 2: Lấy ra thông tin các sản phẩm có tên chứa từ "o", sắp xếp tăng dần theo tên.
class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public decimal Price { get; set; }
}