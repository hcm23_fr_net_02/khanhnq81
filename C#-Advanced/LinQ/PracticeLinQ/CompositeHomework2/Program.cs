﻿List<Product> products = new List<Product>
{
new Product { Id = 1, Name = "Laptop", Price = 1000 },
new Product { Id = 2, Name = "Phone", Price = 500 },
new Product { Id = 3, Name = "Headphones", Price = 50 },
new Product { Id = 4, Name = "Mouse", Price = 20 },
new Product { Id = 5, Name = "Keyboard", Price = 30 }
};

// Yêu cầu 1: Lấy ra "tên" các sản phẩm có giá trị lớn hơn 100, sắp xếp giảm dần theo giá.
var getName = products.Where(product => product.Price > 100).OrderByDescending(product => product.Price).Select(product => product.Name).ToList();
foreach (var item in getName)
{
    Console.WriteLine(item);
}
Console.WriteLine("-----------------------------------------------------------");
// Yêu cầu 2: Lấy ra thông tin các sản phẩm có tên chứa từ "o", sắp xếp tăng dần theo tên.
var getAndOrderByName = products.Where(product => product.Name.Contains('o')).OrderBy(product => product.Name).ToList();
foreach (var item in getAndOrderByName)
{
    Console.WriteLine($"{item.Id} - {item.Name} - {item.Price}");
}