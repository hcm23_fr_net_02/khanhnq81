﻿List<int> numbers = new List<int> { 3, 8, 1, 5, 2, 10, 7 };
// a) Lấy ra các số chẵn và sắp xếp tăng dần kq: 2 8 10
var getEvenAndOrder = numbers.Where(number => number % 2 == 0).OrderBy(number => number).ToList();   
foreach (var number in getEvenAndOrder)
{
    Console.WriteLine(number);
}
Console.WriteLine("-----------------------------------------------------------");
// b) Lấy ra các số lẻ và sắp xếp giảm dần kq: 7 5 3 1
var getOddAndOrderDesc = numbers.Where(number => number % 2 !=0).OrderByDescending(number => number).ToList();
foreach (var number in getOddAndOrderDesc)
{
    Console.WriteLine(number);
}
Console.WriteLine("-----------------------------------------------------------");
// c) Lấy ra các số lớn hơn 5 và nhỏ hơn 10 và sắp xếp tăng dần kq: 7 8
var bigerThanFiveAndSmallerThanTen  = numbers.Where(number => number > 5 && number < 10).OrderBy(number =>number).ToList();
foreach (var item in bigerThanFiveAndSmallerThanTen)
{
    Console.WriteLine(item);
}