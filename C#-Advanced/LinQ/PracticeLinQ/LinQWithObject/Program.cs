﻿var employees = new List<Employee>
{
new Employee { Name = "Alice", Department = "Sales", Salary = 50000 },
new Employee { Name = "Bob", Department = "Marketing", Salary = 60000 },
new Employee { Name = "Charlie", Department = "Sales", Salary = 55000 },
new Employee { Name = "David", Department = "IT", Salary = 70000 }
};
// Hãy sử dụng phương thức Where của LINQ
// Thực hiện các yêu cầu sau và in kết quả ra màn hình:
// 1. Lọc và in ra tên của nhân viên trong phòng IT.
var getByNameAndDepartment = employees.Where(department => department.Department == "IT").Select(employee => employee.Name).ToList();
foreach (var employee in getByNameAndDepartment)
{
    Console.WriteLine($"Deparment IT has: {employee}");
}
Console.WriteLine("--------------------------------------------------------------------------");
// 2. Lọc và in ra tên của nhân viên có mức lương lớn hơn 60000$.
var getByHighestSalary = employees.Where(salary => salary.Salary > 60000).Select(employee => employee.Name).ToList();
foreach (var emp in getByHighestSalary)
{
    Console.WriteLine($"Highest salary: {emp}");
}
Console.WriteLine("--------------------------------------------------------------------------");

// 3. Lọc và in ra tên của nhân viên chứa chữ "o" hoặc kết thúc bằng "e".
var filterName = employees.Where(employee => employee.Name.EndsWith('e') || employee.Name.Contains('o')).ToList();
foreach (var item in filterName)
{
    Console.WriteLine($"Filter: {item.Name}");
}
class Employee
{
    public string Name { get; set; }
    public string Department { get; set; }
    public decimal Salary { get; set; }
}