﻿List<DateTime> dates = new List<DateTime>()
{
new DateTime(2021, 1, 1),
new DateTime(2022, 2, 1),
new DateTime(2023, 1, 7),
new DateTime(2024, 1, 5),
new DateTime(2025, 5, 10),
};
// Hãy sử dụng phương thức Where của LINQ
// Thực hiện các yêu cầu sau và in kết quả ra màn hình:
// 1. Chọn và in ra các ngày có năm là 2023.
var getByYear = dates.Where(date => date.Year == 2023).ToList();
foreach (var date in getByYear)
{
    Console.WriteLine(date);
}
// 2. Chọn và in ra các ngày có tháng là 1 và năm là 2024.
var getByMonthAndYear = dates.Where(date => date.Month == 1 && date.Year == 2024).ToList();
foreach (var date in getByMonthAndYear)
{
    Console.WriteLine(date);
}
// 3. Chọn và in ra các ngày có tháng là 5 và ngày là 10.
var getByDayAndMonth = dates.Where(date => date.Month == 5 && date.Day == 10).ToList();
foreach (var date in getByDayAndMonth)
{
    Console.WriteLine(date);
}