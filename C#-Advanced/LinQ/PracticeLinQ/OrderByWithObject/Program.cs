﻿using OrderByWithObject;
List<Person> people = new List<Person>
{
    new Person { Name = "Alice", Age = 25 },
    new Person { Name = "Bob", Age = 30 },
    new Person { Name = "Charlie", Age = 22 },
    new Person { Name = "David", Age = 28 },
    new Person { Name = "Emily", Age = 27 }
};
// Sắp xếp danh sách theo thứ tự tăng dần của tuổi (Age)
var orderByAge = people.OrderBy(x => x.Age).ToList();
foreach (var person in people)
{
    Console.WriteLine($"Name: {person.Name} - Age: {person.Age}");
}