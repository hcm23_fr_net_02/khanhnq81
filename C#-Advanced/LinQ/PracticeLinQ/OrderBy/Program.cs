﻿var names = new List<string> {
"John",
"Alice",
"Bob",
"David",
"Emily"
};
// Sắp xếp danh sách theo thứ tự tăng dần của tên và in ra màn hình

var orderAsc = names.OrderBy(name => name).ToList();
foreach (var ordered in orderAsc)
{
    Console.WriteLine(ordered);
}
Console.WriteLine("---------------------------------------------");
List<DateTime> dates = new List<DateTime>
{
new DateTime(2021, 1, 15),
new DateTime(2022, 5, 10),
new DateTime(2020, 12, 1),
new DateTime(2023, 3, 25),
new DateTime(2024, 8, 5)
};
// Sắp xếp danh sách theo thứ tự tăng dần của các ngày
var orderedDate = dates.OrderBy(date => date).ToList();
foreach (var date in orderedDate)
{
    Console.WriteLine(date);
}