﻿List<string> fruits =  new List<string> {"apple", "banana", "orange", "pineapple", "mango" };

var getByLength = fruits.Where(fruit => fruit.Length == 6).ToList();

foreach (var fruit in getByLength)
{
    Console.WriteLine(fruit);
}
Console.WriteLine("-------------------------------------------------------");
var getByFirstChar = fruits.Where(fruit => fruit[0] == 'a').ToList();
foreach (var fruit in getByFirstChar)
{
    Console.WriteLine(fruit);
}
Console.WriteLine("-------------------------------------------------------");
var getByContainChar = fruits.Where(fruit => fruit.Contains("a")).ToList();
foreach (var fruit in getByContainChar)
{
    Console.WriteLine(fruit);
}