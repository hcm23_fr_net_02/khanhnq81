﻿var numbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

// đếm số chẵn trong danh sách
var count = numbers.Where(number => number % 2 == 0).Count();
Console.WriteLine($"Number of even: {count}");
//Tính tổng với Sum
var sum = numbers.Sum();
Console.WriteLine($"Total: {sum}");
// tổng số chẵn trong danh sách
var evenSum = numbers.Where(number => number % 2 == 0).Sum();
Console.WriteLine($"Total even: {evenSum}");
//Tính trung bình với Average
var avg = numbers.Average();
Console.WriteLine($"Averge: {avg}");
//Min
var min = numbers.Min();
Console.WriteLine($"Minimum number: {min}");
//Max
var max = numbers.Max();
Console.WriteLine($"Maximum number: {max}");