﻿namespace Homework_OrderList
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("=====================================MENU======================================");
            Console.WriteLine("| 1. Tính tổng số tiền của tất cả các đơn đặt hàng                            |");
            Console.WriteLine("| 2. Lấy ra đơn hàng có giá trị (TotalAmount) cao nhất                        |");
            Console.WriteLine("| 3. Lấy ra đơn hàng có giá trị thấp nhất                                     |");
            Console.WriteLine("| 4. Tính giá trị trung bình của các đơn hàng                                 |");
            Console.WriteLine("| 5. Tính tổng số đơn đặt hàng của một khách hàng cụ thể                      |");
            Console.WriteLine("| 6. Tính tổng số tiền của tất cả các đơn đặt hàng của một khách hàng cụ thể. |");
            Console.WriteLine("| 7. Lấy ra đơn hàng có giá trị cao nhất của một khách hàng cụ thể.           |");
            Console.WriteLine("| 8. Lấy ra đơn hàng có giá trị thấp nhất cho một khách hàng cụ thể.          |");
            Console.WriteLine("| 9. Tính giá trị trung bình các đơn đặt hàng của một khách hàng cụ thể.      |");
            Console.WriteLine("| 10. EXIT                                                                    |");
            Console.WriteLine("===============================================================================");
            Console.Write("Choice: ");

        }
    }
}
