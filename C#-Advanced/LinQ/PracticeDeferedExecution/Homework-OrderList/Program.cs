﻿using Homework_OrderList;
Console.OutputEncoding = System.Text.Encoding.UTF8;
OrderController controller = new OrderController();
controller.GetData();
int choice = 0;
string name;
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine(controller.TotalPrice());
                break;
            case 2:
                var highestPrice = controller.HighestPrice();
                Console.WriteLine($"{highestPrice.ID} - {highestPrice.CustomerName} - {highestPrice.TotalAmount}");
                break;
            case 3:
                var lowestPrice = controller.LowestPrice();
                Console.WriteLine($"{lowestPrice.ID} - {lowestPrice.CustomerName} - {lowestPrice.TotalAmount}");
                break;
            case 4:
                Console.WriteLine(controller.AverageTotalPrice());
                break;
            case 5:
                Console.WriteLine("Enter customer name: ");
                name = Console.ReadLine();
                Console.WriteLine(controller.NumberOfOrderOfCustomer(name));
                break;
            case 6:
                Console.WriteLine("Enter customer name: ");
                name = Console.ReadLine();
                Console.WriteLine(controller.AveragePriceOfCustomer(name));
                break;
            case 7:
                Console.WriteLine("Enter customer name: ");
                name = Console.ReadLine();
                var highestOrderOfCustomer = controller.HighestOrderOfCustomer(name);
                Console.WriteLine($"{highestOrderOfCustomer.ID} - {highestOrderOfCustomer.CustomerName} - {highestOrderOfCustomer.TotalAmount}");
                break;
            case 8:
                Console.WriteLine("Enter customer name: ");
                name = Console.ReadLine();
                var lowestOrderOfCustomer = controller.LowestOrderOfCustomer(name);
                Console.WriteLine($"{lowestOrderOfCustomer.ID} - {lowestOrderOfCustomer.CustomerName} - {lowestOrderOfCustomer.TotalAmount}");
                break;
            case 9:
                Console.WriteLine("Enter customer name: ");
                name = Console.ReadLine();
                Console.WriteLine(controller.AveragePriceOfCustomer(name));
                break;
            case 10:
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 10);
