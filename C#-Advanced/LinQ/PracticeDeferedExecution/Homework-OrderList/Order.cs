﻿namespace Homework_OrderList
{
    public class Order
    {
        public int ID { get; set; }
        public string CustomerName { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
