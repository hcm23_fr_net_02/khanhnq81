﻿using Newtonsoft.Json;
namespace Homework_OrderList
{
    public class OrderController
    {
        List<Order> orders;
        public OrderController()
        {
            orders = new List<Order>();
        }

        public void GetData()
        {
            string data = File.ReadAllText(@"data.json");
            orders = JsonConvert.DeserializeObject<List<Order>>(data);
        }

        public decimal TotalPrice() => orders.Sum(order => order.TotalAmount);
        public Order HighestPrice() => orders.OrderByDescending(order => order.TotalAmount).First();

        public Order LowestPrice() => orders.OrderBy(order => order.TotalAmount).First();

        public decimal AverageTotalPrice() => orders.Average(order => order.TotalAmount);

        public int NumberOfOrderOfCustomer(string name) => orders.Where(order => order.CustomerName.Equals(name)).Count();

        public decimal TotalPriceCustomerPay(string name) => orders.Where(order => order.CustomerName.Equals(name)).Sum(order => order.TotalAmount);
        public Order HighestOrderOfCustomer(string name) => orders.Where(order => order.CustomerName.Equals(name)).OrderByDescending(order => order.TotalAmount).First();
        public Order LowestOrderOfCustomer(string name) => orders.Where(order => order.CustomerName.Equals(name)).OrderBy(order => order.TotalAmount).First();
        public decimal AveragePriceOfCustomer(string name) => orders.Where(order => order.CustomerName.Equals(name)).Average(order => order.TotalAmount);

    }
}
