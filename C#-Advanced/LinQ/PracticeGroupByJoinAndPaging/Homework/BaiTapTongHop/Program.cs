﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
    // Thêm các sản phẩm khác tại đây...
};
// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
var getByPrice = products.Where(p => p.Price > 500000);
foreach (var item in getByPrice)
{
    Console.WriteLine($"{item.Name} - {item.Price} - {item.Category}");
}
Console.WriteLine("--------------------------------------------------------------------");
// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
var getByPriceAndOrder = products.Where(p => p.Price < 300000).OrderByDescending(p => p.Price).ToList();
foreach (var item in getByPriceAndOrder)
{
    Console.WriteLine($"{item.Name} - {item.Price} - {item.Category}");
}
Console.WriteLine("--------------------------------------------------------------------");
// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
var group = products.GroupBy(p => p.Category);
foreach (var item in group)
{
    Console.WriteLine(item.Key);
    foreach (var value in item)
    {
        Console.WriteLine($"{value.Name} - {value.Price} - {value.Category}");
    }
}
Console.WriteLine("--------------------------------------------------------------------");
// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
var totalInCategory = products.Where(p => p.Category.Equals("Áo")).Sum(p => p.Price);
Console.WriteLine(totalInCategory);
Console.WriteLine("--------------------------------------------------------------------");
// Tìm sản phẩm có giá cao nhất:
var highestProduct = products.OrderByDescending(p => p.Price).First();
Console.WriteLine($"{highestProduct.Name} - {highestProduct.Price} - {highestProduct.Category}");
Console.WriteLine("---------------------------------------------------------------------");
// Tạo danh sách các sản phẩm với tên đã viết hoa:
var upper = products.Select(p => new
{
    Name = p.Name.ToUpper(),
    Price = p.Price,
    Category = p.Category
});
foreach (var item in upper)
{
    Console.WriteLine($"{item.Name} - {item.Price} - {item.Category}");
}