﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;
Console.OutputEncoding = System.Text.Encoding.UTF8;
var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);

//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.
var getByNameAndPrice = products.Where(p => p.Price > 100).OrderByDescending(p => p.Price).ToList();
foreach (var product in getByNameAndPrice)
{
    Console.WriteLine($"{product.Id} - {product.Name}");
}
////Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var filter = products.Join(categories,
                            product => product.CategoryId,
                            category => category.Id,
                            (product, category) => new
                            {
                                Id = product.Id,
                                Name = product.Name,
                                Price = product.Price,
                                Category = category.Name
                            }).GroupBy(category => category.Category).OrderBy(product => product.Key).ToList();
foreach (var item in filter)
{
    Console.WriteLine($"KEY-------{item.Key}");
    foreach (var product in item)
    {
        Console.WriteLine($"{product.Id} - {product.Name} - {product.Price}");
    }
}
Console.WriteLine("-------------------------------------------------------------------------------------------------");
////Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
foreach (var item in filter)
{
    Console.WriteLine($"KEY-------{item.Key}");
    foreach (var product in item)
    {
        Console.WriteLine($"{product.Id} - {product.Name} - {product.Price}");
    }
    Console.WriteLine($"Average price: {Math.Round(item.Average(p => p.Price), 2)}");
}
//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.
var top10 = products.OrderByDescending(p => p.Price).Take(10).ToList();
foreach (var item in top10)
{
    Console.WriteLine($"{item.Id} - {item.Name} - {item.Price}");
}
////Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.
var getAvg = categories
    .GroupJoin(products, category => category.Id, product => product.CategoryId, (category, productGroup) => new
    {
        Category = category.Name,
        AvgOfProducts = productGroup.Average(x => x.Price)
    })
    .OrderByDescending(x => x.AvgOfProducts);

foreach (var item in getAvg)
{
    Console.WriteLine($"{item.Category}: {Math.Round(item.AvgOfProducts, 2)} on average");
}
////Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.
var nameOfProductAndCategory = products.Join(categories, products => products.CategoryId, categories => categories.Id, (products, categories) => new
{
    ProductName = products.Name,
    ProductPrice = products.Price,
    CategoryName = categories.Name
}).Where(products => products.ProductPrice > 150).OrderBy(categories => categories.CategoryName).ToList();
foreach (var item in nameOfProductAndCategory)
{
    Console.WriteLine($"{item.ProductName} - {item.ProductPrice} - {item.CategoryName}");
}
//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var sumPriceOfEachCategory = products.GroupBy(p => p.CategoryId).Select(group => new
{
    CategoryName = categories.First(category => category.Id == group.Key).Name,
    TotalPrice = group.Sum(product => product.Price)
}).OrderBy(item => item.CategoryName);
foreach (var item in sumPriceOfEachCategory)
{
    Console.WriteLine($"{item.CategoryName} - {item.TotalPrice}");
}
////Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.
var getByNameApple = products.Where(product => product.Name.Contains("Apple")).OrderBy(product => product.Name).Select(element => new
{
    ProductName = element.Name,
    Category = categories.First(category => category.Id == element.CategoryId).Name
}).ToList();
foreach (var item in getByNameApple)
{
    Console.WriteLine($"{item.ProductName}-{item.Category}");
}
//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.
var getByCategory = products.GroupBy(product => product.CategoryId).Select(element => new
{
    CategoryName = categories.First(category => category.Id == element.Key).Name,
    TotalPrice = element.Sum(p => p.Price)
}).Where(item => item.TotalPrice > 1000).ToList();
foreach (var item in getByCategory)
{
    Console.WriteLine($"{item.CategoryName}-{item.TotalPrice}");
}
////Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.
var categoriesWithLowPriceProducts = products
                                        .Where(product => product.Price < 10)
                                            .Select(product => categories.First(category => category.Id == product.CategoryId).Name)
                                                .Distinct()
                                                    .ToList();
if (categoriesWithLowPriceProducts.Any())
{
    Console.WriteLine("Danh mục có sản phẩm giá nhỏ hơn 10:");
    foreach (var categoryName in categoriesWithLowPriceProducts)
    {
        Console.WriteLine(categoryName);
    }
}
else
{
    Console.WriteLine("Không có danh mục nào có sản phẩm giá nhỏ hơn 10.");
}

////Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.
var maxPriceProductsInEachCategory = products
            .GroupBy(product => product.CategoryId)
            .Select(element => element.OrderByDescending(product => product.Price).First())
            .OrderBy(product => categories.First(c => c.Id == product.CategoryId).Name);

foreach (var product in maxPriceProductsInEachCategory)
{
    Console.WriteLine($"Category: {categories.First(c => c.Id == product.CategoryId).Name}, Product: {product.Name}, Price: {product.Price}");
}
////Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.
var getByCategoryAndPrice = products.GroupBy(product => product.CategoryId).Select(element => new
{
    CategoryName = categories.First(category => category.Id == element.Key).Name,
    TotalPrice = element.Sum(product => product.Price)
}).OrderByDescending(item => item.TotalPrice);

foreach (var item in getByCategoryAndPrice)
{
    Console.WriteLine($"Category: {item.CategoryName}, Total Price: {item.TotalPrice}");
}
//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.
var result = products
          .Where(p => p.Price > products.Average(p => p.Price))
          .Select(p => new
          {
              ProductName = p.Name,
              CategoryName = categories.First(c => c.Id == p.CategoryId).Name
          });

foreach (var item in result)
{
    Console.WriteLine($"Product: {item.ProductName}, Category: {item.CategoryName}");
}
//Tính tổng số tiền của tất cả các sản phẩm.
var totalPriceOfProduct = products.Sum(p => p.Price);
Console.WriteLine($"{totalPriceOfProduct}");


