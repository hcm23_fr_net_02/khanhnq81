﻿var students = new List<Student>
{
new Student { Name = "Alice", Grade = 'A' },
new Student { Name = "Bob", Grade = 'B' },
new Student { Name = "Charlie", Grade = 'A' },
new Student { Name = "David", Grade = 'A' }
};


var groupByGrade = students.GroupBy(student => student.Grade).ToList();
foreach (var student in groupByGrade)
{
    Console.WriteLine($"Grade: {student.Key}");
    foreach (var infor in student) 
    {
        Console.WriteLine($"{infor.Name}");
    }
    Console.WriteLine("\n");
}