﻿List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

var sort = numbers.GroupBy(x => x % 2 == 0 ? "Even" : "Odd");
foreach (var item in sort)
{
    if (item.Key.Equals("Even"))
    {
        Console.WriteLine($"Even");
        foreach (var even in item)
        {
            Console.WriteLine(even);
        }
        var sumEven = item.Sum();
        Console.WriteLine($"Total even sum: {sumEven}");
    }
    if (item.Key.Equals("Odd"))
    {
        Console.WriteLine($"Odd");
        foreach (var odd in item)
        {
            Console.WriteLine(odd);
        }
        var sumOdd = item.Sum();
        Console.WriteLine($"Total odd sum: {sumOdd}");
    }
}