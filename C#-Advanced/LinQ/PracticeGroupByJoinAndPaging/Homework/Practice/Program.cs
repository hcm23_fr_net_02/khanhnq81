﻿var fruits = new List<string> {"apple","banana","orange","pineapple","mango"};
// gom nhóm các chuỗi sau theo độ dài chuỗi
var groupByLength = fruits.GroupBy(fruit => fruit.Length).ToList();
// in nhóm và các chuỗi trong 1 nhóm ra
foreach (var fruit in groupByLength)
{
    Console.WriteLine($"Length: {fruit.Key}");
	foreach (var item in fruit)
	{
        Console.WriteLine(item);
    }
    Console.WriteLine("\n");
}