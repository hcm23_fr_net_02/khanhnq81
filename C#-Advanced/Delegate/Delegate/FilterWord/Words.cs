﻿namespace Homework_Predicate
{
    public class Words
    {
        private string word;
        public string Word
        {
            get => word;
            set => word = value;
        }
        public int Length
        {
            get => word.Length;
        }
    }
}
