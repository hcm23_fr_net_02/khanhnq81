﻿using Homework_Predicate;

FilterWord filterWord = new FilterWord();
Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Words word = new Words();
                Console.WriteLine("Enter word: ");
                word.Word = Console.ReadLine();
                filterWord.AddWord(word);
                break;
            case 2:
                Console.WriteLine(filterWord.ShowAllWord());
                break;
            case 3:
                Console.WriteLine(filterWord.ShowAfterFilter());
                break;
            case 4:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);