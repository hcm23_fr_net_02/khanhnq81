﻿using System.Text;

namespace Homework_Predicate
{
    public class FilterWord
    {
        List<Words> words;
        List<Words> filterList;
        Predicate<Words> predicate;
        public FilterWord()
        {
            words = new List<Words>();
            filterList = new List<Words>();
            predicate = Filter;
        }
        public bool Filter(Words word) => word.Length >= 6;
        public void AddWord(Words word)
        {
            words.Add(word);
        }
        public string ShowAllWord()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Words word in words)
            {
                sb.Append($"{word.Word} | ");
            }
            return sb.ToString();
        }
        public string ShowAfterFilter()
        {
            StringBuilder sb = new StringBuilder();
            Filter();
            foreach (Words word in filterList)
            {
                sb.Append($"{word.Word} | ");
            }
            return sb.ToString();
        }
        private void Filter()
        {
            foreach (Words word in words)
            {
                if (predicate(word))
                {
                    filterList.Add(word);
                }
            }
        }
    }
}
