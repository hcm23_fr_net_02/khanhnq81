﻿//using DelegatePractice;

//MathOperation mathOperation = new MathOperation();
//Console.WriteLine(mathOperation.AdditionResult());

using System.Numerics;
using System.Runtime.ConstrainedExecution;

Operation add = Addition;
Operation subtract = Subtraction;
Operation multiple = Multiplication;
Operation division = Division;


int PerformOperation(int a, int b, Operation op)
{
    return op(a, b);
}
int result = PerformOperation(2,1,add);
int Addition(int a, int b) => a + b;
int Subtraction(int a, int b) => a - b;
int Multiplication(int a, int b) => a * b;
int Division(int a, int b) => a / b;
delegate int Operation(int a, int b);