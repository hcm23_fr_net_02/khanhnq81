﻿using System;

namespace DelegatePractice
{
    public class MathOperation
    {
        int a;
        int b;
        int result;
        private delegate int Operation(int a, int b);

        private int Addition(int a, int b) => a + b;
        private int Subtraction(int a, int b) => a - b;
        private int Multiplication(int a, int b) => a * b;
        private int Division(int a, int b) => a / b;

        private int PerformOperation(int a, int b, Operation op)
        {
            Console.WriteLine("Enter a: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter b: ");
            b = int.Parse(Console.ReadLine());
            return op(a, b);
        }


        public int AdditionResult()
        {
            result = PerformOperation(a, b, Addition);
            return result;
        }

        public int SubtractionResult()
        {
            result = PerformOperation(a, b, Subtraction);
            return result;
        }

        public int MultiplicationResult()
        {
            result = PerformOperation(a, b, Multiplication);
            return result;
        }

        public int DivisionResult()
        {
            result = PerformOperation(a, b, Division);
            return result;
        }
    }
}
