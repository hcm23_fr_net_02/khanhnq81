﻿namespace Generic_Delegate
{
    public abstract class Generic_Delegate<T>
    {
        protected delegate T MathOperation(T param1, T param2);
        protected virtual T PerformOperation(T param1, T param2, MathOperation operation) => operation(param1, param2);
        public abstract T SumTwoNumber();
        public abstract T SubstractTwoNumber();
        public abstract T MultiplyTwoNumber();
        public abstract T DivideTwoNumber();
    }
}
