﻿namespace Generic_Delegate
{
    public class Menu
    {
        public void MenuDisplay()
        {
            Console.WriteLine("============Menu============");
            Console.WriteLine("| 1. Add two number        |");
            Console.WriteLine("| 2. Sunstrac two number   |");
            Console.WriteLine("| 3. Multiply two number   |");
            Console.WriteLine("| 4. Divide two number     |");
            Console.WriteLine("| 5. Exit                  |");
            Console.WriteLine("============================");
            Console.Write("Choose: ");
        }
    }
}
