﻿namespace Generic_Delegate
{
    public class Calculate : Generic_Delegate<int>
    {
        private int FirstNumber;
        private int SecondNumber;
        private Func<int, int, int> Addition = (param1, param2) => param1 + param2;
        private Func<int, int, int> Substraction = (param1, param2) => param1 - param2;
        private Func<int, int, int> Multiply = (param1, param2) => param1 * param2;
        private Func<int, int, int> Divide = (param1, param2) => param1 / param2;

        protected override int PerformOperation(int param1, int param2, MathOperation operation)
        {
            return base.PerformOperation(param1, param2, operation);
        }
        private void Input()
        {
            Console.WriteLine("Enter first number: ");
            FirstNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter second number: ");
            SecondNumber = int.Parse(Console.ReadLine());
        }
        public override int SumTwoNumber()
        {
            Input();
            var sum = new MathOperation(Addition);
            var result = PerformOperation(FirstNumber, SecondNumber, sum);
            return result;
        }
        public override int SubstractTwoNumber()
        {
            Input();
            var sub = new MathOperation(Substraction);
            var result = PerformOperation(FirstNumber, SecondNumber, sub);
            return result;
        }

        public override int MultiplyTwoNumber()
        {
            Input();
            var mul = new MathOperation(Multiply);
            var result = PerformOperation(FirstNumber, SecondNumber, mul);
            return result;
        }

        public override int DivideTwoNumber()
        {
            Input();
            var div = new MathOperation(Divide);
            var result = PerformOperation(FirstNumber, SecondNumber, div);
            return result;
        }

    }
}
