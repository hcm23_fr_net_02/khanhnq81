using Get_Last_Item;

namespace TestGenericStack
{
    public class UnitTest1
    {
        [Fact]
        public void PopItem_ShouldReturn2_WhenPush1And2()
        {
            //Arrange
            var stack = new GenericStack<int>(2);
            stack.PushItem(1);
            stack.PushItem(2);
            var expected = 2;
            //Act
            var actual = stack.PopItem();   
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Peek_ShouldReturn2_WhenPush1And2()
        {
            var stack = new GenericStack<int>(2);
            stack.PushItem(1);
            stack.PushItem(2);
            var expected = 2;
            var actual = stack.PeekItem();
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Count_ShouldReturn0_WhenNotPush()
        {
            var stack = new GenericStack<int>(2);
            var expected = 0;
            var actual = stack.Count();
            Assert.Equal(expected, actual);
        }
    }
}