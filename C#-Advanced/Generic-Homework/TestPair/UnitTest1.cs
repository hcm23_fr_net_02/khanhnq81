using Pair.Class;

namespace TestPair
{
    public class UnitTest1
    {
        [Fact]
        public void Pair_ShouldReturnStringAndInt_WhenTIsStringAndKIsInt()
        {
            //Arrange
            string expectedString = "I Love You";
            int expectedInt = 3000;
            //Act
            var pair = new Pair<string, int>("I Love You", 3000);
            var actualString = pair.First;
            var actualInt = pair.Second;
            //Assert
            Assert.Equal(expectedString, actualString);
            Assert.Equal(expectedInt, actualInt);

        }
    }
}