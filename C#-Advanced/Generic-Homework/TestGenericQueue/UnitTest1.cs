using Generic;

namespace TestGenericQueue
{
    public class UnitTest1
    {
        [Fact]
        public void Dequeue_ShouldReturn1_WhenEnqueue1()
        {
            //Arrange
            var queue = new GenericQueue<int>(1);
            queue.Enqueue(1);
            var expected = 1;
            //Act
            var actual = queue.Dequeue();
            //Assert 
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Peek_ShouldReturn1_WhenEnqueue1()
        {
            var queue = new GenericQueue<int>(1);
            queue.Enqueue(1);
            var expected = 1;
            var actual = queue.Peek();
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void IsEmpty_ShouldReturnFalse_WhenEnqueue1()
        {
            var queue = new GenericQueue<int>(1);
            queue.Enqueue(1);
            var expected = false;
            var actual = queue.IsEmpty();
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Count_ShouldReturn0_WhenNotEnqueue()
        {
            var queue = new GenericQueue<int>(1);
            var expect = 0;
            var actual = queue.Count();
            Assert.Equal(expect, actual);
        }
    }
}