﻿namespace Get_Last_Item
{
    public class GenericStack<T>
    {
        T[] items;
        int _maxStack;
        int bot = -1;
        public GenericStack(int maxStack)
        {
            _maxStack = maxStack;
            items = new T[maxStack];
        }
        public void PushItem(T item)
        {
            bot++;
            if (bot == _maxStack)
            {
                bot--;
                throw new Exception("The box is full");
            }
            items[bot] = item;
        }

        public T PopItem()
        {
            if (bot == -1)
            {
                bot = -1;
                throw new Exception("The box is empty");
            }
            var item = items[bot];
            bot--;
            return item;
        }

        public T PeekItem()
        {
            if (bot == -1)
            {
                throw new Exception("The box is empty");
            }
            var item = items[bot];
            return item;
        }
        public void Clear()
        {
            bot = -1;
        }

        public int Count()
        {
            return bot + 1;
        }

    }
}
