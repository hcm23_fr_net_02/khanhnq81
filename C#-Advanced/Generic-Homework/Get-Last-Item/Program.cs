﻿using Get_Last_Item;

var theBox = new GenericStack<int>(2);
int choice = 0;
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine("Enter the item: ");
                int item = int.Parse(Console.ReadLine());
                theBox.PushItem(item);
                break;
            case 2:
                Console.WriteLine(theBox.PopItem());
                break;
            case 3:
                Console.WriteLine(theBox.PeekItem());
                break;
            case 4:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);