﻿namespace Get_Last_Item
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("===========MENU===========");
            Console.WriteLine("| 1. Push item           |");
            Console.WriteLine("| 2. Pop item            |");
            Console.WriteLine("| 3. Peek item           |");
            Console.WriteLine("| 4. Exit                |");
            Console.WriteLine("==========================");
            Console.Write("Choice: ");
        }
    }
}
