﻿using Generic;
using System.Collections;

var integerQueue = new GenericQueue<int>(2);
int choice = 0;
do
{
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine("Enter item: ");
                int item = int.Parse(Console.ReadLine());
                integerQueue.Enqueue(item);
                break;
            case 2:
                var dequeue = integerQueue.Dequeue();
                Console.WriteLine($"Dequeue: {dequeue}");
                break;
            case 3:
                var peek = integerQueue.Peek();
                Console.WriteLine($"Peek: {peek}");
                break;
            case 4:
                var check = integerQueue.IsEmpty();
                Console.WriteLine($"Is empty: {check}");
                break;
            case 5:
                integerQueue.Clear();
                break;
            case 6:
                int count = integerQueue.Count();
                Console.WriteLine($"Number of element: {count}");
                break;
            case 7:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);