﻿namespace Generic
{
    public class GenericQueue<T>
    {
        T[] queue;
        int _maxQueue;
        int top = -1;
        int bot = -1;
        public GenericQueue(int maxQueue)
        {
            _maxQueue = maxQueue;
            queue = new T[maxQueue];
            top = 0;
        }

        public void Enqueue(T item)
        {
            bot++;
            if (bot == _maxQueue)
            {
                bot--;
                throw new Exception("Queue is full");
            }
            queue[bot] = item;
        }

        public T Dequeue()
        {
            
            if (bot == -1)
            {
                throw new Exception("Queue is empty");
            }
            if (top == bot)
            {
                var value = queue[top];
                top = 0;
                bot = -1;
                return value;
            }
            else
            {
                var valueAtTop = queue[top];
                top++;
                return valueAtTop;
            }
        }

        public T Peek()
        {
            var item = queue[top];
            if (bot == -1)
            {
                throw new Exception("Queue is empty");
            }
            return item;
        }

        public bool IsEmpty()
        {
            if(bot == -1)
            {
                return true;
            }
            return false;
        }

        public void Clear()
        {
            top = 0;
            bot = -1;
        }

        public int Count()
        {
            int count = bot - top + 1;
            return count;
        }
    }
}
