﻿using Pair.Class;

var firstPair = new Pair<int, string>(1, "Khanh");
Console.WriteLine($"{firstPair.First} - {firstPair.Second}");
var secondPair = new Pair<string, string>("Khanh", "Depzai");
Console.WriteLine($"{secondPair.First}-{secondPair.Second}");
var thirdPair = new Pair<char, char>('K', 'U');
Console.WriteLine($"{thirdPair.First} - {thirdPair.Second}");
var fourthPair = new Pair<double, double>(3.2, 4.3);
Console.WriteLine($"{fourthPair.First} - {fourthPair.Second}");