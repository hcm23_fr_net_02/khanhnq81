﻿namespace Pair.Class
{
    public class Pair<T,K>
    {
        public T First { get; set; }
        public K Second { get; set; }
        public Pair(T first, K second)
        {
            First = first; 
            Second = second;
        }
    }
}
