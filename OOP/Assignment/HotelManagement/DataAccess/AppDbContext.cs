﻿using HotelManagement.Model;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.DataAccess
{
    public class AppDbContext : DbContext
    {
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<RoomDetail> RoomDetails { get; set; }
        //--------------------------------------------------------
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Customer> Customers { get; set; }
        //--------------------------------------------------------
        public DbSet<Invoice> Invoices { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoomDetail>()
                                .HasOne(room => room.Room)
                                .WithMany(detail => detail.RoomDetails)
                                .HasForeignKey(detail => detail.RoomId);

            modelBuilder.Entity<RoomDetail>()
                                .HasOne(service => service.Service)
                                .WithMany(detail => detail.RoomDetails)
                                .HasForeignKey(detail => detail.ServiceId);

            modelBuilder.Entity<RoomDetail>().HasKey(sc => new { sc.RoomId, sc.ServiceId });
            //--------------------------------------------------------------------------------

            modelBuilder.Entity<Booking>()
                                .HasOne(customer => customer.Customer)
                                .WithMany(booking => booking.Bookings)
                                .HasForeignKey(booking => booking.CustomerId);

            modelBuilder.Entity<Service>()
                                .HasData
                                    (
                                    new Service { Id = 1, ServiceName = "Wifi", ServicePrice = 100 },
                                    new Service { Id = 2, ServiceName = "Cafe", ServicePrice = 200 },
                                    new Service { Id = 3, ServiceName = "Buffet", ServicePrice = 300 }
                                    );
        }
    }
}
