﻿using HotelManagement.DataAccess.Repository.Implement;
using HotelManagement.DataAccess.Repository.IRepository;

namespace HotelManagement.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        private readonly ICustomerRepository _customerRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IBookingRepository _bookingRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IRoomDetail _roomDetail;
        private readonly IServiceRepository _serviceRepository;
        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _customerRepository = new CustomerRepository(appDbContext);
            _roomRepository = new RoomRepository(appDbContext);
            _bookingRepository = new BookingRepository(appDbContext);
            _invoiceRepository = new InvoiceRepository(appDbContext);
            _roomDetail = new RoomDetailRepository(appDbContext);
            _serviceRepository = new ServiceRepository(appDbContext);
        }
        public ICustomerRepository CustomerRepository { get => _customerRepository; }

        public IRoomRepository RoomRepository { get => _roomRepository; }

        public IBookingRepository BookingRepository { get => _bookingRepository; }

        public IInvoiceRepository InvoiceRepository { get => _invoiceRepository; }

        public IServiceRepository ServiceRepository { get => _serviceRepository; }

        public IRoomDetail RoomDetail { get => _roomDetail; }

        public int SaveChange() => _appDbContext.SaveChanges();
    }
}
