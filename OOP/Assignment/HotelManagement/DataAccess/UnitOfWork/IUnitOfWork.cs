﻿using HotelManagement.DataAccess.Repository.IRepository;

namespace HotelManagement.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        int SaveChange();
        ICustomerRepository CustomerRepository { get; }
        IRoomRepository RoomRepository { get; }
        IBookingRepository BookingRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        IServiceRepository ServiceRepository { get; }
        IRoomDetail RoomDetail { get; }
    }
}
