﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.DataAccess
{
     public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
     {
         public AppDbContext CreateDbContext(string[] args)
         {
             string connectionString = "Data Source= KHANHS; Database = HotelManagementAsm; Trusted_Connection = True; TrustServerCertificate = True";
             var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
             optionBuilder.UseSqlServer(connectionString);
             return new AppDbContext(optionBuilder.Options);
         }
     }

}

