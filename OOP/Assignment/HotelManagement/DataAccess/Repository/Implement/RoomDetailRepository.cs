﻿using HotelManagement.DataAccess.Repository.IRepository;
using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class RoomDetailRepository : GenericRepository<RoomDetail>, IRoomDetail
    {
        public RoomDetailRepository(AppDbContext context) : base(context)
        {
        }
    }
}
