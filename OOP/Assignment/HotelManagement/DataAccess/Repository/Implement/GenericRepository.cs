﻿using HotelManagement.DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected DbSet<T> _dbSet;
        public GenericRepository(AppDbContext context) => _dbSet = context.Set<T>();
        public void AddNew(T enity) => _dbSet.Add(enity);

        public void Delete(T enity) => _dbSet.Remove(enity);

        public List<T> GetAll() => _dbSet.ToList();

        public void Update(T enity) => _dbSet.Update(enity);
    }
}
