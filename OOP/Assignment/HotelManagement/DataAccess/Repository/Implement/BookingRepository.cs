﻿using HotelManagement.DataAccess.Repository.IRepository;
using HotelManagement.Model;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class BookingRepository : GenericRepository<Booking>, IBookingRepository
    {
        private readonly AppDbContext _context;
        public BookingRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Booking FindById(int id)
        {
            var booking = _context.Bookings.FirstOrDefault(booking => booking.Id == id);
            if (booking is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return booking;
        }

        public List<Booking> GetAllReference()
        {
            var list = _context.Bookings.Include(booking => booking.Customer).Include(booking => booking.Room).ToList();
            return list;
        }
    }
}
