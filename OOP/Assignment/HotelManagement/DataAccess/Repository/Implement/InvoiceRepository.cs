﻿using HotelManagement.DataAccess.Repository.IRepository;
using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class InvoiceRepository : GenericRepository<Invoice>, IInvoiceRepository
    {
        private readonly AppDbContext _context;
        public InvoiceRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Invoice FindById(int id)
        {
            var invoice = _context.Invoices.FirstOrDefault(invoice => invoice.Id == id);
            if (invoice == null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return invoice;
        }
    }
}
