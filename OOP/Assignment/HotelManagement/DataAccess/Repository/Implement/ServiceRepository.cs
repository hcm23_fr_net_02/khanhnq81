﻿using HotelManagement.DataAccess.Repository.IRepository;
using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class ServiceRepository : GenericRepository<Service>, IServiceRepository
    {
        private readonly AppDbContext _context;
        public ServiceRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Service FindServiceById(int id)
        {
            var service = _context.Services.FirstOrDefault(service => service.Id == id);
            if (service is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return service;
        }
    }
}
