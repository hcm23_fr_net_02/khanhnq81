﻿using HotelManagement.DataAccess.Repository.IRepository;
using HotelManagement.Model;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class RoomRepository : GenericRepository<Room>, IRoomRepository
    {
        private readonly AppDbContext _context;
        public RoomRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Room FindById(int id)
        {
            var room = _context.Rooms.FirstOrDefault(room => room.RoomId == id);
            if (room is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return room;
        }

        public List<Room> GetAllReference()
        {
            var list = _context.Rooms.Include(room => room.RoomDetails).ThenInclude(service => service.Service).ToList();
            return list;
        }
    }
}
