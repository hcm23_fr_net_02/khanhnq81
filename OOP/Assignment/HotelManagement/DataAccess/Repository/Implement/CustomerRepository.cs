﻿using HotelManagement.DataAccess.Repository.IRepository;
using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.Implement
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        private readonly AppDbContext _context;
        public CustomerRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Customer FindById(int id)
        {
            var customer = _context.Customers.FirstOrDefault(customer => customer.Id == id);
            if (customer is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return customer;
        }
    }
}
