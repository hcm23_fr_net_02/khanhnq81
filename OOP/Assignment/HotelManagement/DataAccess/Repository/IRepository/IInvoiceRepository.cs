﻿using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface IInvoiceRepository : IGenericRepository<Invoice>
    {
        Invoice FindById(int id);
    }
}
