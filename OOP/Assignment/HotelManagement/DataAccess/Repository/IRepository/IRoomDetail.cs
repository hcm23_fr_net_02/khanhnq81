﻿using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface IRoomDetail : IGenericRepository<RoomDetail>
    {
    }
}
