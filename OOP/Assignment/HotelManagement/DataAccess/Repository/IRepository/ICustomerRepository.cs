﻿using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Customer FindById(int id);
    }
}
