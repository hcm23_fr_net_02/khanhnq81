﻿using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface IServiceRepository : IGenericRepository<Service>
    {
        Service FindServiceById(int id);
    }
}
