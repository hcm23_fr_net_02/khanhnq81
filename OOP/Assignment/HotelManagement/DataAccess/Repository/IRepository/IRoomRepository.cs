﻿using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface IRoomRepository : IGenericRepository<Room>
    {
        Room FindById(int id);
        List<Room> GetAllReference();
    }
}
