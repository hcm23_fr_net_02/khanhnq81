﻿namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> GetAll();
        void AddNew(T enity);
        void Update(T enity);
        void Delete(T enity);
    }
}
