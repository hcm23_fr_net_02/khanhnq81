﻿using HotelManagement.Model;

namespace HotelManagement.DataAccess.Repository.IRepository
{
    public interface IBookingRepository : IGenericRepository<Booking>
    {
        List<Booking> GetAllReference();
        Booking FindById(int id);
    }
}
