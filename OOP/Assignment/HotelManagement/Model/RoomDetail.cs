﻿namespace HotelManagement.Model
{
    public class RoomDetail
    {
        public Room Room { get; set; }
        public Service Service { get; set; }
        public int RoomId { get; set; }
        public int ServiceId { get; set; }
    }
}
