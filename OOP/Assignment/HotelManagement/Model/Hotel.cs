﻿namespace HotelManagement.Model
{
    public class Hotel
    {
        public static List<Room> Rooms { get; set; } = new List<Room>();
        public static List<Customer> Customers { get; set; } = new List<Customer>();
        public static List<Booking> Bookings { get; set; } = new List<Booking>();
        public static List<RoomDetail> RoomDetails { get; set; } = new List<RoomDetail>();
    }
}
