﻿using HotelManagement.Model.Enum;

namespace HotelManagement.Model
{
    public class Room
    {
        public int RoomId { get; set; }
        public decimal Price { get; set; }
        public bool IsBooked { get; set; } = false;
        public HotelEnum.RoomType RoomType { get; set; }
        public List<RoomDetail> RoomDetails { get; set; }

    }
}
