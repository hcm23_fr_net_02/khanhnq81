﻿namespace HotelManagement.Model
{
    public class Service
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public decimal ServicePrice { get; set; }
        public List<RoomDetail> RoomDetails { get; set; }
    }
}
