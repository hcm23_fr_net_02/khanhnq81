﻿namespace HotelManagement.Model
{
    public class Booking
    {
        private DateTime _payDay;
        public int Id { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime PayDay 
        {
            get => _payDay;
            set
            {
                if(value <= BookingDate)
                {
                    throw new Exception($"Payday must higher than booking date !");
                }
                _payDay = value;
            }
        }
        public string SpecialRequires { get; set; }
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public Room Room { get; set; }
        public int RoomId { get; set; }
    }
}
