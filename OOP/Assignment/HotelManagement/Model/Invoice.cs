﻿namespace HotelManagement.Model
{
    public class Invoice
    {
        public int Id { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime DateOfInvoice { get; set; }
    }
}
