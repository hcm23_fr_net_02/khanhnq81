﻿using HotelManagement.Bussiness;
using HotelManagement.DataAccess.UnitOfWork;
using HotelManagement.Helper;
using HotelManagement.Model;

Console.WriteLine();
var context = AccessHelper.GetContext();
IUnitOfWork unitOfWork = new UnitOfWork(context);
RoomService roomService = new RoomService(unitOfWork);
BookingService bookingService = new BookingService(unitOfWork);
CustomerService customerService = new CustomerService(unitOfWork);
InvoiceService invoiceService = new InvoiceService(unitOfWork);

int choice = 0;
do
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Room room = new Room();
                RoomDetail roomDetail = new RoomDetail();
                roomService.AddNewRoom(room, roomDetail);
                break;
            case 2:
                Console.WriteLine("Enter room id: ");
                int id = int.Parse(Console.ReadLine());
                roomService.RemoveRoom(id);
                break;
            case 3:
                roomService.ShowAllRoom();
                break;
            case 4:
                Booking booking = new Booking();
                bookingService.Booking(booking);
                unitOfWork.SaveChange();
                break;
            case 5:
                bookingService.ShowAllBooking();
                break;
            case 6:
                Console.WriteLine("Enter booking id: ");
                int bookingId = int.Parse(Console.ReadLine());
                invoiceService.CreateInvoice(bookingId);
                break;
            case 7:
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 7);