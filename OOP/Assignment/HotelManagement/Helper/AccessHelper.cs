﻿using HotelManagement.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.Helper
{
    public class AccessHelper
    {
        private const string ConnectionString = "Data Source= KHANHS; Database = HotelManagementAsm; Trusted_Connection = True; TrustServerCertificate = True";
        public static AppDbContext GetContext()
        {
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionBuilder.UseSqlServer(ConnectionString);
            var context = new AppDbContext(optionBuilder.Options);
            return context;
        }
    }
}
