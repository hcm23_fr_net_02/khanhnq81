﻿namespace HotelManagement.Helper
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("============================MENU============================");
            Console.WriteLine("| 1. Thêm phòng mới vào khách sạn                          |");
            Console.WriteLine("| 2. Xóa phòng khỏi khách sạn                              |");
            Console.WriteLine("| 3. Hiển thị danh sách các phòng trong khách sạn          |");
            Console.WriteLine("| 4. Cho phép khách hàng đặt phòng                         |");
            Console.WriteLine("| 5. Hiển thị thông tin về các đặt phòng hiện có           |");
            Console.WriteLine("| 6. Tạo và hiển thị hóa đơn thanh toán cho khách hàng     |");
            Console.WriteLine("| 7. Exit !                                                |");
            Console.WriteLine("============================================================");
        }
    }
}
