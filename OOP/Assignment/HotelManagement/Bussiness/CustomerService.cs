﻿using HotelManagement.DataAccess.UnitOfWork;
using HotelManagement.Model;

namespace HotelManagement.Bussiness
{
    public class CustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _unitOfWork.CustomerRepository.AddNew(new Customer { Name = "Khanh", Phone = "0961504350" });
            _unitOfWork.CustomerRepository.AddNew(new Customer { Name = "Hoho", Phone = "0961111111" });
            _unitOfWork.CustomerRepository.AddNew(new Customer { Name = "1", Phone = "0123456789" });
            _unitOfWork.CustomerRepository.AddNew(new Customer { Name = "2", Phone = "0123456987" });
            _unitOfWork.CustomerRepository.AddNew(new Customer { Name = "3", Phone = "0123456879" });
            _unitOfWork.CustomerRepository.AddNew(new Customer { Name = "4", Phone = "0123456888" });
        }

        public void AddNewCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.AddNew(customer);
        }

        public void DeleteCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Delete(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Update(customer);
        }
    }
}
