﻿using HotelManagement.DataAccess.UnitOfWork;
using HotelManagement.Model;

namespace HotelManagement.Bussiness
{
    public class BookingService
    {
        private readonly IUnitOfWork _unitOfWork;
        public BookingService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Booking(Booking booking)
        {
            Console.Clear();
            Console.WriteLine("Enter room id: ");
            booking.RoomId = int.Parse(Console.ReadLine());
            var room = _unitOfWork.RoomRepository.FindById(booking.RoomId);
            if (room.IsBooked is true)
            {
                throw new Exception("Room has been booked");
            }
            Console.WriteLine("Enter customer id: ");
            booking.CustomerId = int.Parse(Console.ReadLine());
            _unitOfWork.CustomerRepository.FindById(booking.CustomerId);
            Console.WriteLine("Enter booking date: ");
            booking.BookingDate = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter pay day: ");
            booking.PayDay = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter special require: ");
            booking.SpecialRequires = Console.ReadLine();
            _unitOfWork.BookingRepository.AddNew(booking);
            room.IsBooked = true;
            _unitOfWork.RoomRepository.Update(room);
            Console.Clear();
            Console.WriteLine($"Total price: {TotalPrice(room, booking)}");
        }

        private decimal TotalPrice(Room room, Booking booking)
        {
            decimal totalPrice = 0;
            var totalServicePrice = room.RoomDetails.Sum(service => service.Service.ServicePrice);
            var days = booking.PayDay - booking.BookingDate;
            totalPrice = days.Days * (totalServicePrice + room.Price);
            return totalPrice;
        }

        public void ShowAllBooking(int currentPage = 0, int pageSize = 10)
        {
            Hotel.Bookings = _unitOfWork.BookingRepository.GetAllReference();
            var totalPage = Math.Ceiling((Hotel.Bookings.Count / pageSize) * 1.0);
            while (true)
            {
                Console.Clear();
                var bookings = Hotel.Bookings.Skip(currentPage * pageSize).Take(pageSize).ToList();
                foreach (var item in bookings)
                {
                    Console.WriteLine($"Booking Id: {item.Id} | Customer Name: {item.Customer.Name} | Room id: {item.RoomId} | Booking date: {item.BookingDate.ToString("dd/MM/yyyy")} | Pay day: {item.PayDay.ToString("dd/MM/yyyy")}\n Special Require: {item.SpecialRequires}");
                }
                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage < totalPage - 1)
                    {
                        currentPage++;
                    }
                }
                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }
                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }
            Console.Clear();
        }
    }
}
