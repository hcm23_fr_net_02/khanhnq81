﻿using HotelManagement.DataAccess.UnitOfWork;
using HotelManagement.Model;
using System.Text;
using static HotelManagement.Model.Enum.HotelEnum;

namespace HotelManagement.Bussiness
{
    public class RoomService
    {
        private readonly IUnitOfWork _unitOfWork;
        public RoomService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddNewRoom(Room room, RoomDetail detail)
        {
            Console.Clear();
            Console.WriteLine("Enter price: ");
            room.Price = decimal.Parse(Console.ReadLine());
            Console.WriteLine("Enter room type: ");
            string type = Console.ReadLine();
            if (Enum.TryParse<RoomType>(type, true, out RoomType roomtype))
            {
                room.RoomType = roomtype;
            }
            _unitOfWork.RoomRepository.AddNew(room);
            _unitOfWork.SaveChange();
            Console.WriteLine("Number of service: ");
            int numberOfService = int.Parse(Console.ReadLine());
            for (int i = 0; i < numberOfService; i++)
            {
                Console.WriteLine("Enter service id: ");
                int serviceId = int.Parse(Console.ReadLine());
                _unitOfWork.ServiceRepository.FindServiceById(serviceId);
                detail.ServiceId = serviceId;
                detail.RoomId = room.RoomId;
                _unitOfWork.RoomDetail.AddNew(detail);
                _unitOfWork.SaveChange();
            }
            Console.Clear();
        }

        public void RemoveRoom(int roomId)
        {
            Console.Clear();
            var room = _unitOfWork.RoomRepository.FindById(roomId);
            _unitOfWork.RoomRepository.Delete(room);
            var check = _unitOfWork.SaveChange();
            if(check == 0)
            {
                throw new Exception("Remove fail !");
            }
            Console.WriteLine("Remove success");
        }

        public void ShowAllRoom(int currentPage = 0, int pageSize = 10)
        {
            Hotel.Rooms = _unitOfWork.RoomRepository.GetAllReference();
            var totalPage = Math.Ceiling((Hotel.Rooms.Count / pageSize) * 1.0);
            while (true)
            {
                Console.Clear();
                var rooms = Hotel.Rooms.Skip(currentPage * pageSize).Take(pageSize).ToList();
                foreach (var item in rooms)
                {
                    Console.WriteLine($"Room id: {item.RoomId} | Price: {item.Price} | Status: {item.IsBooked}");
                    foreach (var service in item.RoomDetails)
                    {
                        Console.WriteLine($"\tService name: {service.Service.ServiceName} | Service price: {service.Service.ServicePrice}");
                    }
                }
                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage < totalPage - 1)
                    {
                        currentPage++;
                    }
                }
                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }
                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }
            Console.Clear();
        }
    }
}
