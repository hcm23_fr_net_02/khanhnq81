﻿using HotelManagement.DataAccess.UnitOfWork;
using HotelManagement.Model;

namespace HotelManagement.Bussiness
{
    public class InvoiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        public InvoiceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void CreateInvoice(int bookingId)
        {
            Console.Clear();
            var booking = _unitOfWork.BookingRepository.FindById(bookingId);
            var room = _unitOfWork.RoomRepository.FindById(booking.RoomId);
            Invoice invoice = new Invoice();
            invoice.TotalPrice = TotalPrice(room, booking);
            invoice.DateOfInvoice = DateTime.Now;
            _unitOfWork.InvoiceRepository.AddNew(invoice);
            _unitOfWork.SaveChange();
            ShowInvoice(invoice.Id);
        }

        private decimal TotalPrice(Room room, Booking booking)
        {
            decimal totalPrice = 0;
            var totalServicePrice = room.RoomDetails.Sum(service => service.Service.ServicePrice);
            var days = booking.PayDay - booking.BookingDate;
            totalPrice = days.Days * (totalServicePrice + room.Price);
            return totalPrice;
        }

        private void ShowInvoice(int id)
        {
            var invoice = _unitOfWork.InvoiceRepository.FindById(id);
            Console.WriteLine($"Id: {invoice.Id} | Total: {invoice.TotalPrice} | Date: {invoice.DateOfInvoice.ToString("dd/MM/yyyy")}");
        }
    }
}
