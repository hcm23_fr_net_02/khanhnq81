﻿using PhoneStoreManagement.DataAccess.IRepository;
using PhoneStoreManagement.DataAccess.Repository;

namespace PhoneStoreManagement.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IPhoneRepository _phoneRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _phoneRepository = new PhoneRepository(context);
            _customerRepository = new CustomerRepository(context);
            _orderRepository = new OrderRepository(context);
        }
        public IPhoneRepository PhoneRepository { get => _phoneRepository; }

        public IOrderRepository OrderRepository { get => _orderRepository; }

        public ICustomerRepository CustomerRepository { get => _customerRepository; }

        public int SaveChange()
        {
            return _context.SaveChanges();
        }
    }
}
