﻿using PhoneStoreManagement.DataAccess.IRepository;

namespace PhoneStoreManagement.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        int SaveChange();
        IPhoneRepository PhoneRepository { get; }
        IOrderRepository OrderRepository { get; }
        ICustomerRepository CustomerRepository { get; }
    }
}
