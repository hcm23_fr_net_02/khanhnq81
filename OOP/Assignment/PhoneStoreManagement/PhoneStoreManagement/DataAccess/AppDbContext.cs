﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess
{
    public class AppDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<MobilePhone> MobilePhones { get; set; }
        public DbSet<Order> Orders { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Data Source = KHANHS; Database = PhoneStoreManagement; Trusted_Connection = True; TrustServerCertificate = True";
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                        .HasMany(order => order.Orders)
                        .WithOne(customer => customer.Customer)
                        .HasForeignKey(order => order.CustomerID);

            modelBuilder.Entity<Order>()
                        .HasOne(customer => customer.Customer)
                        .WithMany(order => order.Orders)
                        .HasForeignKey(order => order.CustomerID);

            modelBuilder.Entity<Order>()
                        .HasOne(phone => phone.MobilePhone)
                        .WithMany(order => order.Orders)
                        .HasForeignKey(order => order.PhoneID);

            modelBuilder.Entity<Order>().HasKey(sc => new { sc.ID, sc.PhoneID, sc.CustomerID });
        }
    }
}
