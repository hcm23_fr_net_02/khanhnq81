﻿using PhoneStoreManagement.DataAccess.IRepository;
using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess.Repository
{
    public class PhoneRepository : GenericRepository<MobilePhone>, IPhoneRepository
    {
        public PhoneRepository(AppDbContext context) : base(context)
        {
        }
    }
}
