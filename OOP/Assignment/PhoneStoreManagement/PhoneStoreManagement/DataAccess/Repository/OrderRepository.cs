﻿using PhoneStoreManagement.DataAccess.IRepository;
using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext context) : base(context)
        {
        }
    }
}
