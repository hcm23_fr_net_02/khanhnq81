﻿using Microsoft.EntityFrameworkCore;
using PhoneStoreManagement.DataAccess.IRepository;

namespace PhoneStoreManagement.DataAccess.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly DbSet<T> _dbSet;
        public GenericRepository(AppDbContext context)
        {
            _dbSet = context.Set<T>();
        }
        public void AddNew(T entity)
        {
            _dbSet.Add(entity);
        }

        public void DeleteById(T entity)
        {
            _dbSet.Remove(entity);
        }

        public List<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T GetById(int id)
        {
            var entity = _dbSet.Find(id);
            return entity;
        }

        public void UpdateById(T entity)
        {
            _dbSet.Update(entity);
        }
    }
}
