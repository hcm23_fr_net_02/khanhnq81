﻿using PhoneStoreManagement.DataAccess.IRepository;
using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDbContext context) : base(context)
        {
        }
    }
}
