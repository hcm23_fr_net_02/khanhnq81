﻿using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess.IRepository
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
    }
}
