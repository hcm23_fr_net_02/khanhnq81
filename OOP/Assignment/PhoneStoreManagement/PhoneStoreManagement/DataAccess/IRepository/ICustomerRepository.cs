﻿using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess.IRepository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }
}
