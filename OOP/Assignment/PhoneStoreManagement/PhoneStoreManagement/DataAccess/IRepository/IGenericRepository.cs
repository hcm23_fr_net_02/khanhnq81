﻿namespace PhoneStoreManagement.DataAccess.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        T GetById(int id);
        List<T> GetAll();
        void AddNew(T entity);
        void DeleteById(T entity);
        void UpdateById(T entity);
    }
}
