﻿using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.DataAccess.IRepository
{
    public interface IPhoneRepository : IGenericRepository<MobilePhone>
    {
    }
}
