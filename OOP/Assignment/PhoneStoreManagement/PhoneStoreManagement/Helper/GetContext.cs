﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using PhoneStoreManagement.DataAccess;

namespace PhoneStoreManagement.Helper
{
    public class GetContext
    {
        private const string ConnectionString = "Data Source= KHANHS; Database = StudentManagementEFCore-DI; Trusted_Connection = True; TrustServerCertificate = True";
        public static AppDbContext GetDbContext()
        {
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionBuilder.UseSqlServer(ConnectionString);
            var context = new AppDbContext(optionBuilder.Options);
            return context;

        }
    }
}
