﻿namespace PhoneStoreManagement.Helper
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("===========================MENU===========================");
            Console.WriteLine("| 1. Thêm điện thoại mới vào cửa hàng.                   |");
            Console.WriteLine("| 2. Xóa điện thoại khỏi cửa hàng.                       |");
            Console.WriteLine("| 3. Hiển thị danh sách các điện thoại có trong cửa hàng.|");
            Console.WriteLine("| 4. Cho phép một khách hàng mua điện thoại.             |");
            Console.WriteLine("| 5. Hiển thị thông tin về các mua hàng hiện có.         |");
            Console.WriteLine("| 6. Tìm kiếm điện thoại theo mã số sản phẩm.            |");
            Console.WriteLine("| 7. Exit                                                |");
            Console.WriteLine("==========================================================");
            Console.Write("Choice: ");
        }
    }
}
