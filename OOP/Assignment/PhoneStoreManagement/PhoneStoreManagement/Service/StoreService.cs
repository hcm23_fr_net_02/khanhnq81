﻿using PhoneStoreManagement.DataAccess.UnitOfWork;
using PhoneStoreManagement.Model;

namespace PhoneStoreManagement.Service
{
    public class StoreService
    {
        List<MobilePhone> phones;
        private readonly IUnitOfWork _unitOfWork;
        public StoreService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            phones = new List<MobilePhone>();
        }

        public void AddNewPhone()
        {
            try
            {
                MobilePhone phone = new MobilePhone();
                Console.WriteLine("Enter phone name: ");
                phone.Name = Console.ReadLine();
                Console.WriteLine("Enter manufacturer: ");
                phone.Manufacturer = Console.ReadLine();
                Console.WriteLine("Enter stock quantity: ");
                phone.StockQuantity = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter price: ");
                phone.Price = decimal.Parse(Console.ReadLine());
                _unitOfWork.PhoneRepository.AddNew(phone);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public MobilePhone FindPhoneById(int id)
        {
            var phone = _unitOfWork.PhoneRepository.GetById(id);
            if (phone is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return phone;
        }
        public void RemovePhone()
        {
            Console.WriteLine("Enter id: ");
            int id = int.Parse(Console.ReadLine());
            var phone = FindPhoneById(id);
            _unitOfWork.PhoneRepository.DeleteById(phone);
        }
        public List<MobilePhone> GetAll()
        {
            return _unitOfWork.PhoneRepository.GetAll();
        }
        public void ShowPhone()
        {
            var pagingList = GetAll();
            int pageSize = 3;  // Số phone mỗi trang
            int currentPage = 1;  // Trang hiện tại

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Page " + currentPage + "\n");

                int startIndex = (currentPage - 1) * pageSize;
                int endIndex = Math.Min(startIndex + pageSize, pagingList.Count);

                for (int i = startIndex; i < endIndex; i++)
                {
                    Console.WriteLine($"ID: {pagingList[i].ID} - Name: {pagingList[i].Name} - Manufacturer: {pagingList[i].Manufacturer} - StockQuantity: {pagingList[i].StockQuantity} - Price: {pagingList[i].Price}");
                }

                // Hiển thị điều hướng phân trang
                Console.WriteLine("\n[P] Previous | [N] Next | [B] Back to main menu");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage <= Math.Ceiling((pagingList.Count / pageSize) * 1.0))
                    {
                        currentPage++;
                    }
                }
                else if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 1)
                    {
                        currentPage--;
                    }
                }
                else if (key.Key == ConsoleKey.B)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        public void BuyPhone()
        {
            Console.WriteLine("Enter id to buy: ");
            int id = int.Parse(Console.ReadLine());
            var phone = FindPhoneById(id);
            if (phone is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            if (phone.StockQuantity <= 0)
            {
                throw new Exception("No more stock");
            }
            Order order = new Order();
            Console.WriteLine("Order id: ");
            order.ID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter customer id: ");
            order.CustomerID = int.Parse(Console.ReadLine());
            order.PhoneID = phone.ID;
            order.PurchaseDate = DateTime.Now;
            Console.WriteLine("Enter total price: ");
            order.TotalPrice = decimal.Parse(Console.ReadLine());
            _unitOfWork.OrderRepository.AddNew(order);
        }

        public void FindOrderById(int id)
        {
            var orders = _unitOfWork.OrderRepository.GetAll();
            var findOrder = orders.Where(order => order.ID == id).ToList();
            foreach (var item in findOrder)
            {
                Console.WriteLine($"Id: {item.ID} - Customer id: {item.CustomerID} - Phone id: {item.PhoneID} - Date: {item.PurchaseDate.ToString("dd/MM/yyyy")}");
            }
        }
    }
}
