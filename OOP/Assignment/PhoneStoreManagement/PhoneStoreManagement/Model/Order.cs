﻿namespace PhoneStoreManagement.Model
{
    public class Order
    {
        public int ID { get; set; }
        public DateTime PurchaseDate { get; set; }
        public Customer Customer { get; set; }
        public int CustomerID { get; set; }
        public decimal TotalPrice { get; set; }
        public MobilePhone MobilePhone { get; set; }
        public int PhoneID { get; set; }
    }
}
