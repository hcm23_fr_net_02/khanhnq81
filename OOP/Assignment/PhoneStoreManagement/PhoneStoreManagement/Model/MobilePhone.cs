﻿namespace PhoneStoreManagement.Model
{
    public class MobilePhone
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public int StockQuantity { get; set; }
        public decimal Price { get; set; }
        public List<Order> Orders { get; set; }
    }
}
