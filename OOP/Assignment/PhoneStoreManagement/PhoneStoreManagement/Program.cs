﻿using PhoneStoreManagement.DataAccess.UnitOfWork;
using PhoneStoreManagement.Helper;
using PhoneStoreManagement.Service;

var context = GetContext.GetDbContext();
IUnitOfWork unitOfWork = new UnitOfWork(context);
StoreService storeService = new StoreService(unitOfWork);
int choice = 0;
do
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                storeService.AddNewPhone();
                break;
            case 2:
                storeService.RemovePhone();
                break;
            case 3:
                storeService.ShowPhone();
                break;
            case 4:
                storeService.BuyPhone();
                break;
            case 5:
                Console.WriteLine("Enter order id: ");
                int id = int.Parse(Console.ReadLine());
                storeService.FindOrderById(id);
                break;
            case 6:
                Console.WriteLine("Enter phone id: ");
                int phoneId = int.Parse(Console.ReadLine());
                var phone = storeService.FindPhoneById(phoneId);
                Console.WriteLine($"{phone.ID} - {phone.Name} - {phone.Manufacturer} - {phone.StockQuantity}");
                break;
            case 7:
                Console.WriteLine("Bye bye !");
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 7");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 7);