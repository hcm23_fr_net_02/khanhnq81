﻿using LibraryManagement.DataAccess.UnitOfWork;
using LibraryManagement.Helper;
using LibraryManagement.Model;
using LibraryManagement.Service;
using System.Runtime.Intrinsics.Arm;

var context = AccessHelper.GetContext();
IUnitOfWork unitOfWork = new UnitOfWork(context);
BookService bookService = new BookService(unitOfWork);
CustomerService customerService = new CustomerService(unitOfWork);
int choice = 0;
do
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;
    try
    {
        Menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Book book = new Book();
                bookService.AddNewBook(book);
                break;
            case 2:
                Console.WriteLine("Enter book id: ");
                int bookId = int.Parse(Console.ReadLine());
                bookService.DeleteBook(bookId);
                break;

            case 3:
                bookService.ShowAll();
                break;
            case 4:
                Loan loan = new Loan();
                customerService.BorrowBook(loan);
                break;
            case 5:
                customerService.ShowBorrowedBook();
                break;
            case 6:
                Console.WriteLine("Enter book id to find: ");
                int findByDd = int.Parse(Console.ReadLine());
                var bookById = unitOfWork.BookRepository.GetById(findByDd);
                Console.WriteLine($"Id : {bookById.Id} | Title: {bookById.Title} | Author: {bookById.Author} | Price: {bookById.Price} | Stock quantity: {bookById.StockQuantity}");
                break;
            case 7:
                Environment.Exit(0);
                break;

            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 7);