﻿using LibraryManagement.DataAccess.Repository.IRepository;

namespace LibraryManagement.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        int SaveChange();
        IBookRepository BookRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        ILoanRepository LoanRepository { get; }
    }
}
