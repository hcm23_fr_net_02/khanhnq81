﻿using LibraryManagement.DataAccess.Repository.Implement;
using LibraryManagement.DataAccess.Repository.IRepository;

namespace LibraryManagement.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IBookRepository _bookRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly ILoanRepository _loanRepository;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _bookRepository = new BookRepository(context);
            _customerRepository = new CustomerRepository(context);
            _loanRepository = new LoanRepository(context);
        }
        public IBookRepository BookRepository { get => _bookRepository; }

        public ICustomerRepository CustomerRepository { get => _customerRepository; }

        public ILoanRepository LoanRepository { get => _loanRepository; }

        public int SaveChange() => _context.SaveChanges();
    }
}
