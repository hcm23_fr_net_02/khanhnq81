﻿using LibraryManagement.DataAccess.Repository.IRepository;
using LibraryManagement.Model;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.DataAccess.Repository.Implement
{
    public class LoanRepository : GenericRepository<Loan>, ILoanRepository
    {
        private readonly AppDbContext _context;

        public LoanRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public List<Loan> GetAllReference()
        {
            var loans = _context.Loans.Include(customer => customer.Customer).Include(book => book.Book).ToList();
            return loans;
        }
    }
}
