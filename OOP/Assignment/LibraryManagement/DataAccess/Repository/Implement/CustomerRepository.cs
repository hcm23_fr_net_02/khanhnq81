﻿using LibraryManagement.DataAccess.Repository.IRepository;
using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository.Implement
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        private readonly AppDbContext _context;

        public CustomerRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Customer GetCustomerById(int id)
        {
            var customer = _context.Customers.FirstOrDefault(customer => customer.Id == id);
            if (customer is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return customer;
        }
    }
}
