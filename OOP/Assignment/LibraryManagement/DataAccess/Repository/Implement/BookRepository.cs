﻿using LibraryManagement.DataAccess.Repository.IRepository;
using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository.Implement
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        private readonly AppDbContext _context;
        public BookRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public Book GetById(int id)
        {
            var book = _context.Books.FirstOrDefault(book => book.Id == id);
            if(book is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return book;
        }
    }
}
