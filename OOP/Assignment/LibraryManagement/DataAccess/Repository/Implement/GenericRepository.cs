﻿using LibraryManagement.DataAccess.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.DataAccess.Repository.Implement
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected DbSet<T> _dbSet;  
        public GenericRepository(AppDbContext context)
        {
            _dbSet = context.Set<T>();
        }
        public void AddNew(T entity) => _dbSet.Add(entity);

        public void Delete(T entity) => _dbSet.Remove(entity);

        public List<T> GetAll() => _dbSet.ToList();

        public void Update(T entity) => _dbSet.Update(entity);
    }
}
