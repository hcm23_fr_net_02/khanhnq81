﻿namespace LibraryManagement.DataAccess.Repository.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> GetAll();
        void AddNew(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
