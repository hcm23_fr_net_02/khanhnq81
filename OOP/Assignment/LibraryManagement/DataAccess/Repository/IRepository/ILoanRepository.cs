﻿using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository.IRepository
{
    public interface ILoanRepository : IGenericRepository<Loan>
    {
        List<Loan> GetAllReference();
    }
}
