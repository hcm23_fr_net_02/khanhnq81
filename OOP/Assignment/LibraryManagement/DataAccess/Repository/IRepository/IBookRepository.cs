﻿using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository.IRepository
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        Book GetById(int id);
    }
}
