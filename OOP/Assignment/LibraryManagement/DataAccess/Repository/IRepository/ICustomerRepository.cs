﻿using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository.IRepository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Customer GetCustomerById(int id);
    }
}
