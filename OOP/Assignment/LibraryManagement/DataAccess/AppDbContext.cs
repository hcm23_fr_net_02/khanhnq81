﻿using LibraryManagement.Model;
using Microsoft.EntityFrameworkCore;
namespace LibraryManagement.DataAccess
{
    public class AppDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Loan>()
                        .HasOne(customer => customer.Customer)
                        .WithMany(loan => loan.Loans);

            modelBuilder.Entity<Loan>()
                        .HasOne(book => book.Book)
                        .WithMany(loan => loan.Loans);

            modelBuilder.Entity<Loan>().HasKey(sc => new { sc.Id, sc.BookId, sc.CustomerId});
            modelBuilder.Entity<Customer>().HasData
                                        (
                                            new Customer{ Id = 1, Name = "Khanh", Address = "123/21" },
                                            new Customer{ Id = 2, Name = "A", Address = "13/1" },
                                            new Customer{ Id = 3, Name = "B", Address = "1/2" }
                                        );
        }
    }
}
