﻿namespace LibraryManagement.Model
{
    public class Loan
    {
        public int Id { get; set; }
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public Book Book { get; set; }
        public int BookId { get; set; }
        public DateTime BorrowDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public int Quantity { get; set; }
    }
}
