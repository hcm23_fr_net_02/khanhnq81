﻿namespace LibraryManagement.Model
{
    public class Book
    {
        private int _stockQuantity;
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int StockQuantity 
        {
            get => _stockQuantity;
            set
            {
                if (value < 0)
                {
                    throw new Exception("Stock quantity must be 0 or higher !");
                }
                _stockQuantity = value;
            }
        }
        public decimal Price { get; set; }
        public List<Loan> Loans { get; set; }
    }
}
