﻿namespace LibraryManagement.Model
{
    public class Library
    {
        public static List<Book> Books { get; set; } = new List<Book>();
        public static List<Loan> Loans { get; set; } = new List<Loan>();
    }
}
