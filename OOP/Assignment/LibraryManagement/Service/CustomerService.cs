﻿using LibraryManagement.DataAccess.UnitOfWork;
using LibraryManagement.Model;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LibraryManagement.Service
{
    public class CustomerService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void BorrowBook(Loan loan)
        {
            Console.WriteLine("Enter book id: ");
            int bookId = int.Parse(Console.ReadLine());
            var book = _unitOfWork.BookRepository.GetById(bookId);
            if(book.StockQuantity <= 0)
            {
                throw new Exception("Out of stock !");
            }
            Console.WriteLine("Enter customer id: ");
            int customerId = int.Parse(Console.ReadLine());
            _unitOfWork.CustomerRepository.GetCustomerById(customerId);
            loan.CustomerId = customerId;
            loan.BookId = bookId;
            Console.WriteLine("Enter loan id: ");
            loan.Id = int.Parse(Console.ReadLine());
            loan.BorrowDate = DateTime.Now;
            Console.WriteLine("Enter expired date: ");
            loan.ExpireDate = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter quantity: ");
            int quantity = int.Parse(Console.ReadLine());
            if (quantity > book.StockQuantity)
            {
                throw new Exception("Quantity must smaller than stock quantity !");
            }
            loan.Quantity = quantity;
            book.StockQuantity -= quantity;
            _unitOfWork.BookRepository.Update(book);
            _unitOfWork.LoanRepository.AddNew(loan);
            _unitOfWork.SaveChange();
        }

        public void ShowBorrowedBook(int currentPage = 0, int pageSize = 10)
        {
            Library.Loans = _unitOfWork.LoanRepository.GetAllReference();
            var totalPage = Math.Ceiling((Library.Loans.Count / pageSize) * 1.0);
            while (true)
            {
                Console.Clear();
                var loan = Library.Loans.Skip(currentPage * pageSize).Take(pageSize).ToList();
                foreach (var item in loan)
                {
                    Console.WriteLine($"Id: {item.Id} | Title: {item.Book.Title} | Author: {item.Book.Author} | Price: {item.Book.Price} | Quantity: {item.Quantity} | Borrow Date: {item.BorrowDate.ToString("dd/MM/yyyy")} | Expire Date: {item.ExpireDate.ToString("dd/MM/yyyy")} | Customer: {item.Customer.Name}");
                }

                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage < totalPage - 1)
                    {
                        currentPage++;
                    }
                }
                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }
                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }
            Console.Clear();
        }
    }
}
