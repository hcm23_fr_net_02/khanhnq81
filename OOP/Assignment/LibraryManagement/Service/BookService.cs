﻿using LibraryManagement.DataAccess.UnitOfWork;
using LibraryManagement.Model;

namespace LibraryManagement.Service
{
    public class BookService
    {
        private readonly IUnitOfWork _unitOfWork;
        public BookService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddNewBook(Book book)
        {
            Console.WriteLine("Enter book title: ");
            book.Title = Console.ReadLine();
            Console.WriteLine("Enter book author: ");
            book.Author = Console.ReadLine();
            Console.WriteLine("Enter stock quantity: ");
            book.StockQuantity = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter book price: ");
            book.Price = decimal.Parse(Console.ReadLine());

            _unitOfWork.BookRepository.AddNew(book);
            _unitOfWork.SaveChange();
        }

        public void DeleteBook(int id)
        {
            var book = _unitOfWork.BookRepository.GetById(id);
            _unitOfWork.BookRepository.Delete(book);
            _unitOfWork.SaveChange();
        }

        public void ShowAll(int currentPage = 0, int pageSize = 10)
        {
            Library.Books = _unitOfWork.BookRepository.GetAll();
            var totalPage = Math.Ceiling((Library.Books.Count / pageSize) * 1.0);
            while (true)
            {
                Console.Clear();
                var books = Library.Books.Skip(currentPage * pageSize).Take(pageSize).ToList();
                foreach (var item in books)
                {
                    Console.WriteLine($"Id: {item.Id} | Title: {item.Title} | Author: {item.Author} | Price: {item.Price} | Stock quantity: {item.StockQuantity} ");
                }

                Console.WriteLine("\n[N] Next | [P] Previous | [B] Back");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage < totalPage - 1)
                    {
                        currentPage++;
                    }
                }
                if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 0)
                    {
                        currentPage--;
                    }
                }
                if (key.Key == ConsoleKey.B)
                {
                    break;
                }
            }
            Console.Clear();
        }
    }
}

