﻿using LibraryManagement.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.Helper
{
    public class AccessHelper
    {
        private const string ConnectionString = "Data Source= KHANHS; Database = LibraryManagementAsm; Trusted_Connection = True; TrustServerCertificate = True";
        public static AppDbContext GetContext()
        {
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionBuilder.UseSqlServer(ConnectionString);
            var context = new AppDbContext(optionBuilder.Options);
            return context;
        }

    }
}
