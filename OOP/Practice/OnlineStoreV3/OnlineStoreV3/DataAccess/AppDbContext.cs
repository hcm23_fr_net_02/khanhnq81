﻿using Microsoft.EntityFrameworkCore;
using OnlineStoreV3.Model;

namespace OnlineStoreV3.DataAccess
{
    public class AppDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Data Source = KHANHS; Database = OnlineStoreV3EF; Trusted_Connection = True; TrustServerCertificate = True";
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                        .HasMany(order => order.Orders)
                        .WithOne(customer => customer.Customer)
                        .HasForeignKey(order => order.CustomerId);

            modelBuilder.Entity<Order>()
                        .HasMany(order => order.OrderDetails)
                        .WithOne(orderDetail => orderDetail.Order)
                        .HasForeignKey(orderDetail => orderDetail.OrderId);

            modelBuilder.Entity<OrderDetail>().HasKey(sc => new { sc.Id, sc.OrderId, sc.ProductId });
        }
    }
}
