﻿using BookManagement;
using static System.Reflection.Metadata.BlobBuilder;

int choice = 0;
Library library = new Library();
do
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;
    try
    {
        library.GeBookData();
        Helper.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                library.PrintBookInfo();
                break;
            case 2:
                Console.WriteLine("Enter book id to borrow: ");
                int borrowId = int.Parse(Console.ReadLine());
                var checkBorrow = library.CheckOut(borrowId);
                if (checkBorrow is true)
                {
                    Console.WriteLine("Borrow book success !");
                }
                else
                {
                    Console.WriteLine("Fail !");
                }
                break;
            case 3:
                Console.WriteLine("Enter book id to return: ");
                int returnId = int.Parse(Console.ReadLine());
                var checkReturn = library.ReturnBook(returnId);
                if (checkReturn is true)
                {
                    Console.WriteLine("Return success");
                }
                else
                {
                    Console.WriteLine("Fail !");
                }
                break;
            case 4:
                Console.WriteLine("Bye bye !");
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4 !");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
    finally
    {
        library.SaveFile();
    }
} while (choice != 4);