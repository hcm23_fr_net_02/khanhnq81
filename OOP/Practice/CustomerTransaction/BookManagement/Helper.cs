﻿using Newtonsoft.Json;

namespace BookManagement
{
    public class Helper
    {
        public static readonly string filePath = @"D:\2. FSOFT\FSoft-Training\OOP\CustomerTransaction\BookManagement\bookdata.json";
        public static void MainMenu()
        {
            Console.WriteLine("=================BookManagement=================");
            Console.WriteLine("| 1. Xem danh sách các cuốn sách trong thư viện |");
            Console.WriteLine("| 2. Mượn sách: Nhập mã số của sách để mượn.    |");
            Console.WriteLine("| 3. Trả sách: Nhập mã số của sách để trả.      |");
            Console.WriteLine("| 4. Thoát chương trình.                        |");
            Console.WriteLine("=================================================");
            Console.Write("User choice:");
        }
    }
}
