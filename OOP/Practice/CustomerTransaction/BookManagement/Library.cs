﻿using Newtonsoft.Json;

namespace BookManagement
{
    public class Library
    {
        private List<Book> _books;
        public Library()
        {
            _books = new List<Book>();
        }

        public bool CheckOut(int id)
        {
            var book = _books.FirstOrDefault(book => book.ID == id && book.Available == true);
            if (book is null)
            {
                throw new Exception("Book is not exist !");
            }
            book.Available = false;
            if (book.Available is false)
            {
                return true;
            }
            return false;
        }

        public bool ReturnBook(int id)
        {
            var returnBook = _books.FirstOrDefault(book => book.ID == id && book.Available == false);
            if (returnBook is null)
            {
                throw new Exception("Book is not exist !");
            }
            returnBook.Available = true;
            if (returnBook.Available is true)
            {
                return true;
            }
            return false;
        }

        public void PrintBookInfo()
        {
            int pageSize = 10;  // Số mục trên mỗi trang
            int currentPage = 1;  // Trang hiện tại

            while (true)
            {
                // Hiển thị dữ liệu trên trang hiện tại
                Console.Clear();
                Console.WriteLine("Page " + currentPage + "\n");

                int startIndex = (currentPage - 1) * pageSize;
                int endIndex = Math.Min(startIndex + pageSize, _books.Count);

                for (int i = startIndex; i < endIndex; i++)
                {
                    Console.WriteLine($"ID: {_books[i].ID} - Title: {_books[i].Title} - Author: {_books[i].Author} - Available: {_books[i].Available}");
                }

                // Hiển thị điều hướng phân trang
                Console.WriteLine("\n[P] Previous | [N] Next | [B] Back to main menu");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.N)
                {
                    if (currentPage < (_books.Count / pageSize))
                    {
                        currentPage++;
                    }
                }
                else if (key.Key == ConsoleKey.P)
                {
                    if (currentPage > 1)
                    {
                        currentPage--;
                    }
                }
                else if (key.Key == ConsoleKey.B)
                {
                    Console.Clear();
                    break;
                }

            }
        }

        public void GeBookData()
        {
            string content = File.ReadAllText(Helper.filePath);
            if (content.Length == 0)
            {
                throw new Exception("Fail to loading data !");
            }
            _books = JsonConvert.DeserializeObject<List<Book>>(content);
        }

        public void SaveFile()
        {
            if (!File.Exists(Helper.filePath))
            {
                File.Create(Helper.filePath);
            }
            string content = JsonConvert.SerializeObject(_books);
            File.WriteAllText(Helper.filePath, content);
        }
    }
}
