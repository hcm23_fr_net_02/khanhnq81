﻿namespace BookManagement
{
    public class Book
    {
        private int id;
        private string title;
        private string author;
        private bool available;
        public int ID
        {
            get => id;
            set => id = value;
        }
        public string Title
        {
            get => title;
            set => title = value;
        }
        public string Author
        {
            get => author;
            set => author = value;
        }
        public bool Available 
        {
            get => available;
            set => available = value;
        }
    }
}
