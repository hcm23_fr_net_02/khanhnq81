﻿namespace CustomerTransaction
{
    public class Customer
    {
        private double _accountBalance;
        private string _email;
        public string Name { get; set; }
        public string Email
        {
            get => _email;
            set
            {
                if (!value.Contains("@gmail.com"))
                {
                    throw new Exception("Invalid email !");
                }
                _email = value;
            }
        }
        public double AccountBalance
        {
            get => _accountBalance;
            set
            {
                if (value < 0)
                {
                    throw new Exception("Account balance must higher than 0");
                }
                _accountBalance = value;
            }
        }

        public void AddFund(double amount)
        {
            AccountBalance += amount;
        }

        public void MakePurchase(double amount)
        {
            AccountBalance -= amount;
        }

        public bool CanAfford(double amount)
        {
            if(amount > AccountBalance)
            {
                throw new Exception($"Amount must smaller or equal with Account balance ! Your account balance is : {AccountBalance}");
            }
            return true;
        }
    }
}
