﻿namespace CustomerTransaction
{
    public class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("===================MAIN=MENU===================");
            Console.WriteLine("| 1. Add fund                                 |");
            Console.WriteLine("| 2. Make transaction                         |");
            Console.WriteLine("| 3. Check account balance                    |");
            Console.WriteLine("| 4. Exit                                     |");
            Console.WriteLine("===============================================");
            Console.Write("User choice: ");
        }

        public static void LogIn()
        {
            Console.WriteLine("==========LOG=IN==========");
            Console.WriteLine("| 1. Login               |");
            Console.WriteLine("| 2. Exit                |");
            Console.WriteLine("==========================");
        }
    }
}
