﻿namespace CustomerTransaction
{
    public class TransactionController
    {
        private readonly Customer _customer;
        public TransactionController(Customer customer)
        {
            _customer = customer;
        }

        public void LogIn()
        {
            Console.WriteLine("Enter name: ");
            _customer.Name = Console.ReadLine();
            Console.WriteLine("Enter email: ");
            _customer.Email = Console.ReadLine();
        }

        public void AddFund()
        {
            Console.Clear();
            Console.WriteLine("Enter amount of money you want to add: ");
            double amountAdd = double.Parse(Console.ReadLine());
            _customer.AddFund(amountAdd);
        }

        public void MakePurchase()
        {
            Console.Clear();
            Console.WriteLine("How much money you want to withdraw: ");
            double withDraw = double.Parse(Console.ReadLine());
            _customer.CanAfford(withDraw);
            _customer.MakePurchase(withDraw);
        }

        public void CheckBalance()
        {
            Console.Clear();
            Console.WriteLine($"Your account balance is: {_customer.AccountBalance}");
        }
    }
}
