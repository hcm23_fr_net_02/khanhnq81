﻿using CustomerTransaction;

Customer customer = new Customer();
int transactionChoice = 0;
int loginChoice = 0;
TransactionController transaction = new TransactionController(customer);
do
{
    try
    {
        Menu.LogIn();
        loginChoice = int.Parse(Console.ReadLine());
        switch (loginChoice)
        {
            case 1:
                Console.Clear();
                transaction.LogIn();
                Console.Clear();
                do
                {
                    try
                    {
                        Menu.MainMenu();
                        transactionChoice = int.Parse(Console.ReadLine());
                        switch (transactionChoice)
                        {
                            case 1:
                                transaction.AddFund();
                                Console.Clear();
                                break;
                            case 2:
                                transaction.MakePurchase();
                                Console.Clear();
                                break;
                            case 3:
                                transaction.CheckBalance();
                                break;
                            case 4:
                                break;
                            default:
                                throw new Exception("Please choose from 1 to 4 !");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine("Press any key to continue !");
                        Console.ReadKey();
                    }
                } while (transactionChoice != 4);
                break;
            case 2:
                Console.WriteLine("Bye bye !");
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose 1 and 2");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
        Console.WriteLine("Press any key to continue !");
        Console.ReadKey();
    }
} while (loginChoice != 2);