﻿public class Cat : Animal
{
    public string FurColor { get; set; }

    public Cat(string name, int age, string furColor) : base(name, age, "Cat")
    {
        FurColor = furColor;
    }

    public void Meow()
    {
        Console.WriteLine("Meow! Meow!");
    }
}
