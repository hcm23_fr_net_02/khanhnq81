﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Dog dog1 = new Dog("chó 1", 3, "chó cỏ");
Dog dog2 = new Dog("chó 2", 5, "chó cỏ");
Dog dog3 = new Dog("chó 3", 2, "chó cỏ");

Cat cat1 = new Cat("mèo 1", 4, "mèo mun");
Cat cat2 = new Cat("mèo 2", 2, "mèo vàng");
Cat cat3 = new Cat("mèo 3", 6, "mèo trắng");

Animal[] animals = { dog1, dog2, dog3, cat1, cat2, cat3 };

foreach (var animal in animals)
{
    if(animal.Type is "Dog")
    {
        animal.Speak();
        ((Dog)animal).Bark();
    }
    if(animal.Type is "Cat")
    {
        animal.Speak();
        ((Cat)animal).Meow();
    }
}

