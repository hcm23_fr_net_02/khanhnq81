﻿public class Dog : Animal
{
    public string Breed { get; set; }

    public Dog(string name, int age, string breed) : base(name, age, "Dog")
    {
        Breed = breed;
    }

    public void Bark()
    {
        Console.WriteLine("Woof! Woof!");
    }
}
 