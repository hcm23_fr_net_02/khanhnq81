﻿namespace Shape_Interface
{
    public class Circle : Shape
    {
        public double CaculateArea(double x, double y) => Math.Round((x * y) * Math.PI, 2);
    }
}
