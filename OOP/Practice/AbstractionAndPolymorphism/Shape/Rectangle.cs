﻿namespace Shape_Abstraction
{
    public class Rectangle : Shape
    {
        public override double CaculateArea(double x, double y) => Math.Round(x * y,2);
    }
}
